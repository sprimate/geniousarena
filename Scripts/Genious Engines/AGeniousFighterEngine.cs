using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGeniousFighterEngine : ScriptableObject
{
    public virtual Type RewiredInputTypeDependency { get; } 
    public virtual bool EngineInputDetected { get; }
    public FighterController fighterController { get { return fc; } protected set { fc = value; } }
    public FighterController fc { get; protected set; }
    protected Transform transform { get { return fc?.transform; } }
    protected GameObject gameObject { get { return fc?.gameObject; } }
    protected FighterData fighterData { get { return fc?.fighterData; } }
    protected Animator m_Animator { get { return fc?.m_Animator; } }
    public T CreateInstance<T>(FighterController fighterController) where T : AGeniousFighterEngine
    {
        if (!(this is T))
        {
            Debug.LogError("Invalid cast - " + this.GetType() + " != " + typeof(T));
            return null;
        }

        var ret = Instantiate(this);
        ret.fc = fighterController;
        ret.Init();
        return ret as T;
    }

    protected void Init()
    {
        GenericCoroutineManager.instance.runOnLateUpdate += LateUpdate;
        GenericCoroutineManager.instance.runOnUpdate += Update;
        GenericCoroutineManager.instance.runOnFixedUpdate += FixedUpdate;
        Start();
    }


    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }
    protected virtual void LateUpdate()
    {

    }

    protected virtual void FixedUpdate()
    {

    }

    public virtual void RegisterInputCallbacks(PlayerInput input)
    {

    }


    HashSet<Coroutine> coroutines = new HashSet<Coroutine>();
    protected Coroutine StartCoroutine(IEnumerator a)
    {
        var ret = GenericCoroutineManager.instance.StartCoroutine(a);
        coroutines.Add(ret);
        return ret;
    }

    protected void StopCoroutine(Coroutine c)
    {
        if (coroutines.Contains(c))
        {
            coroutines.Remove(c);
        }
        GenericCoroutineManager.instance.StopCoroutine(c);
    }

    protected void StopAllCoroutines()
    {
        Action toDo = () => { };
        foreach (var c in coroutines)
        {
            toDo += () => { StopCoroutine(c); };
        }
        toDo?.Invoke();
    }
}
