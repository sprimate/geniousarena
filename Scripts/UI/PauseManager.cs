﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseManager : MonoBehaviour
{
    public string pauseInputActionName = "Pause";
    public GeniousRewiredMangerContainer pauseMenuInputManager;
    public UnityEvent OnPause;
    HashSet<PlayerInput> players = new HashSet<PlayerInput>();
    private void Awake()
    {
        GeniousInputManager.instance.onPlayerRegistered += (p) =>
        {
            InitPlayerInput(p);
        };
        GeniousInputManager.instance.OnInputManagerSwitched += (grim) =>
        {
            foreach(var p in players)
            {
                RegisterPauseAction(p);
            }
        };
    }

    void InitPlayerInput(PlayerInput p)
    {
        players.Add(p);
        RegisterPauseAction(p);
    }

    void RegisterPauseAction(PlayerInput p)
    {
        Debug.Log("Player " + p + " registered");
        p.AppendAction<bool>("Pause", (down) =>
        {
            if (down)
            {
                pauseTriggered = p;
            }
        });
    }

    //Use this to disallow multiple clicks per frame
    PlayerInput pauseTriggered;
    private void LateUpdate()
    {
        if (pauseTriggered != null)
        {
            Debug.Log(pauseTriggered + " down.");
            if (pauseMenuInputManager != null)
            {
                GeniousInputManager.instance.SetRewiredInputManager(pauseMenuInputManager);
            }
            OnPause?.Invoke();
            pauseTriggered = null;
        }
    }
}