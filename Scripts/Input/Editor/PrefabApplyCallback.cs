using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
internal class PrefabExtension
{
    static PrefabExtension()
    {
        UnityEditor.PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdate;
    }

    static void OnPrefabInstanceUpdate(GameObject instance)
    {
        UnityEngine.Debug.Log("[Callback] Prefab.Apply on instance named :" + instance.name);

        GameObject prefab = UnityEditor.PrefabUtility.GetPrefabParent(instance) as GameObject;
        string prefabPath = AssetDatabase.GetAssetPath(prefab);
        UnityEngine.Debug.Log("@Prefab originPath=" + prefabPath);
    }
}