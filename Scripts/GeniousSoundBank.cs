﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GeniousSoundBankEntry 
{
   public string soundKey;
   public AudioClip[] clips;
}

[Serializable]
public class GeniousSoundBank
{
   [SerializeField] List<GeniousSoundBankEntry> soundList = new List<GeniousSoundBankEntry>();
   Dictionary<string, AudioClip[]> _soundBankDict;
   public Dictionary<string, AudioClip[]> soundBankDict {
      get {
         if (_soundBankDict == null)
         {
            InitSoundBankDict();
         }

         return _soundBankDict;
      }
      protected set{
         _soundBankDict = value;
      }
   }

   void InitSoundBankDict()
   {
      soundBankDict = new Dictionary<string, AudioClip[]>();
      foreach(var sbe in soundList)
      {
         soundBankDict[sbe.soundKey] = sbe.clips;
      }
   }

   public AudioClip[] GetClips(string soundKey)
   {
      return soundBankDict.GetOrDefault(soundKey);
   }
}