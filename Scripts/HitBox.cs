using Gamekit3D;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class HitBox : APlayerControllerAccessory
{
    public List<HitPoint> orderedHitPoints;
    [HideInInspector] public HitPoint closestHitPoint;
    BoxCollider hitboxCollider;

    public Vector3 size { get { return hitboxCollider.size; } set { hitboxCollider.size = value; } }
    public Vector3 center { get {return hitboxCollider.center; } set { hitboxCollider.center = value; } }
    public void Awake()
    {
        hitboxCollider = GetComponent<BoxCollider>();
        if (orderedHitPoints == null || orderedHitPoints.Count == 0)
        {
            orderedHitPoints = new List<HitPoint>(GetComponentsInChildren<HitPoint>());
        }

        if (orderedHitPoints.Count == 0)
        {
            orderedHitPoints.Add(gameObject.AddComponent<HitPoint>());
        }
    }

    bool canHit;//Only one hit per hitbox actove state

    void OnHit()
    {
        canHit = false;
    }

    public void OnCollisionStay(Collision collision)
    {
        if (!canHit)
        {
            return;
        }

        playerController.RegisterHit(collision.collider, orderedHitPoints, collision.contacts[0].point);
    }
    public void OnTriggerStay(Collider other)
    {
        if (!canHit)
        {
            return;
        }

        playerController.RegisterHit(other, orderedHitPoints, other.transform.position);
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log("Hitbox hit " + hit.gameObject, hit.gameObject);
    }

    //public Damageable ellenDamageable;
    public void Update()
    {
        if (playerController == null)
        {
            return;
        }
        //Debug.Log(myPlayerController + " and " + myPlayerController?.m_Damageable + " and " + myPlayerController?.meleeWeapon);
        /*playerController.m_Damageable.isInvulnerable = meleeWeapon != null && (!playerController.canMove);// || !myPlayerController.isInGuard);
        if (ellenDamageable != null)
        {
            ellenDamageable.isInvulnerable |= playerController.m_Damageable.isInvulnerable;
        }*/
    }

    int frame;
    public void SetActive(bool val)
    {
        if (val)
        {
            canHit = true;
            if (playerController == null)
            {
                Debug.Log("WTF? Why isn't this working? " + playerController);
            }
            playerController.attackEngine.onHitAnimatorCallback += OnHit;
            frame = Time.frameCount;
        }
        else
        {
            if (playerController != null)
            {
                playerController.attackEngine.onHitAnimatorCallback -= OnHit;
            }
            if (frame == Time.frameCount)
            {
                GenericCoroutineManager.instance.RunAfterFrame(() => { hitboxCollider.enabled = (false); });
                return;
            }
        }

        hitboxCollider.enabled = (val);
    }
}
