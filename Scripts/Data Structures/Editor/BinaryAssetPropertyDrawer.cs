using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(BinaryAsset))]
public class BinaryAssetPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var myObject = property?.serializedObject?.targetObject;
        TextAsset ta = null;
        if (myObject != null)
        {
            ta = (fieldInfo.GetValue(myObject) as BinaryAsset).backingTextAsset;
        }
        //EditorGUI.LabelField(position, "Is this working?");
        var newTa =EditorGUI.ObjectField(position, property.displayName + " [.bytes file]", ta, typeof(TextAsset)) as TextAsset;
        if (newTa != ta)
        {
            fieldInfo.SetValue(myObject, new BinaryAsset(newTa));
        }
    }
}