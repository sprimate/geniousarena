using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Mono singleton Class. Extend this class to make singleton component.
/// Example: 
/// <code>
/// public class Foo : MonoSingleton<Foo>
/// </code>. To get the instance of Foo class, use <code>Foo.instance</code>
/// Override <code>Init()</code> method instead of using <code>Awake()</code>
/// from this class.
/// </summary>
/// 

///NOTE!
///DO NOT CALL .Instance from OnDisable! If the scene is changing, it will cause big problems!
public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    //Any file in a "Resources/{monoSingletonDefaultResourcesPath}" folder will be loaded instead of a new empty gameobject for Temp instances
    const string monoSingletonDefaultResourcesPath = "MonoSingleton Defaults";
    protected virtual bool DestroyDuplicateGameObjects { get { return true; } }
    bool _shouldDestroyOnLoad = true;
    protected virtual bool ShouldDestroyOnLoad { get { return true; } }
    private static T m_Instance = null;
    public static T Instance
    {
        get
        {
            return instance;
        }
    }
    public static T instance
    {
        get
        {
            // Instance requiered for the first time, we look for it
            if (m_Instance == null)
            {
                m_Instance = GameObject.FindObjectOfType<T>();

                // Object not found, we create a temporary one
                if (m_Instance == null)
                {
                    //Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                    isTemporaryInstance = true;
                    Object[] objs= Resources.LoadAll(monoSingletonDefaultResourcesPath, typeof(T));
                    if (objs.Length > 1)
                    {
                        Debug.Log("WARNING: Multiple objects of type [" + typeof(T) + "] found in path [Resources/" + monoSingletonDefaultResourcesPath + "]. Instantiating the first obect");
                    }

                    if (objs.Length > 0)
                    {
                        m_Instance = GameObject.Instantiate(((T)objs[0]).gameObject as GameObject).GetComponent<T>();
                    }
                    else
                    {
                        m_Instance = new GameObject("Temp Instance of " + typeof(T).ToString(), typeof(T)).GetComponent<T>();
                    }
                    // Problem during the creation, this should not happen
                    if (m_Instance == null)
                    {
                        Debug.LogError("Problem during the creation of " + typeof(T).ToString());
                    }
                }
                if (!_isInitialized)
                {
                    _isInitialized = true;
                    m_Instance.InitSingleton();
                }
            }

            return m_Instance;
        }
    }

    public static bool hasInstance
    {
        get
        {
            return m_Instance != null;
        }
    }

    public static bool isTemporaryInstance { private set; get; }

    public static bool _isInitialized { private set; get; }

    // If no other monobehaviour request the instance in an awake function
    // executing before this one, no need to search the object.
    protected virtual void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this as T;
        }
        else if (m_Instance != this)
        {
            if (DestroyDuplicateGameObjects)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                DestroyImmediate(this);
            }
            return;
        }
        if (!_isInitialized)
        {
            if (!ShouldDestroyOnLoad)
            {
                //gameObject.DontDestroyOnLoad();
            }
            _isInitialized = true;
            m_Instance.InitSingleton();
        }
    }

    /// <summary>
    /// This function is called when the instance is used the first time
    /// Put all the initializations you need here, as you would do in Awake
    /// </summary>
    public virtual void InitSingleton() { }

    /// Make sure the instance isn't referenced anymore when the user quit, just in case.
    private void OnApplicationQuit()
    {
        m_Instance = null;
    }
}