using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterShield : APlayerControllerAccessory
{
    Vector3 maxShieldScale;
    public float maxShield;
    public float shieldHealth;
    public Action onShieldBreak;
    // Start is called before the first frame update
    void Awake()
    {
        SetActive(false);
    }

    public void InitializeShield(float _maxShield, Action _onShieldBreak, bool shieldActive)
    {
        maxShield = _maxShield;
        onShieldBreak = _onShieldBreak;
        maxShieldScale = transform.localScale;
        shieldHealth = maxShield;
        SetActive(shieldActive);
    }

    public void SetActive(bool val)
    {
        gameObject.SetActive(val);
    }

    public void AdjustShieldHealth(float damageAmount)
    {
        shieldHealth += damageAmount;
        shieldHealth = Mathf.Min(maxShield, Mathf.Max(0, shieldHealth));
        float scale = shieldHealth / maxShield;
        transform.localScale = maxShieldScale * scale;
        if (shieldHealth <= 0)
        {
            onShieldBreak?.Invoke();
        }
    }
}
