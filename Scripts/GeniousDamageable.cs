using Gamekit3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousDamageable : ADamageable
{
    public FighterController physicsObject;
    public float damagePercent { get; protected set; }

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        if (physicsObject == null)
        {
            physicsObject = GetComponent<FighterController>();
        }
    }
    /* 
    public void AddDamage(float percent)
    {
        if (!isInvulnerable)
        {
            damagePercent += percent;
            m_timeSinceLastHit = 0f;
            isInvulnerable = true;
        }
    }

    public override void ApplyDamage(DamageMessage data)
    {
        base.ApplyDamage(data);
        physicsObject.attackEngine?.SetHurtTrigger();
    }
    */
}
