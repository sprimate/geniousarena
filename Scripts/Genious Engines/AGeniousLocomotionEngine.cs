using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGeniousLocomotionEngine : AGeniousFighterEngine
{
    public Material m_CurrentWalkingSurface { get; protected set; }    // Reference used to make decisions about audio.
    public virtual float m_VerticalSpeed { get { return 0f; } set { } }// Debug.Log("Setting VertSpeed to " + m_VerticalSpeed); } }              // How fast Ellen is currently moving up or down.
    //public virtual float minFallSpeed {get;}
    public bool canJump { get; set; } = true;
    public bool canMove { get; set; } = true;
    public bool canRotate { get; set; } = true;

    public abstract void MoveCharacter(Vector3 motion);
    public abstract void OnJump(bool val);
    public abstract void TriggerJump(bool val);
    public abstract void SetTargetRotation();
    public abstract void CalculateMovement();
    public abstract void UpdateGroundedState();
    public virtual float movementInputMagnitude { get; protected set; }


    public virtual void SetMinFallSpeed(){}
    public virtual void UpdateInputAnimatorValues(Animator animator) { }

    Action nextAnimatorFrame;
    public Action onNextAnimatorFrame;
    public virtual void OnAnimatorMove(){
        
        if (nextAnimatorFrame != null)
        {
            nextAnimatorFrame.Invoke();
        }
        nextAnimatorFrame = onNextAnimatorFrame;
        onNextAnimatorFrame = null;
    }//Not sure if this needs to be abstract or not.




}
