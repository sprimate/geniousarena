using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGeniousAttackEngine : AGeniousFighterEngine
{
    protected readonly int m_HashHurt = Animator.StringToHash("Hurt");
    [SerializeField] AGeniousAttackEngineUI attackEngineUiReference;
    static AGeniousAttackEngineUI _attackEngineUiInstance;
    public AGeniousAttackEngineUI attackEngineUiInstance {
        get {
            if (_attackEngineUiInstance == null && attackEngineUiReference != null)
            {
                _attackEngineUiInstance = GameObject.Instantiate(attackEngineUiReference.gameObject).GetComponent<AGeniousAttackEngineUI>();
            }

            return _attackEngineUiInstance;
        }
    }

    public abstract void RegisterAttack(Move move, FighterController attacker, FighterController target, HitPoint hitPoint );
    public abstract void RegisterWasHit(HitPoint hitPoint, Move m);
    public abstract void MoveCompleted(Move move);
    public abstract void Death();
    public virtual bool attackDetected { get; protected set; }
    public virtual void OnMoveIdSet(string moveId)
    {

    }

    public abstract void HandleKnockbackResponse(KnockBackResponse knockback, FighterController damageSource, Move attack);

    public Action onHitAnimatorCallback;
    public virtual void SetOnHitCallback(Action onHit)
    {
        onHitAnimatorCallback = onHit;
    }

    public virtual void SetHurtTrigger()
    {
        fc.m_Animator.SetTrigger(m_HashHurt);
        GenericCoroutineManager.instance.RunInFrames(1, () => {
            fc.m_Animator.ResetTrigger(m_HashHurt);
        });
    }

    protected override void Start()
    {
        attackEngineUiInstance?.RegisterPlayer(fc);

      /*   GenericCoroutineManager.instance.RunInFrames(1, () => {
        });*/
    }
}