using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class GenericCoroutineManager : MonoSingleton<GenericCoroutineManager>
{

    public Action runOnLateUpdate;
    public Action runOnUpdate;
    public Action runOnFixedUpdate;
    public Action runBeforeEndOfFrame;
    public Action runOnEndOfFrame;

    public Action runOnNextUpdate;
    public Action runOnNextLateUpdate;
    public Action runOnNextFixedUpdate;
    public Action runBeforeNextEndOfFrame;
    public Action runOnNextEndOfFrame;

    int numBeforeEndOfFrameActions = 0;

    Thread mainThread;

    public Coroutine RunInSeconds(float numSeconds, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextUpdate += () => { RunInSeconds(numSeconds, action); };
            return null;
        }
        return StartCoroutine(RunInSecondsCoroutine(numSeconds, action));
    }

    public Coroutine RunInSecondsRealtime(float numSeconds, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextUpdate += () => { RunInSecondsRealtime(numSeconds, action); };
            return null;
        }
        return StartCoroutine(RunInSecondsRealtimeCoroutine(numSeconds, action));
    }

    protected override void Awake()
    {
        base.Awake();
        gameObject.DontDestroyOnLoad();
        mainThread = Thread.CurrentThread;
    }
    public Coroutine RunInFrames(int numFrames, Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from tyhe main thread");
            runOnNextLateUpdate += () => { RunInFrames(numFrames, action); };
            return null;
        }
        return StartCoroutine(RunInFramesCoroutine(numFrames, action));
    }

    public Coroutine RunAfterFrame(Action action)
    {
        return ExecuteAfterFrame(action);
    }

    public Coroutine ExecuteAfterFrame(Action action)
    {
        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from the main thread");
            runOnNextEndOfFrame += action;//() => { ExecuteAfterFrame(action); };
            return null;
        }
        return StartCoroutine(ExecuteAfterFrameCoroutine(action));
    }

    /// <summary>
    /// This is run at the end of the frame, but before everything thats run in "ExecuteAtEndOfFrame" is called
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public Coroutine ExecuteBeforeEndOfFrame(Action action)
    {

        if (mainThread != Thread.CurrentThread)
        {
            Debug.Log("WARNING: Returning null Coroutine because this isn't called from the main thread");
            runBeforeNextEndOfFrame += action;// () => { ExecuteBeforeEndOfFrame(action); };
            return null;
        }

        numBeforeEndOfFrameActions++;
        return StartCoroutine(ExecuteBeforeEndOfFrameCoroutine(action));
    }

    public Coroutine ExecuteAtEndOfFrame(Action action)
    {
        return ExecuteAfterFrame(action);
    }

    public Coroutine RunAfterFixedUpdateFrame(Action action)
    {
        return StartCoroutine(RunAfterFixedUpdateCoroutine(action));
    }

    public Coroutine RunInFixedUpdateFrames(int frames, Action action)
    {
        return StartCoroutine(RunInFixedUpdateFramesCoroutine(frames, action));

    }

    public Coroutine RunOnFixedUpdateFrame(Action action)
    {
        return StartCoroutine(RunOnFixedUpdateCoroutine(action));
    }

    public new void StopCoroutine(Coroutine c)
    {
        if (c != null)
        {
            base.StopCoroutine(c);
        }
    }

    IEnumerator RunAfterFixedUpdateCoroutine(Action action)
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForEndOfFrame();
        action.Invoke();
    }

    IEnumerator RunOnFixedUpdateCoroutine(Action action)
    {
        yield return new WaitForFixedUpdate();
        action.Invoke();
    }

    IEnumerator RunInFixedUpdateFramesCoroutine(int frames, Action action)
    { 
        while(frames-- > 0)
        {
            yield return new WaitForFixedUpdate();
        }
        action.Invoke();
    }

    IEnumerator RunInFramesCoroutine(int numFrames, Action action)
    {
        for (int i = 0; i < numFrames; i++)
        {
            yield return null;
        }
        action();
    }

    IEnumerator RunInSecondsCoroutine(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }

    IEnumerator RunInSecondsRealtimeCoroutine(float seconds, Action action)
    {
        yield return new WaitForSecondsRealtime(seconds);
        action();
    }

    Action onBeforeCompleted;
    IEnumerator ExecuteBeforeEndOfFrameCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        action?.Invoke();
        if (--numBeforeEndOfFrameActions<= 0)
        {
            onBeforeCompleted?.Invoke();
            onBeforeCompleted = null;
        }
//        GeniousSettings.Log("Before jawns from " + (numBeforeEndOfFrameActions + 1) + " to " + numBeforeEndOfFrameActions);
    }

    IEnumerator ExecuteAfterFrameCoroutine(Action action)
    {
        yield return new WaitForEndOfFrame();
        ResolveEOFStuff(action);
    }

    void Update()
    {
        if (runOnUpdate != null)
            runOnUpdate.Invoke();

        if (runOnNextUpdate != null)
        {
            runOnNextUpdate.Invoke();
            runOnNextUpdate = null;
        }
    }

    bool beforeEndOfFrameComplete;
    IEnumerator Start()
    {
        while (gameObject != null)
        {
            if (gameObject.activeInHierarchy)
            {
                runBeforeNextEndOfFrame?.Invoke();
                runBeforeNextEndOfFrame = null;
                runBeforeEndOfFrame?.Invoke();
                beforeEndOfFrameComplete = true;

                ResolveEOFStuff(() => {
                    runOnNextEndOfFrame?.Invoke();
                    runOnNextEndOfFrame = null;
                    runOnEndOfFrame?.Invoke();
                //    GeniousSettings.Debug("Finishing frame");
                });
            }
            yield return new WaitForEndOfFrame();
            beforeEndOfFrameComplete = false;
        }
    }

    void ResolveEOFStuff(Action a)
    {
        if (numBeforeEndOfFrameActions > 0)
        {
            onBeforeCompleted += a;
        }
        else if (!beforeEndOfFrameComplete)
        {
            runOnNextEndOfFrame += a;
        }
        else
        {
            a?.Invoke();
        }
    }

    void FixedUpdate()
    {
        runOnFixedUpdate?.Invoke();
        runOnNextFixedUpdate?.Invoke();
        runOnNextFixedUpdate = null;
    }

    void LateUpdate()
    {
        if (runOnLateUpdate != null)
        {
            runOnLateUpdate.Invoke();
        }

        if (runOnNextLateUpdate != null)
        {
            runOnNextLateUpdate.Invoke();
            runOnNextLateUpdate = null;
        }
    }
}