﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuickAndDirty {

    public static class IListExtensions {
        
        public static T RandomItem<T>(this IList<T> self)
        {
            return self[Random.Range(0, self.Count)];
        }

        public static void Swap<T>(this IList<T> list, int lhs, int rhs)
        {
            var a = list[lhs];
            list[lhs] = list[rhs];
            list[rhs] = a;
        }

        public static void Shuffle<T>(this IList<T> list, int itterations = 1)
        {
            for (int j = 0; j < itterations; j++)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    list.Swap(i, Random.Range(0, list.Count));
                }
            }
        }
    }
}
