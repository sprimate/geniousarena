using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFighterDataHolder
{
    FighterData fighterData { get; }
}
