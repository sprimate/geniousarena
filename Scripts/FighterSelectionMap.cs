﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterSelectionMap : ScriptableObject
{
    public int rows {
        get{
            return fighterRows.Count;
        }
    }

    bool saidWarning = false;
    public int columns{
        get {
            int? ret = null;
            foreach(var fdr in fighterRows)
            {
                if (ret == null)
                {
                    ret = fdr.fighters.Count;
                }
                else if (fdr.fighters.Count != ret.Value)
                {
                    if(!saidWarning)
                    {
                        Debug.LogError("All Columns must be the same length! Can leave null columnns if necesary");
                        saidWarning = true;
                    }
                    break;
                }
            }

            return ret.Value;
        }
    }
    public Sprite image;
    [Serializable]
    protected class FighterDataRow
    {
        public List<FighterData> fighters;
    }

    [SerializeField] protected List<FighterDataRow> fighterRows;
    public FighterData Get(Vector2 percentageCoordinates)
    {
        return null;
    }

    public FighterData Get(Vector2Int coords)
    {
        if (coords.x < fighterRows.Count && coords.y<fighterRows[coords.x].fighters.Count)
        {
            return fighterRows[coords.x].fighters[coords.y];
        }

        return null;
    }

    public Vector2Int GetDimensions()
    {
        return new Vector2Int(rows, columns);
    }
}