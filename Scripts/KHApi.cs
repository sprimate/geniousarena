using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using static Smash_Forge.ATKD;

[Serializable]
public class Move : SmashCalculatorJSONClass
{
    public string name;//Specific Name
    public string moveName;
    public UnparsedMove unparsedMove;
    public ushort AnimationId;
    public bool isProjectile; //Custom. Needs to be populated
    public int chargedFrames; //Custom. Needs to be populated
    public string api_id;
    public bool allowMovement;
    public int id;
    public int hitbox_no;
    public float base_damage;
    public float angle;
    public float bkb;
    public float kbg;
    public float wbkb;
    public List<HitboxActiveFrames> hitboxActive;
    public Dictionary<string, HitboxActiveList> specialHitboxes;
    public int faf = -1;
    public int landingLag = -1;
    public List<CancelCond> autoCancel;
    public float preLaunchDamage;
    public float counterMult;
    public float rehitRate;
    public float shieldDamage;
    public bool weightDependent;
    public bool smash_attack;
    public bool @throw;
    public bool chargeable;
    public bool grab;
    public bool tilt;
    public bool jab;
    public bool aerial;
    public bool taunt;
    public bool dashAttack;
    public bool counter;
    public bool commandGrab;
    public bool unblockable;
    public bool windbox;
    public bool multihit;
    public bool spike;
    public bool isFinishingTouch;
    public Charge charge;
    public string type;
    public object pikminColor;
    public string character;
    public HitboxEntry hitbox;
    bool valid = true;

    public Move invalidate()
    {
        valid = false;
        return this;
    }

    public Move(string api_id, int hitbox_no, string name, string moveName, string base_damage, string angle, string bkb, string kbg, float wbkb, HitboxActiveList hitboxActive, string faf, string landingLag, List<CancelCond> autoCancel, double preDamage, double counterMult, double rehitRate, double shieldDamage, bool weightDependent)
    {
        this.api_id = api_id;
        this.id = 0;
        this.hitbox_no = hitbox_no;
        this.name = name;
        this.moveName = moveName;
        float.TryParse(base_damage, out this.base_damage);
        float.TryParse(kbg, out this.kbg);
        float.TryParse(angle, out this.angle);
        float.TryParse(bkb, out this.bkb);
        this.wbkb = wbkb;
        this.hitboxActive = hitboxActive.hitboxActiveList;
        this.specialHitboxes = hitboxActive.specialHitboxes;
        int.TryParse(faf, out this.faf);
        int.TryParse(landingLag, out this.landingLag);
        this.autoCancel = autoCancel;
        this.preLaunchDamage = (float)(preDamage);
        this.counterMult = (float)counterMult;
        this.rehitRate = (float)rehitRate;
        this.shieldDamage = (float)shieldDamage;
        this.weightDependent = weightDependent;
    }

    public static List<Move> GetMoves(UnparsedMove upm, FighterData fighterData)
    {
        var moveParser = new MoveParser(upm.InstanceId, upm.Name, upm.BaseDamage, upm.Angle, upm.BaseKnockBackSetKnockback, upm.KnockbackGrowth, upm.HitboxActive, upm.FirstActionableFrame, upm.LandingLag, upm.AutoCancel, upm.IsWeightDependent, false);
        var moves = moveParser.moves;
        foreach(var m in moves)
        {
            foreach(var entry in fighterData.hitboxData.entries)
            {
                if (entry.animationId == upm.AnimationId)
                {
                    m.hitbox = entry;
                }
            }
            string[] aerials = new string[] { "nair", "fair", "bair", "dair" };
            if (aerials.Contains(m.moveName.ToLower().Trim()))
            {
                m.aerial = true;
                m.allowMovement = true;
            }
        }
        return moves;

        /*List<Move> ret = new List<Move>();
        int numHitboxes = upm.BaseDamage.Split('/').Length;
        numHitboxes = Mathf.Max(numHitboxes, upm.Angle.Split('/').Length);
        numHitboxes = Mathf.Max(numHitboxes, upm.BaseKnockBackSetKnockback.Split('/').Length);
        numHitboxes = Mathf.Max(numHitboxes, upm.KnockbackGrowth.Split('/').Length);
        for (int i = 0; i < numHitboxes; i++)
        {

            
            m.isProjectile = upm.Projectile;
            m.chargedFrames = 0;
            m.moveName = upm.Name;
            m.name = upm.Name;
            m.api_id = upm.InstanceId;
            int.TryParse(upm.InstanceId, out m.id);
            m.hitbox_no = i;

            string baseDamage = upm.BaseDamage;
            var indexOfAddedShieldDamage = baseDamage.IndexOf("(+");
            if (indexOfAddedShieldDamage >= 0)
            {
                baseDamage = baseDamage.Substring(0, indexOfAddedShieldDamage);
            }

            float.TryParse(GetHitboxDependentData(baseDamage, i), out m.base_damage);
            float.TryParse(GetHitboxDependentData(upm.Angle, i), out m.angle);
            float.TryParse(GetHitboxDependentData(upm.BaseKnockBackSetKnockback, i), out m.bkb);
            float.TryParse(GetHitboxDependentData(upm.KnockbackGrowth, i), out m.kbg);
            m.hitboxActive = new HitboxActiveList(upm.HitboxActive).hitboxActiveList;
            int.TryParse(upm.FirstActionableFrame, out m.faf);
            ret.Add(m);
        }
        return ret;
        */
    }
    static string GetHitboxDependentData(string data, int hitbox)
    {
        var split = data.Split('/');
        if (hitbox >= split.Length)
        {
            return split[split.Length - 1];
        }

        return split[hitbox];

    }
}

//khapi.js function parseHitbox(hitboxActive)
[SerializeField]
public class HitboxActiveList : SmashCalculatorJSONClass
{
    public List<HitboxActiveFrames> hitboxActiveList;
    public Dictionary<string, HitboxActiveList> specialHitboxes = new Dictionary<string, HitboxActiveList>();

    public HitboxActiveList(List<HitboxActiveFrames> value)
    {
        hitboxActiveList = value;
    }
    public HitboxActiveList(string hitboxActive)
    {
       // Debug.Log("HitboxActive PreParsed: " + hitboxActive);

        if (hitboxActive.Trim().EndsWith("-"))
        {
            hitboxActive = hitboxActive.Split('-')[0].Trim();
        }
        hitboxActiveList = new List<HitboxActiveFrames>();
        if (String.IsNullOrWhiteSpace(hitboxActive))
        {
            hitboxActiveList.Add(new HitboxActiveFrames(float.NaN, float.NaN));
            return;
        }

        //TODO - test if this regex is correct. Taken from the khapi.js  at the function parseHitbox

      //  Debug.Log(hitboxActive);
        var indexOfSpecials = hitboxActive.IndexOf("(");
        if (indexOfSpecials >= 0)
        {
            specialHitboxes = new Dictionary<string, HitboxActiveList>();
            var specials = hitboxActive.Substring(indexOfSpecials);
            foreach(var special in specials.Substring(1, specials.Length - 2).Split(','))
            {
                if (special.Contains(":"))
                {
                    var split = special.Split(':');
                    Debug.Log("Special: " + special);
                    Debug.Log("Saving " + split[0] + " as " + split[1]);
                    specialHitboxes[split[0]] = new HitboxActiveList(split[1]);
                }
                else
                {
                    //Debug.LogError("WARNING: Special [" + special + "] not handled.");
                    List<HitboxActiveFrames> ret = new List<HitboxActiveFrames>();
                    ret.Add(new HitboxActiveFrames(1f, float.MaxValue));
                    specialHitboxes[special] = new HitboxActiveList(ret);// always
                }
            }
            hitboxActive = hitboxActive.Substring(0, indexOfSpecials);
        }

        var hitbox = hitboxActive.Split(',');
        for (var i = 0; i < hitbox.Length; i++)
        {
            hitboxActiveList.Add(new HitboxActiveFrames(hitbox[i]));
        }
    }
}


class MoveParser
{
    string id, name, base_damage, angle, bkb, kbg, faf, _hitboxes, landingLag, ignore_hitboxes;
    bool weightDependent;
    HitboxActiveList hitboxActive, hitboxes;
    double preDamage;
    bool _throw, aerial, grab;
    List<CancelCond> autoCancel;
    double rehitRate;
    double counterMult;
    double shieldDamage;
    public List<Move> moves;
    public MoveParser(string id, string name, string base_damage, string angle, string bkb, string kbg, string hitboxActive, string faf, string landingLag, string autoCancel, bool weightDependent, bool ignore_hitboxes)
    {
        //Don't remove this for a while... If there are errors with the parser, this helps determine why
        Debug.Log("Parsing " + name);
        HitboxActiveList specialHitboxes = null;
        if (base_damage == null)
            base_damage = "";
        if (angle == null)
            angle = "";
        if (bkb == null)
            bkb = "";
        if (kbg == null)
            kbg = "";

        this.id = id;
        this.name = name;
        this.angle = angle;
        this.faf = faf;

        this.base_damage = base_damage;
        this.bkb = bkb;
        this.kbg = kbg;

        this.preDamage = 0;
        this._throw = name.Contains("Fthrow") || name.Contains("Bthrow") || name.Contains("Uthrow") || name.Contains("Dthrow");
        this.aerial = name.Contains("Uair") || name.Contains("Fair") || name.Contains("Bair") || name.Contains("Dair") || name.Contains("Nair") || name.Contains("Zair");
        this.grab = this.name == "Standing Grab" || this.name == "Dash Grab" || this.name == "Pivot Grab";

        this.landingLag = "-";
        this.autoCancel = new List<CancelCond>();

        this.counterMult = 0;
        var counterRegex = new Regex(@"/ (\([0 - 9] + (\.[0-9]+)*&#215;\))/i");

        this.shieldDamage = 0;
        var shieldDamageRegex = new Regex(@"/\(\+[0-9]+\)/i");

        this.weightDependent = weightDependent;

        if (this.name.Contains("Roll"))
        {
           /* Debug.Log("ROLL!!!!!!!!!");
            Debug.Log("ROLL!!!!!!!!!");
            Debug.Log("ROLL!!!!!!!!!");
            Debug.Log("ROLL!!!!!!!!!");
            Debug.Log("ROLL!!!!!!!!!");
            Debug.Log("ROLL!!!!!!!!!");
            */
        }

        if (!this._throw)
        {
            this.hitboxActive = new HitboxActiveList(hitboxActive);// parseHitbox(hitboxActive);
        }
        else
        {

            this.hitboxActive = new HitboxActiveList("-");// parseHitbox("-");

            this.faf = "";

            var throwdamage = this.base_damage.Split(',');
            if (throwdamage.Length > 0)
            {
                for (var i = 0; i < throwdamage.Length - 1; i++)
                {
                    throwdamage[i] = throwdamage[i].Replace("&#37;", "").Replace("%", "").Replace("&#215;", "x");
                    double value = 0;
                    if (throwdamage[i].Contains("x"))
                    {
                        value = float.Parse(throwdamage[i].Split('x')[0]) * float.Parse(throwdamage[i].Split('x')[1]);
                    }
                    else
                    {
                        value = float.Parse(throwdamage[i]);
                    }
                    this.preDamage += value;

                }
                this.base_damage = throwdamage[throwdamage.Length - 1];
            }
        }

        if (counterRegex.IsMatch(this.base_damage))
        {
            var match = counterRegex.Matches(this.base_damage)[0].Value;
            var c = Regex.Replace(match, @"/[a - z] |\(|\) / gi", "").Replace("&#215;", "");
            this.counterMult = float.Parse(c);
        }

        this.hitboxes = this.hitboxActive;

        var rehitRateRegex = new Regex(@"/(Rehit rate: [0-9]+)/i");
        this.rehitRate = 0;

        if (rehitRateRegex.IsMatch(hitboxActive))
        {
            Regex newRegex = new Regex(@"(/[0 - 9] +/ i");
            this.rehitRate = float.Parse(newRegex.Matches(rehitRateRegex.Matches(hitboxActive)[0].Value)[0].Value);
        }

        var count = 1;
        this.moves = new List<Move>();//[];
        float wbkb = 0f;

        var damage = new List<string>();//<float>();//[];
        var angles = new List<string>();
        var kbgs = new List<string>();
        var bkbs = new List<string>();
        var fkbs = new List<string>();

        if (shieldDamageRegex.IsMatch(this.base_damage))
        {
            Regex replaceRegex = new Regex(@"/\+|\(|\) / gi");
            this.shieldDamage = float.Parse(replaceRegex.Replace(shieldDamageRegex.Matches(this.base_damage)[0].Value, ""));
        }

        if (this.aerial)
        {
            //this.landingLag = float.Parse(landingLag);
            var cancels = autoCancel.Split(',');
            for (var i = 0; i < cancels.Length; i++)
            {
                this.autoCancel.Add(new CancelCond(cancels[i]));
            }
        }

        if (!String.IsNullOrWhiteSpace(this.base_damage) && !String.IsNullOrWhiteSpace(this.bkb) && !String.IsNullOrWhiteSpace(this.kbg) && !string.IsNullOrWhiteSpace(this.angle))
        {
            if (this.base_damage == "-" || this.base_damage == "" || this.base_damage == "?")
            {
                this.base_damage = "";
            }
            if (this.angle == "-" || this.angle == "" || this.angle == "?")
            {
                this.angle = "";
            }
            if (this.bkb == "-" || this.bkb == "" || this.bkb == "?")
            {
                this.bkb = "";
            }
            if (this.kbg == "-" || this.kbg == "" || this.kbg == "?")
            {
                this.kbg = "";
            }
            var hitbox = new HitboxActiveList("");// parseHitbox();

            var ryu_true = new Regex(@"/\(True: [0 - 9] + (\.[0-9]+)*\)/gi");
            var is_ryu_special = false;

            if (ryu_true.IsMatch(this.base_damage))
            {
                is_ryu_special = true;
                this.base_damage = this.base_damage.Split('(')[0] + "/" + this.base_damage.Split(':')[1].Replace(")", "");
            }

            var first_fkb = false;
            if (this.base_damage.Contains("/") || this.bkb.Contains("/") || this.kbg.Contains("/") || this.angle.Contains("/"))
            {
                //multiple hitboxes
                var multi_bkb_wbkb = false;
                var sbkb = 0;
                var multi_bkb = false;
                damage = new List<string>(this.base_damage.Split('/'));
                angles = this.angle.Split('/').ToList();
                kbgs = this.kbg.Split('/').ToList();

                if (shieldDamageRegex.IsMatch(damage[damage.Count - 1]))
                {
                    var replaceRegex = new Regex(@"/\+|\(|\) / gi");
                    this.shieldDamage = float.Parse(replaceRegex.Replace(shieldDamageRegex.Matches(damage[damage.Count - 1])[0].Value, ""));
                }

                if (this.bkb.Contains("W: ") && this.bkb.Contains("B: "))
                {
                    if (this.bkb.Contains(','))
                    {
                        this.bkb = "";
                        foreach (var elem in this.bkb.Replace("/W:", "W:").Replace("/B:", "B:").Split(','))
                        {
                            this.bkb += elem;
                        }

                    }

                    Debug.Log(this.bkb.IndexOf("W:") + " from " + this.bkb);

                   // var w1 = this.bkb.Substring(this.bkb.IndexOf("W:") + 2).Trim();
                    var w = this.bkb.Split(new string[] { "W:" }, StringSplitOptions.None);
                    //Debug.Log(name + "|||" + bkb + " -> " + w[1].Length + " ... " + w[1]);
                    if (w[1].Contains("B:"))
                    {
                        var b = w[1].Split(new string[] { "B:" }, StringSplitOptions.None)[1];
                        var newW = w[1].Split(new string[] { "B:" }, StringSplitOptions.None)[0];
                        fkbs = newW.Trim().Split('/').ToList();
                        bkbs = b.Trim().Split('/').ToList();
                        first_fkb = true;
                    }
                    else
                    {
                        var b = this.bkb.Split(new string[] { "B:" }, StringSplitOptions.None)[1];
                        var newW = b.Split(new string[] { "W:" }, StringSplitOptions.None)[1];
                        b = b.Trim().Split(new string[] { "W:" }, StringSplitOptions.None)[0];
                        fkbs = newW.Trim().Split('/').ToList();
                        bkbs = b.Trim().Split('/').ToList();
                    }
                    var m = Mathf.Max(damage.Count, Mathf.Max(angles.Count, kbgs.Count));

                    for(int i = 0; i < fkbs.Count; i++)
                    {
                        if (string.IsNullOrWhiteSpace(fkbs[i]))
                        {
                            fkbs.RemoveAt(i--);
                        }
                    }
                    if (m == fkbs.Count)
                    {
                        multi_bkb_wbkb = true;
                        if (bkbs.Count == fkbs.Count)
                        {
                            multi_bkb = true;
                        }
                        else
                        {
                            sbkb = int.Parse(bkbs[0]);
                            multi_bkb = false;
                        }
                    }
                }
                else
                {
                    if (this.bkb.Contains("W: "))
                    {
                        var v = this.bkb.Split('/');
                        //Check if W: is on the first hitbox (this often means all hitboxes are WBKB)
                        if (v[0].Contains("W: "))
                        {
                            fkbs = this.bkb.Replace("W:", "").Trim().Split('/').ToList();
                            first_fkb = true;
                        }
                        else
                        {
                            //Splitted in BKB and WBKB, however there is no B: indicator to know which are BKB, so every value before W: will be considered BKB
                            var wb = false;
                            bkbs = new List<string>();// [];
                            fkbs = new List<string>();//[];
                            for (var i = 0; i < v.Length; i++)
                            {
                                if (v[i].Contains("W: "))
                                {
                                    fkbs.Add(v[i].Replace("W:", "").Trim());
                                    wb = true;
                                }
                                else
                                {
                                    if (wb)
                                    {
                                        fkbs.Add(v[i]);
                                    }
                                    else
                                    {
                                        bkbs.Add(v[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        bkbs = this.bkb.Split('/').ToList();
                    }
                }

                var hitbox_count = Mathf.Max(damage.Count, Mathf.Max(angles.Count, Mathf.Max(kbgs.Count, multi_bkb_wbkb ? fkbs.Count : (fkbs.Count + bkbs.Count))));
                var set_count = 0;
                var base_count = 0;
                for (var i = 0; i < hitbox_count; i++)
                {
                    var hitbox_name = this.name;
                    if (!is_ryu_special)
                    {
                        if (!ignore_hitboxes)
                        {
                            hitbox_name += " (Hitbox " + (i + 1) + ")";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            hitbox_name = "True " + hitbox_name;
                        }
                    }

                    var d = i < damage.Count ? damage[i] : damage[damage.Count - 1];
                    var a = i < angles.Count ? angles[i] : angles[angles.Count - 1];
                    var k = i < kbgs.Count ? kbgs[i] : kbgs[kbgs.Count - 1];
                    float b = 0f;

                    double s = 0f;
                    if (this.shieldDamage == 0)
                    {
                        if (shieldDamageRegex.IsMatch(d))
                        {
                            Regex replaceRegex = new Regex(@"/\+|\(|\) / gi");

                            s = double.Parse(replaceRegex.Replace(shieldDamageRegex.Matches(d)[0].Value, ""));
                        }
                    }
                    else
                    {
                        s = this.shieldDamage;

                    }

                    if (multi_bkb_wbkb)
                    {
                        if (multi_bkb)
                        {
                            wbkb = float.Parse(fkbs[set_count]);
                            b = float.Parse(bkbs[set_count]);
                        }
                        else
                        {
                            wbkb = float.Parse(fkbs[set_count]);
                            b = sbkb;
                        }
                        set_count++;
                    }
                    else
                    {
                        if (first_fkb)
                        {
                            if (set_count < fkbs.Count)
                            {
                                b = 0;// "0";
                                foreach(var fkb in fkbs)
                                {
                                   // Debug.Log("fkb: " + fkb);
                                }
                                wbkb = float.Parse(fkbs[set_count]);
                                set_count++;
                            }
                            else
                            {
                                if (bkbs.Count > 0)
                                {
                                    b = float.Parse(bkbs[base_count]);
                                    wbkb = 0;// "0";
                                    base_count++;
                                }
                                else
                                {
                                    b = 0;// "0";
                                    wbkb = float.Parse(fkbs[fkbs.Count - 1]);
                                }
                            }
                        }
                        else
                        {
                            if (base_count < bkbs.Count)
                            {
                                b = float.Parse(bkbs[base_count]);
                                wbkb = 0;//Count "0";
                                base_count++;
                            }
                            else
                            {
                                if (fkbs.Count > 0)
                                {
                                    b = 0;// "0";
                                    wbkb = float.Parse(fkbs[set_count]);
                                    set_count++;
                                }
                                else
                                {
                                    b = float.Parse(bkbs[bkbs.Count - 1]);
                                    wbkb = 0;// "0";
                                }
                            }
                        }
                    }

                    this.moves.Add(new Move(this.id, i, hitbox_name, this.name, d, a, b.ToString(), k, wbkb, this.hitboxes, this.faf, this.landingLag, this.autoCancel, this.preDamage, this.counterMult, this.rehitRate, s, this.weightDependent));
                    if (ignore_hitboxes)
                    {
                        return;
                    }
                }
            }
            else
            {
                //single hitbox
                if (this.bkb.Contains("W: ") && this.bkb.Contains("B: "))
                {
                    this.bkb = "";
                    foreach (var s in this.bkb.Replace("/W:", "W:").Replace("/B:", "B:").Split(','))
                    {
                        this.bkb += s;
                    }

                    var w = this.bkb.Split(new string[] { "W:" }, StringSplitOptions.None);
                    if (w[1].Contains("B:"))
                    {
                        var b = w[1].Split(new string[] { "B:" }, StringSplitOptions.None)[1];
                        var newW = w[1].Split(new string[] { "B:" }, StringSplitOptions.None)[0];
                        fkbs = newW.Trim().Split('/').ToList();
                        bkbs = b.Trim().Split('/').ToList();
                        first_fkb = true;
                    }
                    else
                    {
                        var b = this.bkb.Split(new String[] { "B:" }, StringSplitOptions.None)[1];
                        var newW = b.Split(new String[] { "W:" }, StringSplitOptions.None)[1];
                        b = b.Trim().Split(new String[] { "W:" }, StringSplitOptions.None)[0];
                        fkbs = newW.Trim().Split('/').ToList();
                        bkbs = b.Trim().Split('/').ToList();
                    }
                    this.bkb = bkbs[0];
                    wbkb = float.Parse(fkbs[0]);
                }
                else
                {
                    if (bkb.Contains("W: "))
                    {
                        this.bkb = "0";
                        wbkb = float.Parse(bkb.Replace("W: ", ""));
                    }
                }
                if (this.base_damage == "" && this.angle == "" && this.bkb == "" && this.kbg == "")
                {
                    if (this.grab)
                    {
                        this.moves.Add(new Move(this.id, 0, this.name, this.name, "NaN", "NaN", "NaN", "NaN", float.NaN, this.hitboxes, this.faf, this.landingLag, this.autoCancel, this.preDamage, this.counterMult, this.rehitRate, this.shieldDamage, this.weightDependent));
                    }
                    else
                    {
                        this.moves.Add(new Move(this.id, 0, this.name, this.name, "NaN", "NaN", "NaN", "NaN", float.NaN, this.hitboxes, this.faf, this.landingLag, this.autoCancel, this.preDamage, this.counterMult, this.rehitRate, this.shieldDamage, this.weightDependent).invalidate());
                    }
                }
                else
                {
                    this.moves.Add(new Move(this.id, 0, this.name, this.name, this.base_damage, this.angle, this.bkb, this.kbg, wbkb, this.hitboxes, this.faf, this.landingLag, this.autoCancel, this.preDamage, this.counterMult, this.rehitRate, this.shieldDamage, this.weightDependent));
                }
            }

        }
        else
        {
            this.moves.Add(new Move(this.id, 0, this.name, this.name, "NaN", "NaN", "NaN", "NaN", float.NaN, new HitboxActiveList(new List<HitboxActiveFrames>(new HitboxActiveFrames[] { new HitboxActiveFrames(float.NaN, float.NaN) })), "NaN",this.landingLag, this.autoCancel, 0, this.counterMult, this.rehitRate, this.shieldDamage, this.weightDependent).invalidate());
        }
    }
}

[Serializable]
public class CancelCond : SmashCalculatorJSONClass
{
    float? value;
    string type;
    List<float> values;
    Func<float, bool> eval;
    Func<string> print;

    public CancelCond(string cond)
    {
        string rawValue = cond;
        if (cond.Contains("&gt;"))
        {
            //Greater than
            type = ">=";
            value = float.Parse(cond.Replace("&gt;", ""));
            rawValue = rawValue.Replace("&gt;", "&ge;");
            eval = (value) => {
                return this.value <= value;
            };
            print = () =>
            {
                return this.value + ">";
            };
        }
        else
        {
            Regex r = new Regex(@"/[0 - 9] +\-[0 - 9] +/ i");
            if (r.IsMatch(cond))
            {
                //Range
                this.type = "-";
                this.value = null;
                this.values = new List<float>(new float[] { float.Parse(cond.Split('-')[0]), float.Parse(cond.Split('-')[1]) });
                this.print = () => {
                    return this.values[0] + "-" + this.values[1];
                };
                this.eval = (value) =>
                {
                    return value >= this.values[0] && value <= this.values[1];
                };
            }
            else
            {
                this.type = "empty";
                this.value = null;
                this.values = null;
                this.print = () => {
                    return "-";
                };

                eval = (value) =>
                {
                    return false;
                };
            }
        }
    }
}

[Serializable]
public class HitboxActiveFrames : SmashCalculatorJSONClass
{
    public float start = -1;
    public float end = -1;

    public float Range()
    {
        return end - start;
    }

    public HitboxActiveFrames(string val)
    {
        if (val != null && !val.Contains('-'))
        {
            float ret;
            if (float.TryParse(val.Trim(), out ret))
            {
                Parse(ret, ret);
            }
            return;
        }
        var _start = val.Split('-').Length == 0 ? "0" : val.Split('-')[0];
        var _end = val.Split('-').Length <= 1 ? _start : val.Split('-')[1];
        if (String.IsNullOrWhiteSpace(_start))// == undefined)
        {
            _start = "0";
        }
        if (String.IsNullOrWhiteSpace(_end))// == undefined)
        {
            _end = _start;
        }

        //Debug.Log(val + " -> " + _start + ":" + _end);
        float start;
        float end;
        Parse(float.TryParse(_start, out start) ? start : 1, float.TryParse(_end, out end) ? end : float.MaxValue);
    }

    void Parse(float _start, float _end)
    {
        start = _start;
        end = _end;
    }

    public HitboxActiveFrames(float _start, float _end)
    {
        Parse(_start, _end);
    }
}

[Serializable]
public class UnparsedMove : SmashCalculatorJSONClass
{
    public string Name;
    public bool Projectile;
    public string InstanceId;
    public int OwnerId;
    public ushort AnimationId;
    public string Owner;
    public string HitboxActive;
    public string FirstActionableFrame;
    public string BaseDamage;
    public string Angle;
    public string BaseKnockBackSetKnockback;
    public string LandingLag;
    public string AutoCancel;
    public string KnockbackGrowth;
    public string MoveType;
    public bool IsWeightDependent;
    public List<MoveLink> Links;
}
