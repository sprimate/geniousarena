using Algorithms.Sorting;
using DataStructures.Lists;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[RequireComponent(typeof(SphereCollider))]
public class TargetSphereCollider : MonoBehaviour
{
    Type typeToTrack;
    SphereCollider _sc;
    new SphereCollider collider { get { if (_sc == null) _sc = GetComponent<SphereCollider>(); return _sc; } }
    IList<Component> objectsInCollider = new List<Component>();
    [ReadOnly] Vector3 size;
    public void Awake()
    {
        collider.isTrigger = true;
    }

    public void SetSize(Vector3 _size)
    {
        size = _size;
        float max = Mathf.Max(Mathf.Max(size.x), Mathf.Max(size.y, size.z));
        collider.radius = max / 2f;
    }

    //https://stackoverflow.com/a/17770614
    bool IsWithinElipsoid(Vector3 position)
    {
        position -= transform.position;
        Vector3 ellipsoid = size/2f;
        var ret = Mathf.Pow(position.x / ellipsoid.x, 2)  + Mathf.Pow(position.y / ellipsoid.y, 2) + Mathf.Pow(position.z / ellipsoid.z, 2);// <= 1;
        return ret <= 1;
    }

    SortedList sortedList;
    int? frameSorted;
    public SortedList GetTargetsByDistance<T>() where T : MonoBehaviour
    {
       
        sortedList = new SortedList(objectsInCollider.Count);
        foreach(var genericObj in objectsInCollider)
        {
            var obj = (MonoBehaviour)genericObj;
            if (IsWithinElipsoid(obj.transform.position))
            {
                (sortedList as SortedList<float, T>).Add(Vector3.Distance(transform.position, obj.transform.position), (T) genericObj);
            }
        }

        frameSorted = GeniousSettings.fixedFrame;
        return sortedList;
    }

    //Using insertion sort because the list should be "nearly sorted" (if its sorted frequently).
    //TODO - if not sorted in a while, do the generic sorting method
    //https://www.toptal.com/developers/sorting-algorithms/nearly-sorted-initial-order
    public T GetTargetAtIndexByDistance<T>(int index) where T : Component
    {
        if (index >= objectsInCollider.Count)
        {
            return null;
        }

        if (frameSorted != GeniousSettings.fixedFrame)
        {
            objectsInCollider.InsertionSort<Component>(Comparer<Component>.Create((x, y) =>
            {
                if (!IsWithinElipsoid(x.transform.position))
                {
                    return 1;//send to the back
                }

                if (!IsWithinElipsoid(y.transform.position))
                {
                    return -1;
                }

                var difference = Vector3.Distance(x.transform.position, transform.position) - Vector3.Distance(y.transform.position, transform.position);
                if (difference < 0)
                {
                    return -1;
                }
                else if (difference == 0f)
                {
                    return 0;
                }

                return 1;
            }));

            frameSorted = GeniousSettings.fixedFrame;
        }

        var ret = objectsInCollider[index];
        var str = "";
        for (int i=0; i< objectsInCollider.Count; i++)// in objectsInCollider)
        {
            str += i + ": " + objectsInCollider[i] + ",";
        }

        return IsWithinElipsoid(ret.transform.position) ? (T) ret : null;
    }

    public void LateUpdate()
    {
        ResetScale();//Scale needs to remain constant regardless of parentage
    }

    void ResetScale()
    {
        Transform parent = transform.parent;
        int index = transform.GetSiblingIndex();
        transform.SetParent(null);
        transform.localScale = Vector3.one;
        transform.SetParent(parent);
        transform.SetSiblingIndex(index);
    }

    public static TargetSphereCollider Create<T>(Vector3 _size) where T : MonoBehaviour
    {
        GameObject go = new GameObject("Targetable Trigger");
        var ret = go.AddComponent<TargetSphereCollider>();
        ret.SetSize(_size);
        ret.typeToTrack = typeof(T);
        return ret;
    }

    public void OnTriggerEnter(Collider other)
    {
        Component obj = other.GetComponent(typeToTrack);
        if (obj != null)
        {
            objectsInCollider.Add(obj);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Component obj = other.GetComponent(typeToTrack);
        if (obj != null && objectsInCollider.Contains(obj))
        {
            objectsInCollider.Remove(obj);
        }
    }
}

/// <summary>
/// Comparer for comparing two keys, handling equality as beeing greater
/// Use this Comparer e.g. with SortedLists or SortedDictionaries, that don't allow duplicate keys
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class DuplicateKeyComparer<TKey>
                :
             IComparer<TKey> where TKey : IComparable
{
    #region IComparer<TKey> Members

    public int Compare(TKey x, TKey y)
    {
        int result = x.CompareTo(y);

        if (result == 0)
            return 1;   // Handle equality as beeing greater
        else
            return result;
    }

    #endregion
}