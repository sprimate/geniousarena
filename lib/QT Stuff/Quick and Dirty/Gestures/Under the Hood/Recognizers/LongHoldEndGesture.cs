﻿using UnityEngine;
namespace QuickAndDirty.Gestures {
    public class LongHoldEndGesture : GestureRecognizer
    {
        public float LongHoldEndTime;
        public Vector2 maxDelta;

        public LongHoldEndGesture()
        {
            LongHoldEndTime = QADGestures.instance.longHoldTime;
            maxDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture hold)
        {
            return hold.phase == TouchPhase.Ended
                && hold.totalDeltaPosition.sqrMagnitude <= maxDelta.sqrMagnitude
                && hold.timeHeld >= LongHoldEndTime;
        }
    }

}