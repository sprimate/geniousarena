using UnityEngine;
using System.Linq;

namespace QuickAndDirty.Gestures {
    public abstract class GestureRecognizer
    {
        protected int _listenerCount;

        //public delegate void GestureEvent (Gesture touchInfo);

        protected event GestureEvent  _onGestureEvent;
        public event GestureEvent OnGestureEvent
        {
            add
            {
                _onGestureEvent += value;
                ++_listenerCount;
            }
            remove
            {
                if (_onGestureEvent != null && _onGestureEvent.GetInvocationList().Contains(value))
                {
                    _onGestureEvent -= value;
                    --_listenerCount;
                }
            }
        }

        public GestureRecognizer()
        {
            _listenerCount = 0;
        }

        /*
         * For recognizing a gesture and firing related event
         */
        public virtual void UpdateRecognizer(Gesture[] gestures)
        {
            if (_listenerCount > 0)
            {
                foreach (Gesture g in gestures)
                {
                    if (!g.dirty)
                    {
                        if (Recognize(g))
                        {
                            _onGestureEvent(g);
                        }
                    }
                }
            }
        }

        /*
         * Returns true if the gesture matches
         */
        public abstract bool Recognize(Gesture touch);
    }

    [System.Serializable]
    public class RecognizerSettings {
        public int maxFingers = 5;
        public float dragHoldTime = .15f;
        public Vector2 minDragDelta = new Vector2(1, 1);
        public float maxFlickTime = .3f;
        public float longHoldTime = .5f;
        public Vector2 minFlickDelta = new Vector2(2, 2);
    }
}