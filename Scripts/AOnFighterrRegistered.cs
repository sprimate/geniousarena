using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AOnFighterRegistered : MonoBehaviour
{
    [Range(1, 8)]
    public int playerNumber = 1;
    protected FighterController fighter { get; private set; }
    // Update is called once per frame
    protected virtual void Update()
    {
        if (fighter == null)
        {
            foreach (var f in GeniousSettings.instance.fighters)
            {
                if (f.playerInput?.playerNumber == playerNumber)
                {
                    fighter = f;
                    OnFighterRegistered(fighter);
                }
            }
        }
    }

    protected abstract void OnFighterRegistered(FighterController _fighter);
}
