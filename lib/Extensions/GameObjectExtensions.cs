using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

namespace UnityEngine
{
    public static class GameObjectExt
    {

        public static string GetHierarchyPath(this GameObject o)
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append("/" + o.name);
            if (o.transform.parent != null)
            {
                Transform p = o.transform.parent;
                while (p != null)
                {
                    builder.Insert(0, "/" + p.name);
                    p = p.parent;
                }
            }
            return builder.ToString();
        }


        private static Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();

        private static bool sceneChangeSubscribed;

        public static GameObject Find(string name, bool debugLogError = true, Action<string> OnError = null)
        {
            if (!sceneChangeSubscribed)
            {
                sceneChangeSubscribed = true;
                SceneManager.sceneLoaded += FlushCache;
            }

            if (cache.ContainsKey(name) && cache[name] != null)
                return cache[name];

            GameObject found = GameObject.Find(name);
            if (found == null)
            {
                if (debugLogError)
                    Debug.LogErrorFormat("Could not find GameObject named '{0}'", name);

                if (OnError != null)
                    OnError.Invoke(name);
            }
            else
            {
                if (!cache.ContainsKey(name))
                    cache.Add(name, found);

                cache[name] = found;
            }

            return found;
        }

        private static void FlushCache(Scene arg0, LoadSceneMode arg1)
        {
            cache = new Dictionary<string, GameObject>();
        }

        public static Transform FindTransform(
            string name, bool debugLogError = true, Action<string> OnError = null)
        {
            GameObject found = Find(name, debugLogError, OnError);
            if (found)
                return found.transform;
            else
                return null;
        }

        public static T FindorCreateObjectOfType<T>() where T : MonoBehaviour
        {
            T t = GameObject.FindObjectOfType<T>();
            if (t == null)
            {
                var go = new GameObject("[" + typeof(T).ToString() + "]");
                t = go.AddComponent<T>();
            }

            return t;
        }

        public static bool IsPrefab(this MonoBehaviour obj)
        {
            return obj.gameObject.scene.rootCount == 0;
        }


        public static void DontDestroyOnLoad(this GameObject g)
        {
            g.transform.SetParent(null);
            UnityEngine.Object.DontDestroyOnLoad(g);
        }

        public static string GetFullPath(this Transform transform)
        {
            string path = transform.name;
            while (transform.parent != null)
            {
                transform = transform.parent;
                path = transform.name + "/" + path;
            }
            return transform.gameObject.scene.name + "/" + path;
        }

        public static void SetLayerRecursive(this GameObject gameObject, int layer)
        {
            if (gameObject == null)
                return;

            gameObject.layer = layer;
            foreach (Transform t in gameObject.transform)
            {
                t.gameObject.SetLayerRecursive(layer);
            }
        }

        public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
        {
            transform.localScale = Vector3.one;
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
        }

        public static void SetLayerRecursive(this GameObject gameObject, string layerName)
        {
            SetLayerRecursive(gameObject, LayerMask.NameToLayer(layerName));
        }

        public static T AddOrGetComponent<T>(this GameObject gameObject) where T : Component
        {
            T t = gameObject.GetComponent<T>();
            if (t == null)
            {
                t = gameObject.AddComponent<T>();
            }
            return t;
        }

        public static bool HasComponent<T>(this GameObject gameObject) where T : MonoBehaviour
        {
            return gameObject.GetComponent<T>() != null;
        }

        public static T GetOrAddComponent<T>(this MonoBehaviour go) where T : MonoBehaviour
        {
            return go.gameObject.AddOrGetComponent<T>();
        }

        public static T GetOrAddComponent<T>(this GameObject go) where T : Component
        {
            return go.AddOrGetComponent<T>();
        }

        public static Object GetOrAddComponent(this GameObject go, Type t)
        {
            Object ret = go.GetComponent(t);
            if (ret == null)
            {
                ret = go.AddComponent(t);
            }
            return ret;
        }

        public static T AddOrGetComponent<T>(this Transform transform) where T : MonoBehaviour
        {
            return transform.gameObject.AddOrGetComponent<T>();
        }

        public static T GetOrAddComponent<T>(this Transform transform) where T : Component
        {
            return transform.gameObject.AddOrGetComponent<T>();
        }

        public static void SetXSizeDelta(this RectTransform t, float x)
        {
            t.sizeDelta = new Vector2(x, t.sizeDelta.y);
        }
        public static void SetYSizeDelta(this RectTransform t, float y)
        {
            t.sizeDelta = new Vector2(t.sizeDelta.x, y);
        }

        public static void SetSizeDelta(this RectTransform t, Vector2 size)
        {
            t.SetXSizeDelta(size.x);
            t.SetYSizeDelta(size.y);
        }

        public static void SetXLocalRotation(this Transform t, float x)
        {
            t.localRotation = Quaternion.Euler(x, t.localRotation.eulerAngles.y, t.localRotation.eulerAngles.z);
        }

        public static void SetYLocalRotation(this Transform t, float y)
        {
            t.localRotation = Quaternion.Euler(t.localRotation.eulerAngles.x, y, t.localRotation.eulerAngles.z);
        }

        public static void SetZLocalRotation(this Transform t, float z)
        {
            t.localRotation = Quaternion.Euler(t.localRotation.eulerAngles.x, t.localRotation.eulerAngles.y, z);
        }

        public static void SetXRotation(this Transform t, float x)
        {
            t.rotation = Quaternion.Euler(x, t.rotation.eulerAngles.y, t.rotation.eulerAngles.z);
        }

        public static void SetYRotation(this Transform t, float y)
        {
            t.rotation = Quaternion.Euler(t.rotation.eulerAngles.x, y, t.rotation.eulerAngles.z);
        }

        public static void SetZRotation(this Transform t, float z)
        {
            t.rotation = Quaternion.Euler(t.rotation.eulerAngles.x, t.rotation.eulerAngles.y, z);
        }

        public static void SetXLocalScale(this Transform t, float x)
        {
            Vector3 newScale = new Vector3(x, t.localScale.y, t.localScale.z);
            t.localScale = newScale;
        }

        public static void SetYLocalScale(this Transform t, float y)
        {
            Vector3 newScale = new Vector3(t.localScale.x, y, t.localScale.z);
            t.localScale = newScale;
        }

        public static void SetZLocalScale(this Transform t, float z)
        {
            Vector3 newScale = new Vector3(t.localScale.x, t.localScale.y, z);
            t.localScale = newScale;
        }

        public static void SetXPosition(this Transform t, float x)
        {
            Vector3 newPosition = new Vector3(x, t.position.y, t.position.z);
            t.position = newPosition;
        }

        public static void SetYPosition(this Transform t, float y)
        {
            Vector3 newPosition = new Vector3(t.position.x, y, t.position.z);
            t.position = newPosition;
        }

        public static void SetZPosition(this Transform t, float z)
        {
            Vector3 newPosition = new Vector3(t.position.x, t.position.y, z);
            t.position = newPosition;
        }
    }

}

