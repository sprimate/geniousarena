using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterAnimatorScript : StateMachineBehaviour
{
    protected FighterData fighterData;
    protected FighterController controlla;
    protected FighterController fc { get { return controlla; } set { controlla = value;} }
    protected GameObject gameObject;
    protected Transform transform;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        fighterData = animator.gameObject.GetComponentInParent<IFighterDataHolder>().fighterData;
        controlla = animator.gameObject.GetComponentInParent<FighterController>();
        transform = animator.transform;
        gameObject = animator.gameObject;
        fc?.OnStateEnter(this);

    }

    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateMove(animator, stateInfo, layerIndex);
        fc?.OnStateMove(this);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateMove(animator, stateInfo, layerIndex);
        fc?.OnStateUpdate(this);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        fc?.OnStateExit(this);
    }
}