﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DictionaryExtensions
{
    public static T2 GetOrDefault<T1, T2>(this Dictionary<T1, T2> dict, T1 key)
    {
        if (dict.ContainsKey(key))
        {
            return dict[key];
        }

        return default(T2);
    }
}
