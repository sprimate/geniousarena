using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Required to add the suffix ".bytes" to your binary
/// </summary>
[Serializable]
public class BinaryAsset
{
    public TextAsset backingTextAsset;// { get; protected set; }
    public BinaryAsset(TextAsset textAsset)
    {
        backingTextAsset = textAsset;
    }
    public string name { get { return backingTextAsset.name; } }
    public byte[] bytes { get { return backingTextAsset.bytes; } }
    /// <summary>
    /// 
    /// </summary>
    /// <returns>new file path</returns>
    public string WriteBinaryToPersistentDataPath()
    {
        var filePath = Path.Combine(Application.persistentDataPath, name);
        filePath = filePath.Replace('/', Path.DirectorySeparatorChar).Replace('\\', Path.DirectorySeparatorChar);
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }

        File.WriteAllBytes(filePath, bytes);
        return filePath;
    }
}
