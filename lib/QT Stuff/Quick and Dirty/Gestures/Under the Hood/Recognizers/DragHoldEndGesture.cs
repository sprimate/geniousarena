using UnityEngine;
namespace QuickAndDirty.Gestures {
    public class DragHoldEndGesture : GestureRecognizer
    {
        public float dragHoldTime;
        public Vector2 minDelta;

        public DragHoldEndGesture()
            : base()
        {
            dragHoldTime = QADGestures.instance.dragHoldTime;
            minDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture gesture)
        {
            return gesture.phase == TouchPhase.Ended
                && gesture.timeHeld > dragHoldTime;
        }
    }
}