using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class UiNavigationGrid : MonoBehaviour
{
    public string Name;

	public TArray[] cols;

	[System.Serializable]
	public class TArray
	{
		public Transform[] rows;
	}

    public void AddColumn(IEnumerable<Transform> rows)
    {
        TArray newCol = new TArray();
        newCol.rows = rows.ToArray<Transform>();

        if (cols == null)
        {
            cols = new TArray[1];
        }
        else
        {
            List<TArray> extendedCols = new List<TArray>(cols);
            extendedCols.Add(null);
            cols = extendedCols.ToArray<TArray>();
        }
        cols[cols.Length - 1] = newCol;
    }

	private int row = 0;
	private int col = 0;
	public Transform GetNext(Direction direction)
	{
		int[] colRow = GetColRow(col, row, direction);
		col = colRow[0];
		row = colRow[1];
						
		return cols[col].rows[row];
	}

	public void Clear()
	{
		cols = new TArray[0];
	}


	private int[] GetColRow(int currentCol, int currentRow, Direction direction)
	{
		int col = currentCol;
		int row = currentRow;
			
		bool isValidSelection = true;
		int maxTries = 0;
		for (int i = 0; i < cols.Length; i++)
		{
			for (int j = 0; j < cols[i].rows.Length; j++)
			{
				maxTries++;
			}
		}

		int tries = 0;
		do
		{
			isValidSelection = true;

			switch (direction)
			{
				case Direction.None:
					break;
				case Direction.Left:
					col = (col - 1) % cols.Length;
					break;
				case Direction.Right:
					col = (col + 1) % cols.Length;
					break;
				case Direction.UpLeft:
				case Direction.UpRight:
				case Direction.Up:
					row = (row - 1) % cols[col].rows.Length;
					break;
				case Direction.DownLeft:
				case Direction.DownRight:
				case Direction.Down:
					row = (row + 1) % cols[col].rows.Length;
					break;
				case Direction.Reset:
					row = 0;
					col = 0;
					break;
				default:
					throw new System.NotImplementedException(direction.ToString());
			}

			if (col >= cols.Length)
				col = cols.Length - 1;

			if (col < 0)
				col = cols.Length - 1;

			if (col < 0)
				col = 0;

			if (row >= cols[col].rows.Length)
				row = cols[col].rows.Length - 1;

			if (row < 0)
				row = cols[col].rows.Length - 1;

			if (row < 0)
				row = 0;


			if (direction != Direction.None && direction != Direction.Reset)
			{
				if (cols[col].rows[row] == null || lastSeleted == cols[col].rows[row])
				{
					isValidSelection = false;
					//Debug.LogFormat("({0},{1}) {2} is lastSelected", col, row, cols[col].rows[row]);
				}
			}

			if (cols[col].rows[row] == null || !cols[col].rows[row].gameObject.activeInHierarchy)
			{
				isValidSelection = false;
				//Debug.LogFormat("({0},{1}) {2} is not active", col, row, cols[col].rows[row]);
			}
			else
			{
				var selectable = cols[col].rows[row].GetComponent<Selectable>();

				if (selectable != null && !selectable.interactable)
				{
					isValidSelection = false;
					//Debug.LogFormat("({0},{1}) {2} is not interactable", col, row, cols[col].rows[row]);
				}
			}

			if (!isValidSelection)
			{
				tries++;

				// Determine the next direction.
				switch (direction)
				{
					case Direction.Left:
						if (col == cols.Length - 1) // looped back to the end, move down
							direction = Direction.Down;
						break;
					case Direction.Right: // looped back to the left, move down
						if (col == 0)
							direction = Direction.Down;
						break;
					case Direction.Up:
					case Direction.UpLeft:
					case Direction.UpRight: // looped to the bottom, move right
						if (row == cols[col].rows.Length - 1)
							direction = Direction.Right;
						break;
					case Direction.Down:
					case Direction.DownLeft:
					case Direction.DownRight: //looped back to the top, move right
						if (row == 0)
							direction = Direction.Right;
						break;
				}

				//Debug.Log("new direction: " + direction.ToString());
			}

				
		}
		while (!isValidSelection && tries <= maxTries);

		if (tries >= maxTries)
		{
			Debug.LogError("Could not find a valid control for navigation on " + name, gameObject);
			col = row = 0;
		}

		lastSeleted = cols[col].rows[row];
        //Debug.Log("lastSelected: " + lastSeleted.GetFullPath());
		return new int[] { col, row };
	}


	[Header("In-editor debugging")]
	[SerializeField]
	private Direction d = Direction.None;
	[SerializeField]
#pragma warning disable 0414
	private Transform lastSeleted;
#pragma warning restore
#if UNITY_EDITOR
	private void Update()
	{
		if (d == Direction.None)
			return;

		GetNext(d);

		d = Direction.None;
	}
#endif
}