using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[ExecuteBefore(typeof(GeniousSettings))]
[ExecuteBefore(typeof(PlayerInput))]
public class GeniousInputManager : MonoSingleton<GeniousInputManager>
{
    public static readonly string[] NOT_ANIMATOR_ACTION_CATEGORY_NAME = { "NotAnimatorAction", "Xbox Ref Category" };
    // public SmashControls controls;

    //public Rewired.InputAction Jump;
    public PlayerInput startingInactivePlayer;
    public UnityEvent onInactivePlayerActivated;
    Dictionary<Type, GeniousRewiredMangerContainer> inputManagers = new Dictionary<Type, GeniousRewiredMangerContainer>();
    GeniousRewiredMangerContainer currentInputManager;
    public Action<PlayerInput> onPlayerRegistered;
    public Action<GeniousRewiredMangerContainer> OnInputManagerSwitched;

    protected override void Awake()
    {
        base.Awake();
        foreach(var grim in Resources.LoadAll<GeniousRewiredMangerContainer>(""))
        {
            InitGeniousRewiredManagerContainer(grim);
        }
    }

    void InitGeniousRewiredManagerContainer(GeniousRewiredMangerContainer grim)
    {
        if (grim.actions == null)
        {
            Debug.LogError("Could not load GeniousRewiredInputManager " + grim.name + " - no rewiredInputActionsType applied. Do you need to create the script?");
        }
        else
        {
            inputManagers[grim.actions.GetType()] = grim;
            //currentInputManager = GameObject.Instantiate(inputManagers[grim.actions.GetType()], transform);
        }
    }

    public bool SetRewiredInputManager(Type t)
    {
        //Already instantiated this type. success
        if (currentInputManager != null && currentInputManager.GetType() == t)
        {
            return true;
        }
        if (!t.IsSubclassOf(typeof(RewiredInputActions)))
        {
            Debug.LogError("Type [" + t + "] is not a subclass of [" + typeof(RewiredInputActions) + "]");
        }
        else if (inputManagers.ContainsKey(t))
        {
            if (currentInputManager != null)
            {
                DestroyImmediate(currentInputManager.gameObject);
            }

            foreach(var pi in playersDict.Values)
            {
                pi.ClearAndInitializeCallbacks();
            }

            currentInputManager = GameObject.Instantiate(inputManagers[t],transform);
            var newDict = new Dictionary<Player, PlayerInput>();
            GenericCoroutineManager.instance.RunInFrames(1, () =>
            {
                Action afterTheIterations = () => { };
                foreach (var newPlayer in ReInput.players.AllPlayers)
                {
                    foreach (var newJoystick in newPlayer.controllers.Joysticks)
                    {
                        foreach(var oldPlayer in activePlayers)
                        {
                            foreach(var customController in oldPlayer.controllers.CustomControllers)
                            {
                                if (GetJoystickUniqueId(newJoystick) == customController?.tag)
                                {
                                    afterTheIterations += () =>
                                    {
                                        newDict[newPlayer] = playersDict[oldPlayer];
                                        newDict[newPlayer].rewiredPlayer = newPlayer;
                                        RegisterCustomController(newPlayer, customControllerMaps[oldPlayer]);
                                        customControllerMaps.Remove(oldPlayer);
                                    };
                                }
                            }
                        }
                    }
                }

                afterTheIterations?.Invoke();
                playersDict = newDict;
                activePlayers = new HashSet<Player>(playersDict.Keys);
                OnInputManagerSwitched?.Invoke(currentInputManager);
            });
            return true;
        }
        else
        {
            Debug.LogError("Cannot set RewiredInputManager. no input manager compatible with type [" + t + "] found");
        }

        return false;
    }

    public bool SetRewiredInputManager<T>() where T : RewiredInputActions
    {
        return SetRewiredInputManager(typeof(T));
    }

    public bool SetRewiredInputManager(GeniousRewiredMangerContainer grim)
    {
        InitGeniousRewiredManagerContainer(grim);
        return SetRewiredInputManager(grim.actions.GetType());
    }

    public T Actions<T>() where T : RewiredInputActions
    {
        if (currentInputManager == null)
        {
            Debug.LogError("Can't retrieve actions: no [currentInputManager] loaded");
        }
        if (!inputManagers.ContainsKey(typeof(T)))
        {
            Debug.LogError("Can't retrieve actions - no input manager of type " + typeof(T) + " found. Is the prefab in Resources?");
        }
        else if (typeof(T) != currentInputManager.actions.GetType())
        {
            Debug.LogError("Can't retrieve actions - current inputManager [" + currentInputManager + "] does not match given type [" + typeof(T) + ". The current type is [" + currentInputManager?.actions?.GetType() + "]");
        }
        else
        {
            return currentInputManager.actions as T;
        }

        return null;
    }

    Coroutine listen;
    public void ListenForPlayers(Action<PlayerInput> _onPlayerRegistered, InputAction buttonForRegistration = null)
    {
        if (listen != null)
        {
            StopListeningForPlayers();
        }

        onPlayerRegistered += _onPlayerRegistered;
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            listen = StartCoroutine(ListenForPlayersCoroutine(buttonForRegistration));
        });
    }

    public void StopListeningForPlayers()
    {
        if (listen != null)
        {
            StopCoroutine(listen);
        }

        listen = null;
    }

    public void UnregisterPlayer(PlayerInput p)
    {
        if (activePlayers.Contains(p.rewiredPlayer))
        {
            playersDict.Remove(p.rewiredPlayer);
            activePlayers?.Remove(p.rewiredPlayer);
            nextPlayerNumber = p.playerNumber;
        }
        else
        {
            Debug.Log("WARNING: Trying to unassign player [" + p + "] but they may have never been assigned.");
        }
    }

    string GetJoystickUniqueId(Joystick j)
    {
        return j.unityId + "|" + j.systemId;
    }

    Dictionary<Player, CustomControllerMap> customControllerMaps = new Dictionary<Player, CustomControllerMap>();
    public void RegisterCustomController(Player player, CustomControllerMap mapToCopy = null)
    {
        foreach (var joystick in player.controllers.Joysticks)
        {

            if (joystick.hardwareTypeGuid == Guid.Empty)
            {
                //Debug.Log(player.name + " - Unrecognized joystick! " + joystick.hardwareIdentifier + " and " + joystick.hardwareName);
            }

            CustomController customController = ReInput.controllers.CreateCustomController(0, GetJoystickUniqueId(joystick));
            player.controllers.AddController(customController, true);
            customControllerMaps[player] = new CustomControllerMap(customController, joystick, mapToCopy);
        }

        player.controllers.ClearControllersOfType(ControllerType.Joystick);
        player.controllers.ClearControllersOfType(ControllerType.Keyboard);
        player.controllers.ClearControllersOfType(ControllerType.Mouse);
    }

    bool IsActiveUpdateLoop(Rewired.Config.UpdateLoopSetting updateLoopSetting)
    {
        var currentLoopSetting = currentInputManager?.rewiredInputManager?.userData?.ConfigVars?.updateLoop;
        return currentLoopSetting.HasValue && currentLoopSetting.Value == updateLoopSetting;
    }

    int nextPlayerNumber = 1;
    public HashSet<Player> activePlayers { get; protected set; } = new HashSet<Player>();
    Dictionary<Player, PlayerInput> playersDict = new Dictionary<Player, PlayerInput>();
    IEnumerator ListenForPlayersCoroutine(InputAction buttonForRegistration )
    {
        //currentInputManager.rewiredInputManager.userData.AddActionCategory();
        //int newMap = currentInputManager.rewiredInputManager.userData.DuplicateJoystickMap(0);
        // currentInputManager.rewiredInputManager.userData.GetJou
        while (true)
        {
            Player[] unassignedPlayers = ReInput.players.AllPlayers.Except(activePlayers).ToArray();
            for (int i = 0; i < unassignedPlayers.Length; i++)// (var player in unassignedPlayers)
            {
                var player = unassignedPlayers[i];

                if (!customControllerMaps.ContainsKey(player))
                {
                    RegisterCustomController(player);
                }

                if (buttonForRegistration == null ? player.GetAnyButtonDown() : player.GetButtonDown(buttonForRegistration.id))
                {
                    GameObject go = new GameObject("Player " + nextPlayerNumber.ToString());
                    PlayerInput playerInput = go.AddComponent<PlayerInput>();
                    playerInput.rewiredPlayer = player;
                    playerInput.playerNumber = nextPlayerNumber;
                    activePlayers.Add(player);
                    playersDict[player] = playerInput;
 
                    var orderedByPlayerNumber = playersDict.Values.OrderBy(x => x.playerNumber).ToArray();
                    for (nextPlayerNumber = 1; nextPlayerNumber <= orderedByPlayerNumber.Count(); nextPlayerNumber++)
                    {
                        if (orderedByPlayerNumber[nextPlayerNumber-1].playerNumber != nextPlayerNumber)
                        {
                            break;
                        }
                    }
                    int j = 1;

                    Debug.Log("Player registered: " + player.name);
                    GenericCoroutineManager.instance.RunInFrames(1,() => {
                        onPlayerRegistered?.Invoke(playerInput);
                    });
                }
            }

            yield return null;
        }
    }

    public PlayerInput AddComputerPlayerInput(GameObject targetObject)
    {
        var ret = targetObject.AddComponent<PlayerInput>();
        ret.playerNumber = nextPlayerNumber++;
        return ret;
    }
}

public enum XboxControllerButtonLayout : int {
    A =0,
    B = 1,
    X = 2,
    Y = 3,
    LB = 4,
    RB = 5,
    Select = 6,
    Start = 7,
    LeftStickClick = 8,
    RightStickClick = 9,
    LT = 10,
    RT = 11,
    DpadRight = 12,
    DpadLeft = 13,
    DpadUp = 14,
    DpadDown = 15
}

public enum XboxControllerAxisLayout : int
{
    LeftStickHorizontal = 0,
    LeftStickVertical = 1,
    RightStickHorizontal = 2,
    RightStickVertical = 3,
    DpadHorizontal = 4,
    DpadVertical = 5,
    LT = 6,
    RT=7
}

[Serializable]
public class CustomControllerMap
{
    string playerPrefsKey { get { return "CustomControllerMap|" + joystick.hardwareIdentifier; } }
    public CustomController customController { get; protected set; }
    public Joystick joystick { get; protected set; }

    //Custom Controller Index, Joystick Index
    public Dictionary<XboxControllerAxisLayout, int> axesMap = new Dictionary<XboxControllerAxisLayout, int>();
    public Dictionary<XboxControllerAxisLayout, int> invertedAxesMap = new Dictionary<XboxControllerAxisLayout, int>();
    public Dictionary<XboxControllerButtonLayout, int> buttonMap = new Dictionary<XboxControllerButtonLayout, int>();

    public CustomControllerMap(CustomController c, Joystick j, CustomControllerMap oldMap)
    {
        customController = c;
        joystick = j;

        if (oldMap != null)
        {
            Deserialize(oldMap);
        }
        else
        {
            string mapJson = RetrieveSerializedValue();
            if (!string.IsNullOrEmpty(mapJson))
            {
                Deserialize(mapJson);
            }
            else //Basic Defaults for now. Guessing at the button layout.
            {
                axesMap[XboxControllerAxisLayout.LeftStickHorizontal] = 0;
                axesMap[XboxControllerAxisLayout.LeftStickVertical] = 1;
                axesMap[XboxControllerAxisLayout.RightStickHorizontal] = 2;
                axesMap[XboxControllerAxisLayout.RightStickVertical] = 3;
                //axesMap[XboxControllerAxisLayout.DpadHorizontal] = 4;
                //axesMap[XboxControllerAxisLayout.DpadVertical] = 5;
                axesMap[XboxControllerAxisLayout.LT] = 4;
                axesMap[XboxControllerAxisLayout.RT] = 5;
                buttonMap[XboxControllerButtonLayout.A] = 0;
                buttonMap[XboxControllerButtonLayout.B] = 1;
                buttonMap[XboxControllerButtonLayout.X] = 2;
                buttonMap[XboxControllerButtonLayout.Y] = 3;
                buttonMap[XboxControllerButtonLayout.LB] = 4;
                buttonMap[XboxControllerButtonLayout.RB] = 5;
                buttonMap[XboxControllerButtonLayout.Select] = 6;
                buttonMap[XboxControllerButtonLayout.Start] = 7;
                buttonMap[XboxControllerButtonLayout.LeftStickClick] = 8;
                buttonMap[XboxControllerButtonLayout.RightStickClick] = 9;
                buttonMap[XboxControllerButtonLayout.DpadRight] = 11;
                buttonMap[XboxControllerButtonLayout.DpadLeft] = 13;
                buttonMap[XboxControllerButtonLayout.DpadUp] = 10;
                buttonMap[XboxControllerButtonLayout.DpadDown] = 12;
                Serialize();
            }
        }

        customController.SetAxisUpdateCallback(UpdateAxes);
        customController.SetButtonUpdateCallback(UpdateButtons);
    }
    public CustomControllerMap(CustomController c, Joystick j) : this(c, j, null)
    {
       
    }

    float UpdateAxes(int customControllerAxisIndex)
    {
        XboxControllerAxisLayout customControllerAxis = (XboxControllerAxisLayout)customControllerAxisIndex;
        if (axesMap.ContainsKey(customControllerAxis))
        {
            var ret = joystick.GetAxis(axesMap[customControllerAxis]);
            if (ret != 0)
            {
                Debug.Log(customControllerAxis + ": " + ret);
            }

            return ret;
        }
        else if (invertedAxesMap.ContainsKey(customControllerAxis))
        {
            return -joystick.GetAxis(invertedAxesMap[customControllerAxis]);
        }

        return 0f;
    }

    bool UpdateButtons(int customControllerButtonIndex)
    {
        XboxControllerButtonLayout customControllerButton = (XboxControllerButtonLayout) customControllerButtonIndex;
        bool ret = buttonMap.ContainsKey(customControllerButton) ? joystick.GetButton(buttonMap[customControllerButton]) : false;
        if (ret)
            Debug.Log("Button " + customControllerButton + "|" + buttonMap[customControllerButton]);
        
        return ret;
    }

    string RetrieveSerializedValue()
    {
        if (PlayerPrefs.HasKey(playerPrefsKey))
        {
            Debug.Log("PlayerPrefsKey: " + playerPrefsKey);
            return PlayerPrefs.GetString(playerPrefsKey);
        }
        return null;
    }

    public void Serialize()
    {
        JSONObject jo = new JSONObject();
        jo.AddField("buttons", SaveDictionary(buttonMap));
        jo.AddField("axes", SaveDictionary(axesMap));
        jo.AddField("invertedAxes", SaveDictionary(invertedAxesMap));
        Debug.Log("Saving to " + playerPrefsKey + ": " + jo.ToString());
        PlayerPrefs.SetString(playerPrefsKey, jo.ToString());
    }

    public void Deserialize(CustomControllerMap oldMap)
    {
        buttonMap = oldMap.buttonMap;
        axesMap = oldMap.axesMap;
        invertedAxesMap = oldMap.invertedAxesMap;
    }

    public void Deserialize(string json)
    {
        Debug.Log("Deserializing: " + json);
        JSONObject jo = new JSONObject(json);
        buttonMap = DeserializeDict<XboxControllerButtonLayout>(jo.GetField("buttons"));
        axesMap = DeserializeDict<XboxControllerAxisLayout>(jo.GetField("axes"));
        invertedAxesMap = DeserializeDict< XboxControllerAxisLayout>(jo.GetField("invertedAxes"));
    }

    Dictionary<T, int> DeserializeDict<T>(JSONObject jo) where T : struct, IConvertible
    {
        Dictionary<T, int> ret = new Dictionary<T, int>();
        foreach(var pair in jo.ToDictionary())
        {
            ret[(T) (object)Convert.ToInt32(pair.Key)] = Convert.ToInt32(pair.Value);
        }

        return ret;
    }

    JSONObject SaveDictionary(IDictionary d)
    {
        JSONObject jo = new JSONObject();
        foreach (var key in d.Keys)
        {
            jo.AddField(key.ToString(), (int) d[key]);
        }

        return jo;
    }
}