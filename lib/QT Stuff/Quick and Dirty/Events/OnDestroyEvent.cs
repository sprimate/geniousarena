using UnityEngine;

namespace QuickAndDirty.Events {
public class OnDestroyEvent : EventHandle<OnDestroyEvent> {

	void OnDestroy()
	{
		if (Application.isPlaying) {
			Raise();
		}
	}
}
}