using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentToFighter : AOnFighterRegistered
{
    protected override void OnFighterRegistered(FighterController _fighter)
    {
        transform.SetParent(fighter.transform);
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
    }
}
