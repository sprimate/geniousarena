﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
 
[System.Serializable]
public class FighterStateType : SerializableType
{
    public FighterStateType(System.Type t)
    {
        if (t == null || t.IsSubclassOf(typeof(AFighterState)) )
        {
            type = t;
        }
        else
        {
            Debug.LogError("Invalid Type! " + t);
        }
    }
}