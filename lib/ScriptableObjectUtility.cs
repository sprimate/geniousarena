using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
public static class ScriptableObjectUtility
{
    /// <summary>
    //	This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static T CreateAsset<T>(string path = null) where T : ScriptableObject
    {
        return CreateAsset(typeof(T), path) as T;
    }

    public static ScriptableObject CreateAsset(Type t, string path = null )
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(t);

        string assetPathAndName;
        if (path == null)
        {
            path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "")
            {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + t.ToString() + ".asset");
        }
        else
        {
            assetPathAndName = path;
        }
        Debug.Log(assetPathAndName);
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;

    }

    [UnityEditor.MenuItem("Assets/Create Scriptable Object Asset")]
    private static void CreateScriptableObjectAsset() {
        var typeToCreate = ((UnityEditor.MonoScript)UnityEditor.Selection.activeObject).GetClass();
        CreateAsset(typeToCreate);
    }

    [UnityEditor.MenuItem("Assets/Create Scriptable Object Asset", true)]
    private static bool CreateScriptableObjectAssetValidation() {
        if (UnityEditor.Selection.activeObject != null && UnityEditor.Selection.activeObject.GetType() == typeof(UnityEditor.MonoScript))
        {
            return ((UnityEditor.MonoScript)UnityEditor.Selection.activeObject).GetClass().IsSubclassOf(typeof(ScriptableObject));
        }

        return false;
    }
}
#endif