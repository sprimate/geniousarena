using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class GeniousSocketManager : MonoSingleton<GeniousSocketManager>
{
    const string ON_SOCKET_CONNECTED_KEY = "onconnected";
    const string SOCKET_MESSAGE_KEY = "message";

    Action<KnockBackResponse> knockbackResponseAction;

    Dictionary<string, KeyValuePair<FighterData, Action<List<Move>, FighterData>>> moveConversionActions = new Dictionary<string, KeyValuePair<FighterData, Action<List<Move>, FighterData>>>();
    [Serializable]
    public class JSONAction : UnityEvent<JSONObject> { }
    [Serializable]
    public class SocketIOEventAction : UnityEvent<SocketIOEvent> { }

    string id;

    [Serializable]
    public class BoolAction : UnityEvent<bool> { }
    [SerializeField] BoolAction onConnectionStatusChanged;

    //public JSONAction onGps;
    public JSONAction onMessage;
    public JSONAction onConnectedSocketEvent;

    public Action onConnected;

    public bool IsConnected { get { return socketIo.IsConnected; } }

    [Serializable]
    public struct MessageAction
    {
        public string messageKey;
        public SocketIOEventAction action;
    }
    [SerializeField] List<MessageAction> messageActions;

    [SerializeField]
    SocketIOComponent _socketIO;
    protected SocketIOComponent socketIo
    {
        get
        {
            if (_socketIO == null)
            {
                _socketIO = GetComponent<SocketIOComponent>();
            }
            return _socketIO;
        }
        set
        {
            _socketIO = value;
        }
    }
    bool wasConnected = false;

    public void On(string eventKey, Action<SocketIOEvent> callback)
    {
        socketIo.On(eventKey, callback);
    }
    public void Off(string eventKey, Action<SocketIOEvent> callback)
    {
        socketIo.Off(eventKey, callback);
    }
    void Update()
    {
        if (socketIo.IsConnected != wasConnected)
        {
            if (onConnectionStatusChanged != null)
            {
                onConnectionStatusChanged.Invoke(socketIo.IsConnected);
            }

            if (socketIo.IsConnected)
            {
                onConnected?.Invoke();
            }
            wasConnected = socketIo.IsConnected;
        }
    }

    public void Listen()
    {
        gameObject.SetActive(true);
    }

    public void StopListening()
    {
        gameObject.SetActive(false);
    }


    void OnMessage(SocketIOEvent evt)
    {
        Debug.Log("Message: " + evt.data);
        onMessage.Invoke(evt.data);
    }

    void OnConnectedSocketEvent(SocketIOEvent evt)
    {
        id = evt.data.GetField("id").str;
        Debug.Log("SocketManager Connected - ID:  " + id);
        onConnectedSocketEvent.Invoke(evt.data);
        onConnectionStatusChanged.Invoke(socketIo.IsConnected); //Connection ID has been updated
    }

    public void ConnectToNewUrl(string newUrl)
    {
        Debug.Log("URL Changed. " + newUrl);
        if (socketIo.url != newUrl)
        {
            bool autoConnectBefore = socketIo.autoConnect;
            socketIo.autoConnect = false;
            if (socketIo.IsConnected)
            {
                socketIo.Close();
                id = "";
            }
            socketIo.url = newUrl;
            socketIo.Connect();
            socketIo.autoConnect = autoConnectBefore;
        }
    }

    public string GetId()
    {
        return id;
    }

    protected void Start()
    {
        if (socketIo == null)
        {
            socketIo = GetComponent<SocketIOComponent>();
            if (socketIo == null)
            {
                Debug.LogError("Can't find SocketIOComponent");
                return;
            }
        }
        socketIo.On(ON_SOCKET_CONNECTED_KEY, OnConnectedSocketEvent);
        socketIo.On(SOCKET_MESSAGE_KEY, OnMessage);
        if (messageActions == null)
        {
            messageActions = new List<MessageAction>();
        }
        foreach (MessageAction a in messageActions)
        {
            socketIo.On(a.messageKey, (parameter) => { a.action.Invoke(parameter); });
        }

        onConnectedSocketEvent.AddListener(obj =>
        {
            Debug.Log("Connected! " + obj);
        });
        socketIo.On("knockbackDataResponse", (x) =>
        {
            KnockBackResponse response = JsonUtility.FromJson<KnockBackResponse>(x.data.ToString());
            knockbackResponseAction?.Invoke(response);
//            HandleResponeCallback<KnockBackResponse>(knockbackResponseAction, x);
        });

        socketIo.On("moveConversionResponse", (x) =>
        {
            JSONObject moves = x.data.GetField("moves");
            List<Move> ret = new List<Move>();
            foreach(var m in moves.list)
            {
                Move move = JsonUtility.FromJson<Move>(m.ToString());
                ret.Add(move);
            }
  
            string instanceId = x.data.GetField("unparsedInstanceId").str;

            if (moveConversionActions.ContainsKey(instanceId))
            {
                moveConversionActions[instanceId].Value.Invoke(ret, moveConversionActions[instanceId].Key);
                moveConversionActions.Remove(instanceId);
            }
            else
            {
                Debug.LogError("Error: Move Conversion can't be found for the callback");
            }
        });
    }

    JSONObject GetDataObject(SocketIOEvent evt)
    {
        return new JSONObject(evt.data.ToString()).GetField("message");
    }

    public void HandleResponeCallback<T>(Action<T> a, SocketIOEvent evt)
    {
        JSONObject outerObject = new JSONObject(evt.data.ToString());
        Debug.Log("Outer Response: " + outerObject.ToString());

        JSONObject data = GetDataObject(evt);
        Debug.Log("Response: " + data.ToString());
        T ret = JsonUtility.FromJson<T>(data.ToString());
        Debug.Log("Ret Parsed: " + ret);
        a?.Invoke(ret);
    }
    public void SendKnockbackRequest(KnockbackRequest request)
    {
        Debug.Log("KnockbackRequest: " + request);
        knockbackResponseAction = request.OnKnockbackResponse;
        Emit(JsonUtility.ToJson(request), "requestKnockbackData");
    }

    public void RequestMoveConversion(UnparsedMove unparsedMove, FighterData frameData, Action<List<Move>, FighterData> callback)
    {
        moveConversionActions[unparsedMove.InstanceId] = new KeyValuePair<FighterData, Action<List<Move>, FighterData>>(frameData, callback);
        Emit(JsonUtility.ToJson(unparsedMove), "requestParsedMove");
    }

    void Emit(string json, string type)
    {
        //Debug.Log("Sending " + type + ": " + json);
        if (socketIo.IsConnected)
        {
            socketIo.Emit(json, type);
        }
        else
        {
            Debug.Log("Can't send request because it's not connected");
        }
    }
}