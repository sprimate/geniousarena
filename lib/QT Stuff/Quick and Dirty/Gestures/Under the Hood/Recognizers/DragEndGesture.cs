using UnityEngine;
namespace QuickAndDirty.Gestures {
    public class DragEndGesture : GestureRecognizer
    {
        public float dragHoldTime;
        public Vector2 minDelta;

        public DragEndGesture()
            : base()
        {
            dragHoldTime = QADGestures.instance.dragHoldTime;
            minDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture gesture)
        {
            return gesture.phase == TouchPhase.Ended
                && gesture.totalDeltaPosition.sqrMagnitude > minDelta.sqrMagnitude
                && gesture.timeHeld > dragHoldTime;
        }
    }
}