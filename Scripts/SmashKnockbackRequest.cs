using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]public class Modifier : SmashCalculatorJSONClass
{
    public string name;
    public float damage_dealt;
    public float damage_taken;
    public float kb_dealt;
    public float kb_received;
    public float gravity;
    public float fall_speed;
    public float shield;
    public float air_friction;
    public float traction;

    public Modifier()
    {
        name = "Normal";
        damage_dealt = 1;
        damage_taken = 1;
        kb_dealt = 1;
        kb_received = 1;
        gravity = 1;
        fall_speed = 1;
        shield = 1;
        air_friction = 1;
        traction = 1;
    }
}

[Serializable]public class KnockbackPlayer : SmashCalculatorJSONClass
{
    public string display_name;
    public Modifier modifier;
    public List<Modifier> modifiers;
    public string name;
    public string api_name;
    public Sm4shCalculatorCharacterAttributes attributes;

    public KnockbackPlayer(FighterData fighterData)
    {
        display_name = fighterData.displayName;
        modifier = new Modifier();
        modifiers = new List<Modifier>();
        name = fighterData.characterName;
        api_name = fighterData.characterName;
        attributes = fighterData.attributes;
    }
}

[Serializable]public class KnockbackAttack : SmashCalculatorJSONClass
{
    public float base_damage;
    public int hitlag;
    public float angle;
    public float bkb;
    public float wbkb;
    public float kbg;
    public float shield_damage;
    public float preLaunchDamage;
    public bool is_smash_attack;
    public int charged_frames;
    public bool windbox;
    public bool projectile;
    public bool set_weight;
    public bool aerial_opponent;
    public bool ignore_staleness;
    public bool mega_man_fsmash;
    public bool on_witch_time;
    public bool unblockable;
    public List<string> stale_queue; //?
    public string name;
    public string landingLag;
    public List<string> autoCancel;
    public bool isFinishingTouch;
    public string effect;

    public KnockbackAttack(Move m, FighterController target)
    {
        base_damage = m.base_damage;
        hitlag = 0;
        angle = m.angle;
        bkb = m.bkb;
        wbkb = m.wbkb;
        kbg = m.kbg;
        shield_damage = m.shieldDamage;
        preLaunchDamage = m.preLaunchDamage;
        is_smash_attack = m.smash_attack;
        charged_frames = m.chargedFrames;
        windbox = m.windbox;
        projectile = m.isProjectile; //?
        set_weight = m.weightDependent; //?
        aerial_opponent = !target.m_IsGrounded;
        ignore_staleness = false;
        mega_man_fsmash = false; //TODO - Handle when mega man is imported
        on_witch_time = false;//TODO - Handle when in witch time
        unblockable = m.unblockable;
        stale_queue = new List<string>(); //TODO - implement staleness
        name = "";
        //landingLag = "";
        autoCancel = new List<string>();//Todo - implement autocancel
        isFinishingTouch = false;
        effect = "None/Other";
    }
}

[Serializable]public class Di
{
    public int X;
    public int Y;
}

[Serializable]public class Modifiers : SmashCalculatorJSONClass
{
    public Di di;
    public bool no_di;
    public float launch_rate;
    public bool grounded_meteor;
    public bool crouch_cancel;
    public bool interrupted_smash_charge;

    public Modifiers()
    {
        di = new Di();
        no_di = true;
        launch_rate = 1f;
        grounded_meteor = false;
        crouch_cancel = false;
        interrupted_smash_charge = false;
    }
}

[Serializable]public class ShieldAdvantage : SmashCalculatorJSONClass
{
    public int hit_frame;
    public int faf;
    public bool powershield;
    public int landing_frame;
    public bool use_landing_lag;
    public bool use_autocancel;

    //TODO - should probably actually fill these in at some point
    public ShieldAdvantage()
    {
        hit_frame = 0;
        faf = 0;
        powershield = false;
        landing_frame = 0;
        use_landing_lag = false;
        use_autocancel = false;
    }
}

[Serializable]public class Stage : SmashCalculatorJSONClass
{
    public Position position;
    public bool inverse_x;
    public string stage_data;

    public Stage()
    {
        position = new Position();
        inverse_x = false;
        stage_data = "";
    }
}

[Serializable]public class NewKnockbackRequest : SmashCalculatorJSONClass
{
    public KnockbackPlayer attacker;
    public int attacker_percent;
    public KnockbackPlayer target;
    public int target_percent;
    public int luma_percent;
    public KnockbackAttack attack;
    public Modifiers modifiers;
    public ShieldAdvantage shield_advantage;
    public Stage stage;
    public bool vs_mode;


    [System.NonSerialized] public FighterController targetController;
    [System.NonSerialized] public FighterController attackController;
    Move move;
    public NewKnockbackRequest(Move _move, FighterController _attackController, FighterController _targetController)
    {
        move = _move;
        vs_mode = true;
        attack = new KnockbackAttack(_move, _targetController);
        attacker = new KnockbackPlayer(_attackController.fighterData);
        target = new KnockbackPlayer(_targetController.fighterData);
        modifiers = new Modifiers();
        shield_advantage = new ShieldAdvantage();
        targetController = _targetController;
        attackController = _attackController;
        vs_mode = true;
    }

    public void OnKnockbackResponse(KnockBackResponse response)
    {
        targetController?.attackEngine?.HandleKnockbackResponse(response, attackController, move);
    }
}