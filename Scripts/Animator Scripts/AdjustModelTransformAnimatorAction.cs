using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustModelTransformAnimatorAction : FighterAnimatorScript
{
    public Vector3 newPosition;
    public bool resetPositionOnExit = true;

    public Vector3 newRotation;
    public bool resetRotationOnExit = true;

    public Vector3 newScale;
    public bool resetScaleOnExit = true;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        fc.model.AdjustEulerAngles(newRotation);
        fc.model.AdjustPosition(newPosition);
        fc.model.AdjustScale(newScale);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (resetRotationOnExit)
        {
            fc.model.ResetEulerAngles();
        }
        if (resetPositionOnExit)
        {
            fc.model.ResetPosition();
        }
        if (resetScaleOnExit)
        {
            fc.model.ResetScale();
        }
    }
}