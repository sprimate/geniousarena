using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnHit : MonoBehaviour
{
    [System.Serializable]
    public class DamageEvent : UnityEvent<float, MonoBehaviour, Vector3, Vector3> { }
    public DamageEvent onHit;
    public void ApplyDamage(float damage, MonoBehaviour damager, Vector3 direction, Vector3 damageSource, bool throwing = false, bool stopCamera = false)
    {
        onHit?.Invoke(damage, damager, direction, damageSource);
    }

    void Reset()
    {
        onHit = new DamageEvent();
        //Probably need to move this to the Gamekit3d assembly in order for this reset to work
        //UnityEditor.Events.UnityEventTools.AddPersistentListener(onHit, GetComponent<Gamekit3D.Damageable>().ApplyDamage);
    }
}
