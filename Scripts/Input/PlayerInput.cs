using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Experimental.Input;
//using UnityEngine.Experimental.Input.Plugins.PlayerInput;
//using UnityEngine.Experimental.Input.Plugins.Users;
using Rewired;
using UnityEngine.SceneManagement;

//Should be before the earliest thing we can think of.
[ExecuteBefore(typeof(FighterController))]
public class PlayerInput : MonoBehaviour
{
    //either fixedUpdate or Update. Those are the only options for now
    //NOTE - FixedUpdate is called before Update, so it's probably best to do it here
    //TODO - maybe mke this an enum to allow LateUpdate or EndOfFrame for some reason
    public static bool updateInputInFixedUpdate = true;
    //even if we're supposed to update in fixedUpdate normally, that won't run if TimeFrame is 0, so do it in update instead
    public bool updateInputInUpdateIfTimeScale0 = true;
    public int playerNumber = 1;
    public Player rewiredPlayer { get; set; }
    protected Dictionary<string, Action<bool>> onButton;// = new Dictionary<string, Action<bool>>();
                                                        // protected Dictionary<string, Action> onButtonDown = new Dictionary<string, Action>();
    protected Dictionary<string, bool> buttonsDown;// = new Dictionary<string, bool>();
    //protected Dictionary<string, int> buttonsTriggered = new Dictionary<string, int>();

    protected Dictionary<string, Action<Vector2>> onAxis2d;
    protected Dictionary<string, Vector2> axes2d;

    protected Dictionary<string, Action<float>> onAxis1d;
    protected Dictionary<string, float> axes1d;
    //public Vector2 dpad;
    /*    [SerializeField] public FighterControls controls;
        [HideInInspector] public FighterControls.PlayerActions playerActions;
        */
    bool inputBlocked;
   // public InputDevice device;

    public bool GetButtonValue(InputAction action)
    {
        return IsButton(action);// buttonsDown[action.name];
    }

    public List<InputAction> allButtonActions;
    public List<InputAction> all1dAxisActions;
    public List<InputAction> all2dAxisActions;

    public void ClearAndInitializeCallbacks()
    {
        onButton = new Dictionary<string, Action<bool>>();
        buttonsDown = new Dictionary<string, bool>();
        onAxis2d = new Dictionary<string, Action<Vector2>>();
        axes2d = new Dictionary<string, Vector2>();
        onAxis1d = new Dictionary<string, Action<float>>();
        axes1d = new Dictionary<string, float>();
        allButtonActions = new List<InputAction>();
        all1dAxisActions = new List<InputAction>();
        all2dAxisActions = new List<InputAction>();
        vector2Keys = new Dictionary<string, string>();
        axis2dToCompleteAfterFrame = new HashSet<string>();
        combos = new Dictionary<string, List<KeyValuePair<InputAction[], Action>>>();
        rewiredPlayer?.ClearInputEventDelegates();
    }
    public float GetInputMagnitude(Vector2 input)
    {
        if (input.x >= 1f || input.y >= 1)
        {
            return 1;
        }

        //x^2 + y^2 = 1^2
        //max hypotenuse of a 1x1 right triangle is 1.41, so this should get an even scale between 0 and 1
        return Mathf.Min(1f, input.magnitude);// (input.x * input.x + input.y * input.y) / 1f;// (Mathf.Sqrt(input.x * input.x + input.y * input.y));
    }

    public Vector2 GetVector2(InputAction horizontalAction, InputAction verticalAction)
    {
        if (rewiredPlayer == null)
        {
            return Vector2.zero;
        }
        return rewiredPlayer.GetAxis2D(horizontalAction.id, verticalAction.id);
    }

    public T Get<T>(InputAction action)
    {
        if (typeof(T) == typeof(float))
        {
            if (rewiredPlayer == null)
                return (T)Convert.ChangeType(0, typeof(float));
            return (T)Convert.ChangeType(rewiredPlayer.GetAxis(action.id), typeof(float)); //axes1d[action.name], typeof(float));

        }
        else if (typeof(T) == typeof(bool))
        {
            if (rewiredPlayer == null)
            {
                return (T)Convert.ChangeType(false, typeof(bool));
            }
            return (T)Convert.ChangeType(rewiredPlayer.GetButton(action.id), typeof(bool));//(T)Convert.ChangeType(buttonsDown[action.name], typeof(bool));
        }
        else
        {
            Debug.LogError("We can't handle type " + typeof(T) + " right now");
            return default(T);
        }
    }
    public Dictionary<string, string> vector2Keys;// = new Dictionary<string, string>();

    Action<T> WrapCallback<T>(Action<object> callback)
    {
        if (callback == null)
        {
            Debug.LogError("Where I am doing a null callback from?");
        }
        return (x) => {
            callback.Invoke((T)x);
        };
    }

    public Action AppendAction<T>(InputAction action, Action<T, PlayerInput> callback, InputAction verticalActionForVector2 = null)
    {
        return AppendAction(action.name, callback, verticalActionForVector2?.name);
    }

    public Action AppendAction<T>(string action, Action<T, PlayerInput> callback, string verticalActionForVector2 = null)
    {
        return AppendAction<T>(action, (x) =>
        {
            callback?.Invoke(x, this);
        }, verticalActionForVector2);
    }
    //Returns: an action for unsubscribing to this 
    public Action AppendAction<T>(InputAction action, Action<T> callback, InputAction verticalActionForVector2 = null )
    {
        return AppendAction<T>(action.name, callback, verticalActionForVector2?.name);
    }

    //Returns: an action for unsubscribing to this 
    public Action AppendAction<T>(string action, Action<T> callback, string verticalActionForVector2 = null)
    {
        Action<object> objCallback = (x) =>
        {
            if (x is T)
            {
                callback.Invoke((T)x);
            }
            else
            {
                Debug.LogError(action + " callback type mismatch");
                callback.Invoke(default(T));
            }
        };
        return AppendAction(action, objCallback, typeof(T), verticalActionForVector2);
    }

    public Action AppendAction(InputAction action, Action<object> callback, Type t, InputAction verticalActionForVector2 = null)
    {
        return AppendAction(action.name, callback, t, verticalActionForVector2?.name);
    }

    public Action AppendAction(string action, Action<object> callback, Type t, string verticalActionForVector2 = null )
    {
        if (t == typeof(Vector2))
        {
            if (verticalActionForVector2 == null)
            {
                Debug.LogError("Need 2 actions for Axis2D");
            }
            else
            {
                string axis2dKey = action + "|" + verticalActionForVector2;
                vector2Keys[action] = axis2dKey;
                vector2Keys[verticalActionForVector2] = axis2dKey;
                var specificCallback = WrapCallback<Vector2>((x) => {
                   // Debug.Log("Callback for " + action.name + "|" + verticalActionForVector2.name + ": " + x);
                    callback.Invoke(x);
                    }
                );
                if (!onAxis2d.ContainsKey(axis2dKey))
                {
                    onAxis2d[axis2dKey] = specificCallback;// as Action<Vector2>;
                }
                else
                {
                    onAxis2d[axis2dKey] += specificCallback;
                }

                return () => {
                    onAxis2d[axis2dKey] -= specificCallback;
                };
            }
        }
        else if (t == typeof(float))
        {
            var specificCallback = WrapCallback<float>(callback);
            if (!onAxis1d.ContainsKey(action))
            {
                onAxis1d[action] = specificCallback;
            }
            else
            {
                onAxis1d[action] += specificCallback;
            }

            return () => {
                onAxis1d[action] -= specificCallback;
            };
        }
        else if (t== typeof(bool))
        {
            if (rewiredPlayer == null)
            {
                return null;
            }
            /*Action<InputActionEventData> eventDelegateJustPressedFixedUpdate = (x) =>
            {
                Debug.Log("FixedUpdate Callback for " + action.name,this);
                if (updateInputInFixedUpdate)
                {
                    callback?.Invoke(true);
                }
            };

            Action<InputActionEventData> eventDelegateJustPressedUpdate = (x) =>
            {
                if (!updateInputInFixedUpdate || Time.timeScale == 0f)
                {
                    callback?.Invoke(true);
                }
            };

            Action<InputActionEventData> eventDelegateJustReleasedFixedUpdate = (x) =>
            {
                if (updateInputInFixedUpdate)
                {
                    callback?.Invoke(false);
                }
            };

            Action<InputActionEventData> eventDelegateJustReleasedpdate = (x) =>
            {
                if (!updateInputInFixedUpdate || Time.timeScale == 0f)
                {
                    callback?.Invoke(false);
                }
            };

            rewiredPlayer.AddInputEventDelegate(eventDelegateJustPressedFixedUpdate, UpdateLoopType.FixedUpdate, InputActionEventType.ButtonJustPressed, action.id);
            //rewiredPlayer.AddInputEventDelegate(eventDelegateJustPressedUpdate, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, action.id);
            rewiredPlayer.AddInputEventDelegate(eventDelegateJustReleasedFixedUpdate, UpdateLoopType.FixedUpdate, InputActionEventType.ButtonJustReleased, action.id);
            //rewiredPlayer.AddInputEventDelegate(eventDelegateJustReleasedpdate, UpdateLoopType.Update, InputActionEventType.ButtonJustReleased, action.id);
            */
            var specificCallback = WrapCallback<bool>((x) => 
            {
                callback?.Invoke(x);
            });

            if (!onButton.ContainsKey(action))
            {
                onButton[action] = specificCallback;
                //SetInputEventDelegate<bool>(action.name);
            }
            else
            {
                onButton[action] += specificCallback;
            }
            
            return () => {
                onButton[action] -= specificCallback;
            };
            /*
            return () =>
            {
                rewiredPlayer.RemoveInputEventDelegate(eventDelegateJustPressedFixedUpdate);
                rewiredPlayer.RemoveInputEventDelegate(eventDelegateJustPressedUpdate);
                rewiredPlayer.RemoveInputEventDelegate(eventDelegateJustReleasedFixedUpdate);
                rewiredPlayer.RemoveInputEventDelegate(eventDelegateJustReleasedpdate);
            };*/
        }
        else
        {
            Debug.LogError("We can't handle type [" + t + "] right now");
        }

        return null;
    }

    UpdateLoopType inputUpdateLoopType { get { return (updateInputInUpdateIfTimeScale0 && Time.timeScale == 0f) || !updateInputInFixedUpdate ? UpdateLoopType.Update : UpdateLoopType.FixedUpdate; } }
    UpdateLoopType? lastInputLoopType;
    void SetInputEventDelegate<T>(string action)
    {
        if (typeof(T) == typeof(bool))
        {
            Action<InputActionEventData> eventDelegateJustPressed = (x) =>
            {
                onButton?[action]?.Invoke(true);
            };

            Action<InputActionEventData> eventDelegateReleased = (x) =>
            {
                 onButton?[action]?.Invoke(false);
            };

            Debug.Log("Settign rewired to follow " + action + " on " + inputUpdateLoopType);

            rewiredPlayer.AddInputEventDelegate(eventDelegateJustPressed, inputUpdateLoopType, InputActionEventType.ButtonJustPressed, action);
            rewiredPlayer.AddInputEventDelegate(eventDelegateReleased, inputUpdateLoopType, InputActionEventType.ButtonJustReleased, action);
        }
    }

    public void AppendComboAction(Action callback, params InputAction[] actions)
    {
        AppendComboAction(callback, true, actions);
    }

    void UpdateInputDelegates()
    {
        if (rewiredPlayer == null)
        {
            return;
        }


        //Debug.Log("Trying to update input delegates for buttons");
        rewiredPlayer.ClearInputEventDelegates();
        foreach (var b in onButton.Keys)
        {
            SetInputEventDelegate<bool>(b);
        }
    }

    public void Update()
    {
        if (!updateInputInFixedUpdate)
        {
            UpdateInput();
        }
        return;
        if (lastInputLoopType != inputUpdateLoopType)
        {
           // UpdateInputDelegates();
        }

        lastInputLoopType = inputUpdateLoopType;
    }

    void FixedUpdate()
    {
        if (updateInputInFixedUpdate)
        {
            UpdateInput();
        }
    }

    void UpdateInput()
    {
        if (rewiredPlayer == null)
            return;

        int i = 0;
        ApplyToAllActions((a) =>
        {
            // Debug.Log("NumTimes: " + ++i);
            if (a.type == InputActionType.Button)
            {
                //  Debug.Log(a.name + " - " + a.id);
                if (rewiredPlayer.GetButtonDown(a.id))
                {
                    //  SmashSettings.Log(a.name + " Down");
                }

                /*if (rewiredPlayer.GetButtonDown(a.id) && onButtonDown.ContainsKey(a.name))
                {
                    onButtonDown[a.name]?.Invoke();
                }*/

                if (onButton.ContainsKey(a.name))
                {
                    bool currentButtonValue = rewiredPlayer.GetButton(a.id);
                    if (!buttonsDown.ContainsKey(a.name) || currentButtonValue != buttonsDown[a.name])
                    {
                        onButton[a.name]?.Invoke(currentButtonValue);
                    }
                    buttonsDown[a.name] = currentButtonValue;
                }
            }
            else if (a.type == InputActionType.Axis)
            {
                if (onAxis1d.ContainsKey(a.name))
                {
                    float currentAxisValue = rewiredPlayer.GetAxis(a.id);
                    if (!axes1d.ContainsKey(a.name) || currentAxisValue != axes1d[a.name])
                    {
                        onAxis1d[a.name]?.Invoke(currentAxisValue);
                    }

                    axes1d[a.name] = currentAxisValue;
                }

                if (vector2Keys.ContainsKey(a.name))
                {
                    axis2dToCompleteAfterFrame.Add(vector2Keys[a.name]);//To avoid doing this twice each frame
                    //Debug.Log("Cn't handle axis2d yet");
                }
            }
            else
            {
                Debug.LogError("Can't handle this type of InputAction");
            }
        });
    }

    HashSet<string> axis2dToCompleteAfterFrame;// = new HashSet<string>();
    IEnumerator MonitorAxis2d()
    {
        while (true)
        {
            foreach (var key in axis2dToCompleteAfterFrame)
            {
                var split = key.Split('|');
                Vector2 currentAxisValue = rewiredPlayer.GetAxis2D(split[0], split[1]);
                if (!axes2d.ContainsKey(key) || currentAxisValue != axes2d[key])
                {
                    onAxis2d[key]?.Invoke(currentAxisValue);
                }
                axes2d[key] = currentAxisValue;
            }
            axis2dToCompleteAfterFrame.Clear();
            yield return new WaitForFixedUpdate();
        }
    }

    Dictionary<string, List<KeyValuePair<InputAction[], Action>>> combos;// = new Dictionary<string, List<KeyValuePair<InputAction[], Action>>>();
    public void AppendComboAction(Action callback, bool onTriggerOnly, params InputAction[] actions)
    {
        if (onTriggerOnly)
        {
            foreach(var a in actions)
            {
                if (!combos.ContainsKey(a.name))
                {
                    combos[a.name] = new List<KeyValuePair<InputAction[], Action>>();
                }
                combos[a.name].Add(new KeyValuePair<InputAction[], Action>(actions,callback));
            }
        }
        else
        {
            Debug.LogError("Don't yet handle the combo option for onTriggerOnly being false");
        }
    }

    IEnumerator Start()
    {
        if (monitorAxis2dCoroutine == null)
        {
            Initialize();
        }

        yield return new WaitForEndOfFrame();
       /* SceneManager.activeSceneChanged += (x, y) =>
        {
            onButton.Clear();
            onAxis1d.Clear();
            onAxis2d.Clear();
            ApplyToAllActions((action) =>
            {
                Action<InputAction.CallbackContext> a;
            });
        };
        */
    }

    Coroutine monitorAxis2dCoroutine;
    // Start is called before the first frame update
    public virtual void Initialize()
    {
        ClearAndInitializeCallbacks();
        monitorAxis2dCoroutine = StartCoroutine(MonitorAxis2d());
    }

    void ApplyToAllActions(Action<InputAction> action)
    {
        if (ReInput.mapping == null)
        {
            return;
        }
       // Debug.Log("Actions Count: " + ReInput.mapping.Actions.Count);
        foreach(var a in ReInput.mapping.Actions)
        {
            action.Invoke(a);
        }
    }

    public bool IsButton(InputAction action)
    {
        if (rewiredPlayer == null)
            return false;
        return rewiredPlayer.GetButton(action.id);
//        return action.phase != InputActionPhase.Waiting && action.phase != InputActionPhase.Cancelled && action.phase != InputActionPhase.Cancelled;
    }

    HashSet<string> buttonsInCombo = new HashSet<string>();

    bool HandleCombos(InputAction inputAction)
    {
        Debug.Log("Can't handle combos yet");
      
        return false;
    }
}