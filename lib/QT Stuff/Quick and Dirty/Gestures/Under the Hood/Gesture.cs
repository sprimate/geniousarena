using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class Gesture
    {
        public Gesture(int fingerId)
        {
            _fingerID = fingerId;
            _dirty = true;
        }

        protected float _downTime;
        public virtual float downTime		//Time finger touched down
        {
            get
            {
                return _downTime;
            }
        }

        protected float _upTime;
        public virtual float upTime			//Time finger lifted
        {
            get
            {
                return _upTime;
            }
        }

        protected Vector2 _downPos;
        public virtual Vector2 downPosition	//Position finger started touching at
        {
            get
            {
                return _downPos;
            }
        }

        protected Vector2 _upPos;
        public virtual Vector2 upPosition 	//Position finger stopped touching at
        {
            get
            {
                return _upPos;
            }
        }


        protected Touch _touch;
        public virtual Touch touch			//The UnityEngine Touch this is associated with
        {
            get
            {
                return _touch;
            }
        }

        public virtual void UpdateGesture(Touch touch)
        {
            if (touch.fingerId != _fingerID)
            {
                Debug.Log("WRONG FUCKING FINGER ASSHAT");
                return;
            }

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _isEnded = false;
                    _dirty = false;
                    _downTime = Time.timeSinceLevelLoad;
                    _downPos = touch.position;
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Ended:
                    if (!isEnded)
                    {
                        _isEnded = true;
                        _upPos = touch.position;
                        _upTime = Time.timeSinceLevelLoad;
                    }
                    break;
                default:
                    Debug.Log("Unexpected touch phase while updating gesture.");
                    break;
            }

            _touch = touch;
        }

        public virtual float deltaTime
        {
            get
            {
                return _touch.deltaTime;
            }
        }

        public virtual Vector2 deltaPosition
        {
            get
            {
                return _touch.deltaPosition;
            }
        }

        public virtual Vector2 totalDeltaPosition
        {
            get
            {
                if (isEnded)
                    return _upPos - _downPos;
                else
                    return _touch.position - _downPos;
            }
        }

        public virtual Vector2 position
        {
            get
            {
                return _touch.position;
            }
        }

        public virtual TouchPhase phase
        {
            get
            {
                return _touch.phase;
            }
        }

        public virtual float timeHeld
        {
            get
            {
                if (isEnded)
                    return _upTime - _downTime;
                else
                    return Time.timeSinceLevelLoad - _downTime;
            }
        }

        public virtual float distanceMoved
        {
            get
            {
                if (isEnded)
                    return Vector2.Distance(_downPos, _upPos);
                else
                    return Vector2.Distance(_downPos, _touch.position);
            }
        }

        protected int _fingerID;
        public virtual int fingerID
        {
            get
            {
                //Debug.Log(_fingerID == _touch.fingerId);
                return _fingerID;
            }
        }

        protected bool _isEnded;
        public virtual bool isEnded		//Hopefully more efficient than loading several structs on the stack
        {
            get
            {
                return _isEnded;
            }
        }

        /*
         * Alright, this one is a little odd...
         * Basically it should be treated the same as this object being null,
         * because actually setting it to null would leave the object to GC
         * which is overagressive on some platforms.
         */
        protected bool _dirty;
        public virtual bool dirty
        {
            get
            {
                return _dirty;
            }
            set
            {
                if (value == false)
                    Debug.Log("False");
                _dirty = value;
            }
        }
    }
}