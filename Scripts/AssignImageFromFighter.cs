using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AssignImageFromFighter : AOnFighterRegistered
{
    protected override void OnFighterRegistered(FighterController _fighter)
    {
        GetComponent<Image>().sprite = fighter.fighterData.portrait;
    }
}