using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FighterDataInputPair
{
    public PlayerInput input;
    public FighterData fighterData;
}

[RequireComponent(typeof(Canvas))]
public class CharacterSelectionCanvas : MonoSingleton<CharacterSelectionCanvas>
{
    [Range(0f, 1f)]
    public float selectionBoxColorOffset;
    Canvas canvas;
    public Image selectionBoxTemplate;
    protected FighterSelectionMap fighterSelectionMap;
    public Image fightersContainer;
    public Image unavailableCharacterScrim;
    public Transform selectedCharacterContainer;
    public TextMeshProUGUI instructions;
    public AspectRatioFitter.AspectMode aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
    Action<List<FighterDataInputPair>> OnFightersSelected;
    Dictionary<Transform, Player> playerCursors = new Dictionary<Transform, Player>();

    public int minNumFighters = 1;
    public int maxNumFighters = 4;
    protected override void Awake()
    {
        base.Awake();
        canvas = GetComponent<Canvas>();
        var trigger = fightersContainer.GetOrAddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnCharacterClicked);
        trigger.triggers.Add(entry);
    }

    public void Activate(FighterSelectionMap map, Action<List<FighterDataInputPair>> callback)
    {
        fighterSelectionMap = map;
        //Time.timeScale=0f;
        OnFightersSelected = callback;
        var fitter = fightersContainer.GetOrAddComponent<AspectRatioFitter>();
        fitter.aspectRatio = fighterSelectionMap.image.rect.width / fighterSelectionMap.image.rect.height;
        fitter.aspectMode = aspectMode;
        fightersContainer.preserveAspect = false;
        fightersContainer.sprite = fighterSelectionMap?.image;
        for (int x = 0; x < fighterSelectionMap.rows; x++)
        {
            for (int y = 0; y < fighterSelectionMap.columns; y++)
            {
                var fighter = fighterSelectionMap.Get(new Vector2Int(x, y));
                if (fighter == null)
                {
                    var thisScrim = Instantiate(unavailableCharacterScrim);
                    thisScrim.transform.SetParent(fightersContainer.transform);
                    thisScrim.rectTransform.anchorMin = new Vector2((float)(y) / (float)fighterSelectionMap.columns, (float)(fighterSelectionMap.rows - x - 1) / (float)fighterSelectionMap.rows);
                    thisScrim.rectTransform.anchorMax = new Vector2((float)(y + 1) / (float)fighterSelectionMap.columns, (float)(fighterSelectionMap.rows - x) / (float)fighterSelectionMap.rows);
                    thisScrim.rectTransform.localPosition = Vector3.zero;
                    thisScrim.rectTransform.anchoredPosition = Vector2.zero;
                    thisScrim.rectTransform.sizeDelta = Vector2.zero;
                    thisScrim.enabled = true;
                    thisScrim.gameObject.SetActive(true);
                }
            }
        }
        Vector3[] corners = new Vector3[4];
        fightersContainer.rectTransform.GetWorldCorners(corners);
        xRange = new Vector2(corners[0].x, corners[2].x);
        yRange = new Vector2(corners[0].y, corners[2].y);

        UpdateInstructions();
        gameObject.SetActive(true);
    }

    void InitializeInput()
    {
        GeniousInputManager.instance.SetRewiredInputManager<CharacterSelectionInputManagerActions>();
        GeniousInputManager.instance.ListenForPlayers(OnNewPlayer);
    }

    List<PlayerInput> activePlayers = new List<PlayerInput>();
    Dictionary<PlayerInput, Image> selectionObjects = new Dictionary<PlayerInput, Image>();
    Dictionary<PlayerInput, Vector2Int> selectionPosition = new Dictionary<PlayerInput, Vector2Int>();
    void OnNewPlayer(PlayerInput newPlayer)
    {
        while(activePlayers.Count < newPlayer.playerNumber)
        {
            activePlayers.Add(null);
        }

        activePlayers[newPlayer.playerNumber - 1] = newPlayer;
        var newBox = Instantiate<Image>(selectionBoxTemplate, fightersContainer.transform);
        newBox.transform.name = "Selection Box (Player " + newPlayer.playerNumber + ")";
        float hueValue = (GetColorBySplitting(newPlayer.playerNumber+1, 1f) + selectionBoxColorOffset) % 1f;
        newBox.color = Color.HSVToRGB(hueValue, 1f, 1f);
        newBox.gameObject.SetActive(true);
        //A sort of arbitrary wait to ensure callbacks aren't called during registration (single button press registered twice)
        GenericCoroutineManager.instance.RunInSecondsRealtime(.2f, () =>
        {
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Up, UpButton);
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Down, DownButton);
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Right, RightButton);
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Left, LeftButton);
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Select, SelectButton);
            newPlayer.AppendAction<bool>(CharacterSelectionInputManagerActions.Start, StartButton);
        });

        selectionObjects[newPlayer] = newBox;
        HighlightChar(0, 0, newPlayer);
    }

    void HighlightChar(int row, int col, PlayerInput player)
    {
        row = row % fighterSelectionMap.rows;
        col = col % fighterSelectionMap.columns;

        if (row < 0)
        {
            row = fighterSelectionMap.rows - 1;
        }

        if (col < 0)
        {
            col = fighterSelectionMap.columns - 1;
        }

        float minY = 1 - (((float)(row+1)) / fighterSelectionMap.rows);
        float maxY = minY + (1f / fighterSelectionMap.rows / selectionObjects[player].rectTransform.localScale.y);
        float minX= ((float)(col)) / fighterSelectionMap.columns;
        float maxX = minX + (1f / fighterSelectionMap.columns / selectionObjects[player].rectTransform.localScale.x);
        selectionObjects[player].rectTransform.anchorMin = new Vector2(minX, minY);
        selectionObjects[player].rectTransform.anchorMax = new Vector2(maxX, maxY);
        selectionPosition[player] = new Vector2Int(row, col);
    }

    void UpButton(bool val, PlayerInput player)
    {
        if (val)
        {
            HighlightChar(selectionPosition[player].x-1, selectionPosition[player].y, player);
        }
    }

    void DownButton(bool val, PlayerInput player)
    {
        if (val)
        {
            HighlightChar(selectionPosition[player].x+1, selectionPosition[player].y , player);
        }
    }

    void LeftButton(bool val, PlayerInput player)
    {
        if (val)
        {
            HighlightChar(selectionPosition[player].x , selectionPosition[player].y - 1, player);
        }
    }

    void RightButton(bool val, PlayerInput player)
    {
        if (val)
        {
            HighlightChar(selectionPosition[player].x, selectionPosition[player].y + 1, player);
        }
    }
    void SelectButton(bool val, PlayerInput player)
    {
        if (val)
        {
            var fighter = fighterSelectionMap.Get(selectionPosition[player]);
            if (fighter != null)
            {
                //If you already selected a fighter and selected a different one, that one must be a computer.
                foreach(var pair in selectedFighters)
                {
                    if (pair.input == player)
                    {
                        player = null;
                        break;
                    }
                }
                OnCharacterSelected(fighter, player);
            }
        }
    }

    public void StartButton(bool val)
    {
        if (val && selectedFighters.Count >= minNumFighters)
        {
            GeniousInputManager.instance.StopListeningForPlayers();
            FinishPickingCharacters();
        }
    }

    //Equidistant colors, halving the distance as we get farther into the function
    float GetColorBySplitting(int whichCut, float maxNum = 1f)
    {
        var log = Mathf.Log(whichCut, 2);
        float nextPowerOf2 = Mathf.IsPowerOfTwo(whichCut) ? log : Mathf.Ceil(log);//Mathf.Ceil();
        float cut = Mathf.Pow(2, nextPowerOf2);
        float offset = cut == 1f ? 0f : maxNum / (cut / 2f);
        float mod = cut % whichCut;
        var ret = (maxNum / cut) + offset * mod;
        //Debug.Log(whichCut + ".) " + nextPowerOf2 + " cut " + cut + " offsety " + offset + " mod " + mod + " = " + ret);
        return ret;
        //1 = 360
        //2 = 180 (/2)
        //3 = 270
        //4 = 90
        //5 = 315
        //6 = 225
        //7 = 135
        //8 = 45
    }

    void OnEnable()
    {
        InitializeInput();
    }

    Vector2 xRange;
    Vector2 yRange;
    List<FighterDataInputPair> selectedFighters = new List<FighterDataInputPair>();

    void OnCharacterClicked(BaseEventData bed)
    {
        var ped = bed as PointerEventData;
        if (ped != null)
        {
            int column = Mathf.FloorToInt(fighterSelectionMap.rows * ((ped.pressPosition.x - xRange.x) / (xRange.y-xRange.x)));
            int row = Mathf.FloorToInt(fighterSelectionMap.columns * (1 - ((ped.pressPosition.y - yRange.x) / (yRange.y-yRange.x))));
            var fd = fighterSelectionMap.Get(new Vector2Int(row, column));
            if (fd != null)
            {
                OnCharacterSelected(fd);
            }
        }
    }

    void OnCharacterSelected(FighterData fd, PlayerInput pi = null)
    {
        selectedFighters.Add(new FighterDataInputPair { fighterData = fd, input=pi});
        var go = new GameObject(fd.characterName);
        var img = go.AddComponent<Image>();
        img.sprite = fd.portrait;
        img.preserveAspect = true;
        go.transform.SetParent(selectedCharacterContainer);
        //(go.transform as RectTransform)
        go.transform.SetSiblingIndex(selectedFighters.Count-1);
        UpdateInstructions();
    }

    void FinishPickingCharacters()
    {
        Time.timeScale=1f;
        gameObject.SetActive(false);
        OnFightersSelected?.Invoke(selectedFighters);
    }

    void UpdateInstructions()
    {
        if (selectedFighters.Count >= minNumFighters)
        {
            instructions.text = "Press Start To Continue";
        }
    }
}