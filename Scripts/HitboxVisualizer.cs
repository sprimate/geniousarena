using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Prett coupled right now with MoveTesterManager, but probably not for long
public class HitboxVisualizer : MonoBehaviour, IHitboxActivator
{
    Color activeColor = Color.red;
    Color inactiveColor = Color.gray;
    public GameObject visualization;
    public void ActivateHitbox(Frame frames, Move m)
    {
        var hitbox = m.hitbox;
        visualization.transform.SetYLocalScale(hitbox.rect.height * transform.localScale.y);
        visualization.transform.SetZLocalScale(hitbox.rect.width * transform.localScale.z);
        visualization.transform.localPosition = new Vector3(visualization.transform.localPosition.x, visualization.transform.localScale.y / 2f + hitbox.rect.yMin * transform.localScale.y, visualization.transform.localScale.z / 2f + hitbox.rect.xMin * transform.localScale.z);
        SetHitboxActive(true);
        GenericCoroutineManager.instance.RunInFixedUpdateFrames(frames.value + 1, () => {
            SetHitboxActive(false);
        });
    }

    public void SetHitboxActive(bool val)
    {
        visualization.GetComponent<Renderer>().material.color = val ? activeColor : inactiveColor;
//        visualization.SetActive(val);
    }

    public void SetMove(UnparsedMove upm)
    {
        if (MoveTesterManager.instance.hitboxes.ContainsKey(upm.InstanceId))
        {
            var hitbox = MoveTesterManager.instance.hitboxes[upm.InstanceId];
            Debug.Log(MoveTesterManager.instance.moveDict[upm.InstanceId].name);
            visualization.transform.SetYLocalScale(hitbox.rect.height * transform.localScale.y);
            visualization.transform.SetZLocalScale(hitbox.rect.width * transform.localScale.z);
            visualization.transform.localPosition = new Vector3(visualization.transform.localPosition.x, visualization.transform.localScale.y/2f + hitbox.rect.yMin * transform.localScale.y, visualization.transform.localScale.z/2f + hitbox.rect.xMin * transform.localScale.z);
            //hitbox.center = new Vector3(hitbox.center.x, characterCenter + (entry.ymin * transform.localScale.y), entry.rect.center.x);
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        visualization = GameObject.CreatePrimitive(PrimitiveType.Cube);
        visualization.name = "Hitbox";
        visualization.transform.position = transform.position;
        visualization.GetComponent<Renderer>().material = Instantiate(MoveTesterManager.instance.hitboxMaterial);
        //visualization.transform.SetParent(transform);
        visualization.transform.localPosition = Vector3.zero;
        //visualization.gameObject.SetActive(false);
        inactiveColor.a = .3f;
        activeColor.a = 0.4f;
    }
}
