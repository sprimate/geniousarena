using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGeniousVirtualCamera : MonoBehaviour
{
    protected bool initialized;
    public virtual void OnInitialized() {
        initialized = true;
    }
    public virtual void OnUninitialized()
    {
        initialized = false;
    }
}
