using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Jobs;
using UnityEngine;

public class DamageCalculatorManager : MonoSingleton<DamageCalculatorManager>
{
    [Tooltip("In reference to streaming assets")]
    public string calculatorBinary = "Binaries/calculator_winx64.exe";

    CommandExecutor commandExecutor;
    protected void Start()
    {
       // calculatorBinary = "\"" + Path.Combine(Application.streamingAssetsPath, calculatorBinary).Replace('/', Path.DirectorySeparatorChar) +"\"";
        Debug.Log("Damage Calc Path: " + calculatorBinary);
        CommandExecutor.InstantiateNewCommandExecutor(
            calculatorBinary,
            (x) =>
            {
                Debug.Log("Sm4shCalculator Output: " + x);
            },
            (x) => {
                Debug.LogError("Sm4shCalculator Error: " + x);
            },
            (x) =>
            {
                Debug.Log("Sm4shCalculator Exited. Code: " + x);
            }
        );
    }

   /* public void OnDestroy()
    {
        commandExecutor.
    }*/
}