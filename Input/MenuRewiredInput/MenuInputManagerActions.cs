using Rewired;
public class MenuInputManagerActions : RewiredInputActions{
	public static string[] AllButtonActions = new string[] {"Pause","A","B","X","Y","RB","LB","Start","Back","LeftStickClick","RightStickClick","LTButton","RTButton","LeftStickVertical","RightStickHorizontal","RightStickVertical","DPadHoriziontal","DPadVert","Select","GoBack","Up","Down","Left","Right"};
	public override string[] AllButtonNames {get {return AllButtonActions;}}
	public static string[] AllAxisActions = new string[] {"RTAxis","LTAxis","LeftStickHorizontal"};
	public override string[] AllAxisNames {get {return AllAxisActions;}}
	public static string[] NotAnimatorActions = new string[] {"Pause","Select","GoBack","Up","Down","Left","Right"};
	public override string[] NotAnimatorActionNames {get {return NotAnimatorActions;}}

	public static InputAction Pause {get{return Get("Pause", typeof(MenuInputManagerActions));}}
	public static InputAction A {get{return Get("A", typeof(MenuInputManagerActions));}}
	public static InputAction B {get{return Get("B", typeof(MenuInputManagerActions));}}
	public static InputAction X {get{return Get("X", typeof(MenuInputManagerActions));}}
	public static InputAction Y {get{return Get("Y", typeof(MenuInputManagerActions));}}
	public static InputAction RB {get{return Get("RB", typeof(MenuInputManagerActions));}}
	public static InputAction RTAxis {get{return Get("RT Axis", typeof(MenuInputManagerActions));}}
	public static InputAction LB {get{return Get("LB", typeof(MenuInputManagerActions));}}
	public static InputAction LTAxis {get{return Get("LT Axis", typeof(MenuInputManagerActions));}}
	public static InputAction Start {get{return Get("Start", typeof(MenuInputManagerActions));}}
	public static InputAction Back {get{return Get("Back", typeof(MenuInputManagerActions));}}
	public static InputAction LeftStickClick {get{return Get("Left Stick Click", typeof(MenuInputManagerActions));}}
	public static InputAction RightStickClick {get{return Get("Right Stick Click", typeof(MenuInputManagerActions));}}
	public static InputAction LTButton {get{return Get("LT Button", typeof(MenuInputManagerActions));}}
	public static InputAction RTButton {get{return Get("RT Button", typeof(MenuInputManagerActions));}}
	public static InputAction LeftStickHorizontal {get{return Get("Left Stick Horizontal", typeof(MenuInputManagerActions));}}
	public static InputAction LeftStickVertical {get{return Get("Left Stick Vertical", typeof(MenuInputManagerActions));}}
	public static InputAction RightStickHorizontal {get{return Get("Right Stick Horizontal", typeof(MenuInputManagerActions));}}
	public static InputAction RightStickVertical {get{return Get("Right Stick Vertical", typeof(MenuInputManagerActions));}}
	public static InputAction DPadHoriziontal {get{return Get("DPad Horiziontal", typeof(MenuInputManagerActions));}}
	public static InputAction DPadVert {get{return Get("DPad Vert", typeof(MenuInputManagerActions));}}
	public static InputAction Select {get{return Get("Select", typeof(MenuInputManagerActions));}}
	public static InputAction GoBack {get{return Get("GoBack", typeof(MenuInputManagerActions));}}
	public static InputAction Up {get{return Get("Up", typeof(MenuInputManagerActions));}}
	public static InputAction Down {get{return Get("Down", typeof(MenuInputManagerActions));}}
	public static InputAction Left {get{return Get("Left", typeof(MenuInputManagerActions));}}
	public static InputAction Right {get{return Get("Right", typeof(MenuInputManagerActions));}}
}