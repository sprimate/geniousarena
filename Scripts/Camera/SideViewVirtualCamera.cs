using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class SideViewVirtualCamera : AGeniousVirtualCamera
{
    public CinemachineBrain brain;
    CinemachineVirtualCamera cam;
    public CinemachineTargetGroup targetGroup;
    public Vector3 minOffset;
    public Vector3 maxOffset = Vector3.positiveInfinity;
    public float viewingAngle = 12.5f;

    /// <summary>
    /// A percentage of the total width between characters that is added to the sides
    /// </summary>
    [Range(0f, 1f)]
    public float horizontalBorder = 0.2f;
   //public Vector3 maxOffset;
    public float maxDeltaPerSecond = 50.94f;
    public float positionSmoothDamp = 0.25f;
    public float zoomSmoothDamp = 0.25f;
    public float fovOffsetPercentage = .3f;
    public float minFov;
    public float maxFov;
    float zoomVelocity;
    Vector3 currentPosVelocity;

    // Update is called once per frame
    void LateUpdate()
    {
        var ogRotation = transform.rotation;
        var ogPos = transform.position;

        var requiredDistance = GetGreatestDistance() / brain.OutputCamera.aspect * 0.5f / Mathf.Tan(cam.m_Lens.FieldOfView * 0.5f * Mathf.Deg2Rad);
        float wantedDistance = (requiredDistance + horizontalBorder * requiredDistance);
        Vector3 functionalOffset = minOffset * Mathf.Abs((wantedDistance / minOffset.z));
        if (Mathf.Abs(functionalOffset.z) < Mathf.Abs(minOffset.z)) //magnitude might be a better check
        {
            functionalOffset = minOffset;
        }

        if (Mathf.Abs(functionalOffset.z) > Mathf.Abs(maxOffset.z))
        {
            functionalOffset = maxOffset;
        }

        transform.position = targetGroup.transform.position;
        var direction = (transform.position - targetGroup.m_Targets[0].target.position);

        //90 degrees one way
        var toLookAt = Quaternion.AngleAxis(90, Vector3.up) * direction;
        if (toLookAt != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(toLookAt);
        }
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);

        transform.Translate(functionalOffset, Space.Self);
        Vector3 pos1 = transform.position;

        transform.Translate(-functionalOffset, Space.Self);

        //90 degrees the other way

        toLookAt = Quaternion.AngleAxis(-90, Vector3.up) * direction;
        if (toLookAt != null)
        {
            transform.rotation = Quaternion.LookRotation(toLookAt);
        }
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);

        transform.Translate(functionalOffset, Space.Self);
        Vector3 pos2 = transform.position;

        Vector3 finalPos = Vector3.Distance(ogPos, pos1) < Vector3.Distance(ogPos, pos2) ? pos1 : pos2;

        transform.position = Vector3.SmoothDamp(ogPos, finalPos, ref currentPosVelocity, positionSmoothDamp);
        transform.rotation = ogRotation;

        var angle = Vector3.Angle(transform.forward, targetGroup.m_Targets[0].target.position - transform.position);
        float newZoom = Mathf.Max(minFov, angle + angle * fovOffsetPercentage);
        cam.m_Lens.FieldOfView = Mathf.SmoothDamp(cam.m_Lens.FieldOfView, newZoom, ref zoomVelocity, zoomSmoothDamp);
        cam.m_Lens.FieldOfView = Mathf.Min(cam.m_Lens.FieldOfView, maxFov);
        cam.transform.SetXLocalRotation(viewingAngle);
    }

    public void Start()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targetGroup.m_Targets[0].target.position, Vector3.zero);
        foreach(var t in targetGroup.m_Targets)
        {
            bounds.Encapsulate(t.target.position);
        }

        return bounds.size.x;
    }
    float ogViewingAngle;
    public override void OnInitialized()
    {
        base.OnInitialized();
        if (!brain.gameObject.activeInHierarchy)
        {
            Debug.Log("WARNING: Camera brain for this component not active", this);
        }

        ogViewingAngle = cam.transform.eulerAngles.x;
        if (GeniousSettings.instance.disallowVerticalMovementInSideView)
        {
            GeniousSettings.instance.allowZMovement = false;
        }

        Vector3 average = Vector3.zero;
        foreach (var target in targetGroup.m_Targets)
        {
            average += target.target.transform.position;
        }

        average /= targetGroup.m_Targets.Length;
        transform.LookAt(average);
    }

    public override void OnUninitialized()
    {
        base.OnUninitialized();
        GeniousSettings.instance.allowZMovement = true;
        cam.transform.SetXLocalRotation(ogViewingAngle);
    }
}