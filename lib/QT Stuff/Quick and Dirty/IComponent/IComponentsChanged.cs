﻿using UnityEngine;
namespace QuickAndDirty {
    public interface IComponentTracker<T> where T : Component {
        
        void ComponentAdded(T comp);
        void ComponentRemoved(T comp);
    }

    public static class IComponentExtensions
    {
        public static void AnnounceComponentAdded<T>(this T self, bool includeInactive = false) where T : Component
        {
            var trackers = self.GetComponentsInChildren<IComponentTracker<T>>(includeInactive);
            foreach (var tracker in trackers)
            {
                tracker.ComponentAdded(self);
            }
        }
        public static void AnnounceComponentRemoved<T>(this T self, bool includeInactive = false) where T : Component
        {
            var trackers = self.GetComponentsInChildren<IComponentTracker<T>>(includeInactive);
            foreach (var tracker in trackers)
            {
                tracker.ComponentRemoved(self);
            }
        }
    }

    public static class LazyExtension
    {
        public static T Comp<T>(this Component self, ref T reference) where T : Component
        {
            return reference ?? (reference = self.GetComponent<T>());
        }
    }
}