using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using UnityEngine.Events;
using System;

/// <summary>
/// Remote GPS transmitter
/// </summary>
[Serializable]
[RequireComponent(typeof(SocketIOComponent))]
public class SocketManager : MonoSingleton<SocketManager>
{

    protected override bool ShouldDestroyOnLoad { get { return true; } }
    const string ON_SOCKET_CONNECTED_KEY = "onconnected";
	const string SOCKET_MESSAGE_KEY = "message";

	[Serializable]
	public class JSONAction : UnityEvent<JSONObject> { }
	[Serializable]
	public class SocketIOEventAction : UnityEvent<SocketIOEvent> { }

	string id;

	[Serializable]
	public class BoolAction : UnityEvent<bool> { }
	[SerializeField] BoolAction onConnectionStatusChanged;

	//public JSONAction onGps;
	public JSONAction onMessage;
	public JSONAction onConnectedSocketEvent;

	[Serializable]
	public struct MessageAction {
		public string messageKey;
		public SocketIOEventAction action;
	}
	[SerializeField] List<MessageAction> messageActions;

	[SerializeField]
	SocketIOComponent _socketIO;
	protected SocketIOComponent socketIo 
	{
		get 
		{
			if (_socketIO == null)
			{
				_socketIO = GetComponent<SocketIOComponent>();
			}
			return _socketIO;
		}
		set
		{
			_socketIO = value;
		}
	}
	// Use this for initialization
	void Start()
	{
        if (socketIo == null)
        {
            socketIo = GetComponent<SocketIOComponent>();
            if (socketIo == null)
            {
                Debug.LogError("Can't find SocketIOComponent");
                return;
            }
        }
		socketIo.On(ON_SOCKET_CONNECTED_KEY, OnConnectedSocketEvent);
		socketIo.On(SOCKET_MESSAGE_KEY, OnMessage);
        if (messageActions == null)
        {
            messageActions = new List<MessageAction>();
        }
		foreach (MessageAction a in messageActions) 
		{
			socketIo.On (a.messageKey, (parameter) => {a.action.Invoke(parameter);});
		}
	}
	bool wasConnected = false;

	public void On(string eventKey, Action<SocketIOEvent> callback)
	{
		socketIo.On (eventKey, callback);
	}
	public void Off(string eventKey, Action<SocketIOEvent> callback)
	{
		socketIo.Off (eventKey, callback);
	}
	void Update()
	{
		if (socketIo.IsConnected != wasConnected)
		{
            if (onConnectionStatusChanged != null)
            {
                onConnectionStatusChanged.Invoke(socketIo.IsConnected);
            }
			wasConnected = socketIo.IsConnected;
		}
	}

    public void Listen()
	{
		gameObject.SetActive (true);
	}

	public void StopListening()
	{
		gameObject.SetActive (false);
	}
		

	void OnMessage(SocketIOEvent evt)
	{
		Debug.Log("Message: " + evt.data);
		onMessage.Invoke(evt.data);
	}

	void OnConnectedSocketEvent(SocketIOEvent evt)
	{
		id = evt.data.GetField("id").str;
		Debug.Log("SocketManager Connected - ID:  " + id);
		onConnectedSocketEvent.Invoke(evt.data);
		onConnectionStatusChanged.Invoke(socketIo.IsConnected); //Connection ID has been updated
	}

	public void ConnectToNewUrl(string newUrl)
	{
		Debug.Log("URL Changed. " + newUrl);
		if (socketIo.url != newUrl)
		{
			bool autoConnectBefore = socketIo.autoConnect;
			socketIo.autoConnect = false;
			if (socketIo.IsConnected)
			{
				socketIo.Close();
				id = "";
			}
			socketIo.url = newUrl;
			socketIo.Connect();
			socketIo.autoConnect = autoConnectBefore;
		}
	}

	public string GetId()
	{
		return id;
	}
}