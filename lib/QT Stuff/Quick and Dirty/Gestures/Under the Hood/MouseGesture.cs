using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class MouseGesture : Gesture
    {
        Vector2 _position;
        Vector2 lastPosition;
        float lastUpdate;

        public MouseGesture(int id)
            : base(id)
        {
            _fingerID = id;
            _dirty = true;
        }

        //Touch _touch;
        private Touch touch
        {
            get
            {
                Debug.Log("DO NOT ACCESS MouseGesture.touch");
                return new Touch();
            }
        }

        public override float deltaTime
        {
            get
            {
                return Time.timeSinceLevelLoad - lastUpdate;
            }
        }

        public override Vector2 deltaPosition
        {
            get
            {
                return _position - lastPosition;
            }
        }

        public override Vector2 totalDeltaPosition
        {
            get
            {
                if (isEnded)
                    return _upPos - _downPos;
                else
                    return lastPosition - _downPos;
            }
        }

        public override Vector2 position
        {
            get
            {
                return _position;
            }
        }
        TouchPhase _phase;
        public override TouchPhase phase
        {
            get
            {
                return _phase;
            }
        }

        public void UpdateMouse()
        {
            //Debug.Log("Update MouseGesture");

            //_fingerID = fingerId;


            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("MouseGesture began");
                _phase = TouchPhase.Began;
                _isEnded = false;
                _dirty = false;
                _downPos = Input.mousePosition;
                _downTime = Time.timeSinceLevelLoad;

                lastPosition = _downPos;
                _position = _downPos;
                lastUpdate = Time.timeSinceLevelLoad;
            }
            else if (Input.GetMouseButton(0))
            {
                lastPosition = position;
                _position = Input.mousePosition;
                lastUpdate = Time.timeSinceLevelLoad;

                //TouchPhase moved/stationary
                if (deltaPosition.sqrMagnitude > 0f)
                {
                    _phase = TouchPhase.Moved;
                }
                else
                {
                    _phase = TouchPhase.Stationary;
                }
            }
            else if (_phase != TouchPhase.Ended)
            {
                //Debug.Log("MouseGesture ended, phase " + this.phase);
                _phase = TouchPhase.Ended;
                //Debug.Log("Phased ended obviously " + this.phase);
                _isEnded = true;
                _upPos = Input.mousePosition;
                _upTime = Time.timeSinceLevelLoad;

                lastPosition = position;
                _position = Input.mousePosition;
                lastUpdate = Time.timeSinceLevelLoad;
            }
        }
    }
}