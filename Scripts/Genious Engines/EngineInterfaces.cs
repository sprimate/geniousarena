using System;
using Gamekit3D;
using UnityEngine;

public interface IAirDodgeable
{
    void TriggerAirDodge();
}

public interface IPercentable
{
    float percent {get;}
}

public interface IFloorable
{
    void TriggerGetUp();
}

public interface ITargetable
{
    ADamageable currentTarget {get;}
}

public interface IKillable
{
    void Kill();
}

public interface IRollable
{
    bool CheckRoll();
    void TriggerRoll();
}

public interface IHitStunnable
{
    void CancelHitStun(bool allowFlooredLanding);
    void CancelHitStun();//Defaults to false;
    bool hitStunAerialCancellable { get; }// = true;
    
    bool hitStunAirDodgeCancellable {get;}
}

public interface IShieldable
{
    FighterShield shield {get;}
    float maxShield { get; }
    float shieldDepletionPerFrame {get;}
    float shieldRegenerationPerFrame {get;}
    void SetShield(bool isOn);
    void AdjustShieldHealth(float toAdjust);
}
