using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class CommandExecutor : MonoBehaviour
{
    const string proccessesLockFile = "processes.lock";
    public void StandardIn(string value)
    {
        if (process == null)
        {
            UnityEngine.Debug.LogError("Cannot send data to standard in - process does not exist: [" + value + "]");
        }
    }

    static bool LockFileExists()
    {
        return File.Exists(GetLockFilePath());
    }

    static string GetLockFilePath()
    {
        string path = Application.persistentDataPath + "/" + proccessesLockFile;
        path = path.Replace('\\', '/').Replace('/', Path.DirectorySeparatorChar);
        return path;
    }

    static List<string> GetActiveProcesses()
    {
        var ret = new List<string>();
        if (LockFileExists())
        {
            foreach(var s in File.ReadAllText(GetLockFilePath()).Split('\n'))
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    ret.Add(s);
                }
            }
        }
        return ret;
    }

    static void AddToActiveProccesses(int pid, CommandExecutor exec)
    {
        bool isFromPlayMode = exec != null && Application.IsPlaying(exec);
        string toAdd = pid.ToString() + "," + isFromPlayMode.ToString() + "\n";
        if (!LockFileExists())
        {
            using (StreamWriter sw = File.CreateText(GetLockFilePath()))
            {
                sw.WriteLine(toAdd);
            }
        }
        else 
        {
            File.AppendAllText(GetLockFilePath(), toAdd);
        }
    }

    public static void KillAllActiveProcesses(bool playModeProcessesOnly = false)
    {
        foreach(var p in GetActiveProcesses())
        {
            //pid,wasStartedInPlayMode
            var options = p.Split(',');
            if (!playModeProcessesOnly || bool.Parse(options[1].Trim()))
            {
                KillProcessTree(int.Parse(options[0]));
            }
        }
    }

    public void KillProcess()
    {
        if (process != null)
        {
            process.StandardInput.Close();
            KillProcessTree(process);
            process = null;
        }
    }

    public static void KillProcessTree(Process process)
    {
        if (process != null)
        {
            KillProcessTree(process.Id);
        }
    }

    public static void KillProcessTree(int pid)
    {
        bool removeFromFile = true;
        try
        {
            Process process = Process.GetProcessById(pid);
            if (process != null)
            {
                KillChildProcesses(pid);
                KillThisProcess(process);
            }
        }
        catch(ArgumentException ae)
        {
            Debug.LogError(ae);
        }
        catch(Exception e)
        {
            removeFromFile = false;
            Debug.LogError(e);
        }
        finally
        {
            if (removeFromFile)
            {
                string contents = File.ReadAllText(GetLockFilePath())
                   .Replace(pid.ToString() + ",True\n", "")
                   .Replace(pid.ToString() + ",False\n", "");

                if (contents.Trim().Length == 0)
                {
                    File.Delete(GetLockFilePath());
                }
                else
                {
                    File.WriteAllText(GetLockFilePath(), contents);
                }
            }
        }
    }

    static void KillChildProcesses(int pid)
    {
        ProcessStartInfo processInfo;
        Process p;
        processInfo = new ProcessStartInfo("cmd.exe", "/c wmic process where (ParentProcessId=" + pid + ") get ProcessId");
        processInfo.CreateNoWindow = true;
        processInfo.UseShellExecute = false;
        // *** Redirect the output ***
        processInfo.RedirectStandardError = true;
        processInfo.RedirectStandardOutput = true;

        p = Process.Start(processInfo);
        p.WaitForExit();

        //Stack Overflow says:
        // *** Read the streams ***
        // Warning: This approach can lead to deadlocks, see Edit #2
        string output = p.StandardOutput.ReadToEnd();
        string error = p.StandardError.ReadToEnd();
        p.Close();

        foreach(var l in output.Split('\n'))
        {
            int id;
            if (int.TryParse(l, out id))
            {
                Process.GetProcessById(id)?.Kill();
            }
        }
    }

    static void KillThisProcess(Process process)
    {
        process.Kill();
    }

    Process process;
    public static CommandExecutor InstantiateNewCommandExecutor(string command, Action<string> OnOutput, Action<string> OnError, Action<int> OnExit)
    {
        GameObject newGo = new GameObject("Command Executor");
        var ret= newGo.AddComponent<CommandExecutor>();
        ret.Execute(command, OnOutput, OnError, OnExit);
        return ret;
    }

    static Process ExecuteInternal(string command, Action<string> OnOutput, Action<string> OnError, Action<int> OnExit)
    {
//        OnExit = _OnExit;
        var process = new Process();
        // Log?.Invoke("CommandPrompt: " + commandPromptApplication);
        process.StartInfo.FileName = "cmd.exe";
        process.StartInfo.RedirectStandardInput = true;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.UseShellExecute = false;
        process.Start();

        //var cmd = bashCommand ? "C:\\Windows\\System32\\bash.exe -c \"" + command + "\"" : command;
        var cmd = "\"" + command + "\"";
        process.StandardInput.WriteLine(cmd);// commandRequest.command);
        process.StandardInput.Flush();
        UnityEngine.Debug.Log("Processing Request (" + process.Id + "): " + cmd);

        //  commandRequest.processId = process.Id;
        //processes[process.Id] = process;
        process.OutputDataReceived += (object sender, System.Diagnostics.DataReceivedEventArgs e) =>
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                OnOutput?.Invoke(e.Data);
            }
        };

        process.BeginOutputReadLine();
        process.ErrorDataReceived += (object sender, System.Diagnostics.DataReceivedEventArgs e) =>
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                OnError?.Invoke(e.Data);
            }
        };
        process.BeginErrorReadLine();

        Thread processWatcherThread = new Thread(() =>
        {
            if (process != null && process.HasExited)
            {
                    process.StandardInput.Close();
                    ((Action<int>)OnExit)?.Invoke(process.ExitCode);
                    process.Close();
                    process = null;
                //Exit();
            }
            Thread.Sleep(100);
        });

        processWatcherThread.Start();
        return process;
    }

    //Action<int> OnExit;
    public void Execute(string command, Action<string> OnOutput, Action<string> OnError, Action<int> OnExit)
    {
        process = ExecuteInternal(command, OnOutput, OnError, OnExit);
        AddToActiveProccesses(process.Id, this);
    }

    public static Process ExecuteStatic(string command, Action<string> OnOutput, Action<string> OnError, Action<int> OnExit)
    {
        var process = ExecuteInternal(command, OnOutput, OnError, OnExit);
        AddToActiveProccesses(process.Id, null);
        return process;
    }

    void Exit()
    {
        CloseProcess();
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            Destroy(gameObject); //Needs to be run from Unity thread
        });
    }

    void CloseProcess()
    {
        
    }

    void OnDestroy()
    {
        KillProcess();
    }
}