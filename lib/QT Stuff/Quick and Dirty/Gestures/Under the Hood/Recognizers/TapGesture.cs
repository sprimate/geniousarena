using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class TapGesture : GestureRecognizer
    {
        public float longHoldTime;
        public Vector2 maxDelta;

        public TapGesture()
            : base()
        {
            longHoldTime = QADGestures.instance.longHoldTime;
            maxDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture tap)
        {
            return tap.phase == TouchPhase.Ended
                && tap.totalDeltaPosition.sqrMagnitude <= maxDelta.sqrMagnitude
                && tap.timeHeld < longHoldTime;
        }
    }
}