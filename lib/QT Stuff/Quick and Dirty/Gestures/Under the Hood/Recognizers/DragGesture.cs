using UnityEngine;
namespace QuickAndDirty.Gestures {

    public class DragGesture : GestureRecognizer
    {
        public float dragHoldTime;
        public Vector2 minDelta;

        public DragGesture()
            : base()
        {
            dragHoldTime = QADGestures.instance.dragHoldTime;
            minDelta = QADGestures.instance.minDragDelta;
        }

        public override bool Recognize(Gesture gesture)
        {
            //Debug.Log(gesture.phase + " " + gesture.deltaPosition + " " + gesture.timeHeld);
            return gesture.deltaPosition.sqrMagnitude > minDelta.sqrMagnitude
                && gesture.timeHeld > dragHoldTime;
        }
    }
}