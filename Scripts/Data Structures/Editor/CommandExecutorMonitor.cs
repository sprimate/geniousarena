using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Rewired;

// ensure class initializer is called whenever scripts recompile
[InitializeOnLoadAttribute]
public static class CommandExecutorMonitor
{
    static CommandExecutorMonitor()
    {
        EditorApplication.playModeStateChanged += CloseAllPlayModeProccesses;
    }

    private static void CloseAllPlayModeProccesses(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredEditMode)
        {
            CommandExecutor.KillAllActiveProcesses();
        }
    }
}