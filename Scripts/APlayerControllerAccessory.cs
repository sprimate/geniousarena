using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class APlayerControllerAccessory : MonoBehaviour
{
    public FighterController playerController;

    public IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        playerController = GetComponentInParent<FighterController>();
        AfterStart();
    }

    protected virtual void AfterStart() { }
}
