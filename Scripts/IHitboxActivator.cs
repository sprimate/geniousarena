using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHitboxActivator
{
    void SetHitboxActive(bool val);
    void ActivateHitbox(Frame frames, Move move);
}
