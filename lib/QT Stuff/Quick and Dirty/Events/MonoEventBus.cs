using UnityEngine;
using UnityEngine.Events;

namespace QuickAndDirty {
	
public class MonoEventBus : MonoBehaviour {

	[SerializeField] UnityEvent _onBecameVisible;
	[SerializeField] UnityEvent _onBecameInvisible;
	[SerializeField] UnityEvent _onEnable;
	[SerializeField] UnityEvent _onDisable;
	[SerializeField] UnityEvent _onDestroy;
	
	void OnBecameVisible()
	{
		_onBecameVisible.Invoke();
	}
	
	void OnBecameInvisible()
	{
		_onBecameInvisible.Invoke();
	}
	
	void OnDestroy()
	{
		_onDestroy.Invoke();
	}
	
	void OnEnable()
	{
		_onEnable.Invoke();
	}
	
	void OnDisable()
	{
		_onDisable.Invoke();
	}
}
}