using UnityEngine;
using System.Collections;

namespace QuickAndDirty {
public static class FadeRoutines {

        public static IEnumerator FadeMaterialColor(float time, ColorRange fromTo, Material material, string property = "_Color")
        {
            float clock = 0f;

            while (clock < time)
            {
                clock += Time.deltaTime;
                float t = clock / time;
                material.SetColor(property, fromTo.Sample(t));
                yield return 0;
            }
            material.SetColor(property, fromTo.max);
        }
}
}