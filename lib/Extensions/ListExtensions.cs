using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static T GetCircular<T>(this List<T> list, int i)
    {
        i = i % (list.Count);
        if (i < 0)
        {
            i += list.Count;
        }

        if (list != null && list.Count > 0 && i >= 0)
        {
            return list[i];
        }

        return default(T);
    }

    public static T GetRandom<T>(this List<T> list)
    {
        if (list != null && list.Count > 0)
        {
            return list[Random.Range(0, list.Count)];
        }

        return default(T);
    }
}
