using UnityEngine;
using System;
using System.Collections;
namespace QuickAndDirty {
    public static class WWWRoutines
    {
        public static IEnumerator DownloadTexture(string url, System.Action<Texture2D> textureDownloaded)
        {
            using (WWW www = new WWW(url)) {
                yield return www;
                textureDownloaded(www.texture);
            }
        }
    }
}