using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GeniousEffectManager : MonoSingleton<GeniousEffectManager>
{
    protected override bool ShouldDestroyOnLoad { get { return true; } }

    [SerializeField] string layerName = "Effects";
    [SerializeField] public List<GameObject> hitEffects;
    [SerializeField] public List<GameObject> deathEffects;
    [SerializeField] public List<GameObject> swapCharEffects;
    Dictionary<GameObject, GameObject> poolReference = new Dictionary<GameObject, GameObject>();
    protected override void Awake()
    {
        base.Awake();
        foreach(var effect in hitEffects.Concat(deathEffects).Concat(swapCharEffects))
        {
            var go = Instantiate(effect);
            go.SetActive(false);
            if (go.HasComponent<CFX_AutoDestructShuriken>())
            {
                go.GetComponent<CFX_AutoDestructShuriken>().OnlyDeactivate = true;
            }

            go.SetLayerRecursive(layerName);

            foreach (var ps in go.GetComponentsInChildren<ParticleSystem>())
            {
                var main = ps.main;
                main.useUnscaledTime = true;
            }
            poolReference[effect] = go;
            PoolManager.WarmPool(go, 5);
        }
    }

   /* void ReadyPool(GameObject go, )
    {

    }*/
    public void GenerateSwapCharacterEffect(Vector3 position, Action onEffectComplete = null, int? index = null)
    {
        GenerateEffect(position, onEffectComplete, index, swapCharEffects);
    }

    public void GenerateDeathEffect(Vector3 position, Action onEffectComplete = null, int? index = null)
    {
        GenerateEffect(position, onEffectComplete, index, deathEffects);
    }

    public void GenerateHitEffect(Vector3 position, Action onEffectComplete = null, int? index = null)
    {
        GenerateEffect(position, onEffectComplete, index, hitEffects);
    }

    void GenerateEffect(Vector3 position, Action onEffectComplete, int? index, List<GameObject> effects)
    {
        if (!index.HasValue)
        {
            index = UnityEngine.Random.Range(0, effects.Count);
        }

        //GameObject g = Instantiate(effects[index.Value], position, Quaternion.LookRotation(Camera.main.transform.position - position), transform);

        GameObject g = SpawnObject(effects[index.Value], position);
        if (onEffectComplete != null)
        {
            StartCoroutine(MonitorGameObjectSurvival(g, onEffectComplete));
        }
    }

    GameObject SpawnObject(GameObject g, Vector3 position)
    {
        if (!poolReference.ContainsKey(g))
        {
            //Add to the pool
        }
        return PoolManager.SpawnObject(poolReference[g], position, Quaternion.LookRotation(Camera.main.transform.position - position));
    }

    IEnumerator MonitorGameObjectSurvival(GameObject g, Action callback)
    {
        while (g != null && g.activeSelf)
        {
            yield return new WaitForFixedUpdate();
        }

        callback?.Invoke();
        PoolManager.ReleaseObject(g);
    }
}
