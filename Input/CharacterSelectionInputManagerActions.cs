using Rewired;
public class CharacterSelectionInputManagerActions : RewiredInputActions{
	public static string[] AllButtonActions = new string[] {"Select","Start","Back","Up","Down","Left","Right"};
	public override string[] AllButtonNames {get {return AllButtonActions;}}
	public static string[] AllAxisActions = new string[] {"LeftStickHorizontal","LeftStickVertical"};
	public override string[] AllAxisNames {get {return AllAxisActions;}}
	public static string[] NotAnimatorActions = new string[] {"Select","Start","LeftStickHorizontal","LeftStickVertical","Back","Up","Down","Left","Right"};
	public override string[] NotAnimatorActionNames {get {return NotAnimatorActions;}}

	public static InputAction Select {get{return Get("Select", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Start {get{return Get("Start", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction LeftStickHorizontal {get{return Get("Left Stick Horizontal", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction LeftStickVertical {get{return Get("Left Stick Vertical", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Back {get{return Get("Back", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Up {get{return Get("Up", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Down {get{return Get("Down", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Left {get{return Get("Left", typeof(CharacterSelectionInputManagerActions));}}
	public static InputAction Right {get{return Get("Right", typeof(CharacterSelectionInputManagerActions));}}
}