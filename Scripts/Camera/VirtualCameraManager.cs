using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualCameraManager : MonoSingleton<VirtualCameraManager>
{
    public List<CinemachineVirtualCameraBase> cameras;
    int currentIndex = -1;
    public int lastCameraIndex;
    public int preferredCameraIndex;
    public GeniousTransformFollow followPlayer1;
    public GeniousTransformFollow followPlayer2;
    public CinemachineTargetGroup targetGroup;
    
    void Start()
    {
        SetCameraIndex(0);
    }

    public void SetCameraIndex(int newIndex)
    {
        SetCurrentCameraInitialized(false);
        newIndex = newIndex < 0 || newIndex >= cameras.Count ? 0 : newIndex;
        if (newIndex != currentIndex)
        {
            lastCameraIndex = currentIndex;
            currentIndex = newIndex;
        }
        for (int i = 0; i < cameras.Count; i++)
        {
            cameras[i].Priority = i == currentIndex ? 1000 : 0;
        }

        SetCurrentCameraInitialized(true);
        Debug.Log("Set to " + cameras[currentIndex], this);
    }

    void SetCurrentCameraInitialized(bool val)
    {
        if (currentIndex >= cameras.Count || currentIndex < 0)
        {
            return;
        }
        var currentCamera = cameras[currentIndex].GetComponent<AGeniousVirtualCamera>();
        if (currentCamera != null)
        {
            if (val)
            {
                currentCamera.OnInitialized();
            }
            else
            {
                currentCamera.OnUninitialized();
            }
        }
    }

    public void SetupCameras(List<FighterController> players)
    {
        followPlayer1.target = players.GetCircular(0)?.transform;
        followPlayer1.target2 = players.GetCircular(1)?.transform;
        followPlayer2.target = players.GetCircular(1)?.transform;
        followPlayer2.target2 = players.GetCircular(0)?.transform;
        targetGroup.m_Targets = new CinemachineTargetGroup.Target[players.Count];
        for (int i = 0; i < players.Count; i++)
        {
            targetGroup.m_Targets[i] = new CinemachineTargetGroup.Target();
            targetGroup.m_Targets[i].target = players[i].transform;
            targetGroup.m_Targets[i].weight = 1;
            targetGroup.m_Targets[i].radius = 0;
        }
    }

    public void LastOrPreferred()
    {
        var newIndex = currentIndex == preferredCameraIndex ? lastCameraIndex : preferredCameraIndex;
        SetCameraIndex(newIndex);
    }
    public void Next()
    {
        SetCameraIndex(currentIndex + 1);
    }
}