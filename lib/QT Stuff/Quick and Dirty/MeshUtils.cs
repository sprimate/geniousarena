﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuickAndDirty {

    public static class MeshUtils {

        public enum MeshPositionMode {
            Vertex, Edge, Triangle
        }

        public static Vector3 RandomPoint(this Mesh mesh, MeshPositionMode mode = MeshPositionMode.Triangle)
        {
            if (!mesh.isReadable)
            {
                Debug.LogErrorFormat("Can't read from mesh {0}, set read/write enabled in import settings.", mesh.name);
                return default(Vector3);
            }

            if (mode == MeshPositionMode.Edge) {
                return GetRandomEdgePosition(mesh.vertices, mesh.triangles);
            }
            else if (mode == MeshPositionMode.Vertex) {
                return GetRandomVertexPosition(mesh.vertices);
            }
            else {
                return GetRandomTrianglePosition(mesh.vertices, mesh.triangles);
            }
        }
        private static Vector3 GetRandomTrianglePosition(Vector3[] vertices, int[] triangles)
        {
            int t = Random.Range(0, triangles.Length / 3) * 3;

            Vector3 a = vertices[triangles[t]];
            Vector3 b = vertices[triangles[t + 1]];
            Vector3 c = vertices[triangles[t + 2]];

            Vector3 ab = Vector3.Lerp(a, b, Random.value);
            Vector3 abc = Vector3.Lerp(ab, c, Random.value);
            return abc;
        }
        private static Vector3 GetRandomVertexPosition(Vector3[] vertices)
        {
            return vertices[Random.Range(0, vertices.Length)];
        }
        private static Vector3 GetRandomEdgePosition(Vector3[] vertices, int[] triangles)
        {
            int t = Random.Range(0, triangles.Length / 3) * 3;
            int a = Random.Range(0, 3);
            int b = (a + 1) % 3;

            Vector3 ab = Vector3.Lerp(vertices[triangles[t + a]], vertices[triangles[t + b]], Random.value);
            return ab;
        }

        public static PointNormal RandomPoNorm(this Mesh mesh, MeshPositionMode mode = MeshPositionMode.Triangle)
        {

            if (!mesh.isReadable)
            {
                Debug.LogErrorFormat("Can't read from mesh {0}, set read/write enabled in import settings.", mesh.name);
                return default(PointNormal);
            }

            if (mode == MeshPositionMode.Edge) {
                return GetRandomEdgePointNormal(mesh.vertices, mesh.normals, mesh.triangles);
            }
            else if (mode == MeshPositionMode.Vertex) {
                return GetRandomVertexPosition(mesh.vertices, mesh.normals);
            }
            else {
                return GetRandomTrianglePointNormal(mesh.vertices, mesh.normals, mesh.triangles);
            }
        }

        private static PointNormal GetRandomTrianglePointNormal(Vector3[] vertices, Vector3[] normals, int[] triangles)
        {
            int t = Random.Range(0, triangles.Length / 3) * 3;

            Vector3 a = vertices[triangles[t]];
            Vector3 an = normals[triangles[t]];
            Vector3 b = vertices[triangles[t + 1]];
            Vector3 bn = normals[triangles[t + 1]];
            Vector3 c = vertices[triangles[t + 2]];
            Vector3 cn = normals[triangles[t + 2]];

            float ra = Random.value;
            float rb = Random.value;

            Vector3 ab = Vector3.Lerp(a, b, ra);
            Vector3 abn = Vector3.Lerp(an, bn, ra);
            Vector3 abc = Vector3.Lerp(ab, c, rb);
            Vector3 abcn = Vector3.Lerp(abn, cn, rb);
            return new PointNormal {point = abc, normal = abcn};
        }

        private static PointNormal GetRandomEdgePointNormal(Vector3[] vertices, Vector3[] normals, int[] triangles)
        {
            int t = Random.Range(0, triangles.Length / 3) * 3;
            int a = Random.Range(0, 3);
            int b = (a + 1) % 3;
            float ra = Random.value;

            Vector3 ab = Vector3.Lerp(vertices[triangles[t + a]], vertices[triangles[t + b]], ra);
            Vector3 abn = Vector3.Lerp(normals[triangles[t + a]], normals[triangles[t + b]], ra);
            return new PointNormal{point = ab, normal = abn};
        }
        private static PointNormal GetRandomVertexPosition(Vector3[] vertices, Vector3[] normals)
        {
            int a = Random.Range(0, vertices.Length);
            return new PointNormal { point = vertices[a], normal = normals[a]};
        }
    
    }
    public struct PointNormal {
        public Vector3 point;
        public Vector3 normal;
    }
}
