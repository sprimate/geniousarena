using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreRootMotionFromAnimator : FighterAnimatorScript
{
    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateMove(animator, stateInfo, layerIndex);
        controlla.ignoreRootMotionOnThisFrame = GeniousSettings.fixedFrame;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        controlla.ignoreRootMotionOnThisFrame = GeniousSettings.fixedFrame;
    }
}