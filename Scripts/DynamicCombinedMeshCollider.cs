using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCombinedMeshCollider : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        Vector3 ogPosition = transform.position;
        transform.position = Vector3.zero;
        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.worldToLocalMatrix * Matrix4x4.Scale(transform.lossyScale);// localToWorldMatrix;
            //combine[i].transform = Matrix4x4.Translate(transform.position);
            //meshFilters[i].gameObject.SetActive(false);
            i++;
        }

        Mesh m = new Mesh();
        
        m.CombineMeshes(combine);
        transform.GetOrAddComponent<MeshCollider>().sharedMesh = m;
        transform.GetOrAddComponent<MeshCollider>().convex = true;
        transform.position = ogPosition;
    }
}