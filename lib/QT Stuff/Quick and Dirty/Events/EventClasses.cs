﻿using System;
using UnityEngine.Events;

namespace QuickAndDirty {
    [Serializable]
    public class StringEvent : UnityEvent<string>
    {
        
    }

    [Serializable]
    public class FloatEvent : UnityEvent<float>
    {

    }

    [Serializable]
    public class IntEvent : UnityEvent<int>
    {

    }

    [Serializable]
    public class Vec2Event : UnityEvent<UnityEngine.Vector2>
    {
        
    }
}