using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineFreeLook))]
public class FreeLookVirtualCamera : AGeniousVirtualCamera
{
    public string horizontalCameraInputAxisName;
    public string verticalCameraInputAxisName;
    CinemachineFreeLook freeLook;

    FighterController activePlayer;
    public override void OnInitialized()
    {
        base.OnInitialized();
        freeLook = GetComponent<CinemachineFreeLook>();
        if (activePlayer == null)
        {
            activePlayer = GeniousSettings.instance.fighters.GetCircular(0);
            activePlayer.playerInput.AppendAction<Vector2>(horizontalCameraInputAxisName, OnCameraInputChanged, verticalCameraInputAxisName);
        }
    }

    void OnCameraInputChanged(Vector2 input)
    {
        if (initialized)
        {
            freeLook.m_XAxis.m_InputAxisValue = input.x;
            freeLook.m_YAxis.m_InputAxisValue = input.y;
        }
    }
}
