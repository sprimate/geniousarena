﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AGeniousAudioEngine : AGeniousFighterEngine
{

    HashSet<FighterController> registeredControllers = new HashSet<FighterController>();
    public void RegisterController(FighterController controller)
    {
        registeredControllers.Add(controller);
    }

    public void UnregisterController(FighterController controller)
    {
        if (registeredControllers.Contains(controller))
        {
            registeredControllers.Remove(controller);
        }
    }
    public void PlayFighterAudio(FighterController controller, string audioKey)
    {
        
    }
}   