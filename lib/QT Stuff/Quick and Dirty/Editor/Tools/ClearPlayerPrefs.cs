﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClearPlayerPrefs : MonoBehaviour {

    [MenuItem("Tools/Clear Player Prefs")]
    public static void Clear()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Deleted all player prefs.");
    }
}
