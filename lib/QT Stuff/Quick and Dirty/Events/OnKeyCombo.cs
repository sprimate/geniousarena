﻿using UnityEngine;
using UnityEngine.Events;

namespace QuickAndDirty.Events
{
    public class OnKeyCombo : MonoBehaviour
    {
        public enum TimescaleReaction
        {
            DoesntMatter,
            TimescaleZero,
            TimescaleNonZero
        }

        public KeyCode[] keyCombo = new KeyCode[0];
        public UnityEvent action;
        public TimescaleReaction runIfTimescale = TimescaleReaction.DoesntMatter;

        public static object lockObject = null;

        private void Start() 
        {
            if (keyCombo.Length == 0) {
                this.enabled = false;
                Debug.LogError("Disabling KeyCombo " + this.name + " [No Keys]");
            }
        }

        void Update() 
        {
            if (!CompatibleTimescale()) return;

            bool combo = true;
            for (int i = 0; i < keyCombo.Length - 1; i++)
            {
                combo &= Input.GetKey(keyCombo[i]);
            }

            combo &= Input.GetKeyDown(keyCombo[keyCombo.Length - 1]);

            if (combo) {
                action.Invoke();
            }
        }

        bool CompatibleTimescale()  
        {
            if (lockObject != null) return false;

            switch (runIfTimescale)
            {
                case TimescaleReaction.TimescaleZero:
                    return Time.timeScale < float.Epsilon;
                case TimescaleReaction.TimescaleNonZero:
                    return Time.timeScale > 0f;
                default:
                    return true;
            }
        }

        /*
         * Acquire KeyCombo lock
         */
        public static bool Lock(object obj)
        {
            if (lockObject == null) {
                lockObject = obj;
                return true;
            }
            else {
                return false;
            }
        }

        /*
         * Free the prisoners!
         */
        public static void Unlock()
        {
            lockObject = null;
        }
    }
}
