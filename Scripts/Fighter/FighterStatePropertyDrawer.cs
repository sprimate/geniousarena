﻿// The property drawer class should be placed in an editor script, inside a folder called Editor.

// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
//using UnityEditor.Compilation;
//using UnityEditor.Compilation;

[CustomPropertyDrawer(typeof(FighterStateType))]
public class FighterStatePropertyDrawer : PropertyDrawer
{
    public Type[] types;
    public string[] typeNames;
    int selectedIndex = 0;
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var myObject = property?.serializedObject?.targetObject;
        Type currentType = ((SerializableType) fieldInfo.GetValue(myObject))?.type;
        if (types == null)
        {
            var t = AFighterState.GetAllFighterStateTypes();
            types = new Type[t.Count() + 1];
            typeNames = new string[t.Count() + 1];
            types[0] = null;
            typeNames[0] = "";
            int i = 1;
            foreach(var type in t.OrderBy(x => x.Name))
            {
                if (type == currentType)
                {
                    selectedIndex = i;
                }
                types[i] = type;//.Name;
                typeNames[i] = type.Name;
                i++;
            }
        }
      
        selectedIndex = EditorGUI.Popup(position, property.name, selectedIndex, typeNames);
        fieldInfo.SetValue(myObject, new FighterStateType(types[selectedIndex])); 
    }
}