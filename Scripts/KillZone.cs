using ProBuilder.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class KillZone : MonoSingleton<KillZone>
{
    [SerializeField] Material killzoneMaterial;
    Dictionary<FighterController, KeyValuePair<int, Collider>> triggeredFrameDict = new Dictionary<FighterController, KeyValuePair<int, Collider>>();

    protected virtual void Start()
    {
        GetComponent<Renderer>().material = killzoneMaterial;
    }

    private void OnTriggerStay(Collider other)
    {
        Hit(other);
    }

    public void OnCollisionStay(Collision collision)
    {
        Hit(collision.collider);
    }

    void Hit(Collider other)
    {
        var controlla = other.GetComponentInParent<FighterController>();
        if (controlla != null)
        {
            triggeredFrameDict[controlla] = new KeyValuePair<int, Collider>(GeniousSettings.fixedFrame, other);
        }
    }
    public void FixedUpdate()
    {
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            foreach(var pair in triggeredFrameDict)
            {
                if (pair.Value.Key != GeniousSettings.fixedFrame && pair.Value.Value != null && pair.Value.Value.gameObject.activeSelf)
                {
                    (pair.Key?.attackEngine as IKillable)?.Kill();
                }
            }
        });
    }

    public void SetSize(Vector3 size)
    {
        transform.localScale = size;
    }

    /*public void Grow(float size)
    {
        var pb = GetComponent<pb_Object>();
        foreach (pb_Face face in pb.faces)
        {
            pb.TranslateVertices(face.distinctIndices, pb_Math.Normal(pb.vertices) * size);
        }
    }*/
}