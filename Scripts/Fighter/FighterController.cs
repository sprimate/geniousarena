using Gamekit3D;
using Rewired;
using Smash_Forge;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//using UnityEngine.Experimental.Input;
using UnityEngine.Serialization;
using static Smash_Forge.ATKD;

public class MoveConversions
{
    public Dictionary<string, List<MoveConversions>> moveConversion;
}

[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(GeniousDamageable))]
[RequireComponent(typeof(Animator))]

public class FighterController : MonoBehaviour, IHitboxActivator, IFighterDataHolder
{
    const string fighterLayer = "Hero";
    public Animator m_Animator { get; protected set; } // Reference used to make decisions based on Ellen's current animation and to set parameters.
    public AGeniousLocomotionEngine locomotion { get; protected set; }
    public AGeniousAttackEngine attackEngine { get; protected set; }// {get {return GeniousSettings.instance.attackEngine;}}
    public AGeniousAudioEngine audioEngine { get; protected set; }
    public FighterData fighterData { get { return _fighterData; } protected set { _fighterData = value; } }
    [SerializeField] FighterData _fighterData;
    public FighterModel model { get; protected set; }
    CapsuleCollider personalSpace;
    public int ignoreRootMotionOnThisFrame { get; set; }
    [ReadOnly] public Transform landingDirectionTransform;
    public PlayerInput playerInput { get; protected set; }
    //[SerializeField] SmashFrameData frameData;
    [Range(0f, 1f)]
    [SerializeField]
    float inputDeadZone = 0.1f;
    public readonly int m_FacingUp = Animator.StringToHash("FacingUp");

    readonly int m_ExitLocomotion = Animator.StringToHash("ExitLocomotion");
    readonly int m_HashBlockInput = Animator.StringToHash("BlockInput");
    readonly int m_HashStateTime = Animator.StringToHash("StateTime");

    protected AnimatorStateInfo m_CurrentStateInfo;    // Information about the base layer of the animator cached.
    protected AnimatorStateInfo m_NextStateInfo;
    protected bool m_IsAnimatorTransitioning;
    protected AnimatorStateInfo m_PreviousCurrentStateInfo;    // Information about the base layer of the animator from last frame.
    protected AnimatorStateInfo m_PreviousNextStateInfo;
    protected bool m_PreviousIsAnimatorTransitioning;
    protected bool m_PreviouslyGrounded = true;    // Whether or not Ellen was standing on the ground last frame.
    protected bool m_ReadyToJump;                  // Whether or not the input state and Ellen are correct to allow jumping.
    //protected float m_DesiredForwardSpeed;         // How fast Ellen aims be going along the ground based on input.
    //public float m_ForwardSpeed {get; protected set;}                // How fast Ellen is currently going along the ground.
    //public float m_VerticalSpeed { get { return _vertSpeed; } set { _vertSpeed = value; } }// Debug.Log("Setting VertSpeed to " + m_VerticalSpeed); } }              // How fast Ellen is currently moving up or down.
    protected float _vertSpeed;
    //protected PlayerInputGameKit m_Input;                 // Reference used to determine how Ellen should move.
    public CharacterController m_CharCtrl { get; protected set; }      // Reference used to actually move Ellen.
    Animator[] animators;
    protected Material m_CurrentWalkingSurface;    // Reference used to make decisions about audio.
    //public Quaternion m_TargetRotation {get; protected set;}         // What rotation Ellen is aiming to have based on input.
    protected float m_AngleDiff;                   // Angle in degrees between Ellen's current rotation and her target rotation.
    protected Collider[] m_OverlapResult = new Collider[8];    // Used to cache colliders that are near Ellen.
    public bool m_InAttack { get; protected set; }                     // Whether Ellen is currently in the middle of a melee attack.
    protected virtual bool m_InCombo { get; set; }                      // Whether Ellen is currently in the middle of her melee combo.
    public ADamageable m_Damageable;//{ get; protected set; }             // Reference used to set invulnerablity and health based on respawning.
    protected Renderer[] m_Renderers;              // References used to make sure Renderers are reset properly. 
    //protected Checkpoint m_CurrentCheckpoint;      // Reference used to reset Ellen to the correct position on respawn.
    protected bool m_Respawning;                   // Whether Ellen is currently respawning.
    protected float m_IdleTimer;                   // Used to count up to Ellen considering a random idle.
    public bool invulnerable { get; set; }
    HashSet<InputAction> attackDetectedActions = new HashSet<InputAction>();
    //Normal state can not be named "Grounded" because another animator parameter uses it. 
    //None of these states can be other animtor parm names
    /* public enum FighterStateEnum { Normal, Tumbling, Reeling, Helpless, Stunned, Sleeping, Frozen, Buried, Floored, GettingUp, Guard, Dodging, Dead, JumpSquat, Landing }
    [SerializeField] FighterStateEnum _state;
    public FighterStateEnum state {
        get { return _state; }
        set
        {
            if (value == _state)
            {
                return;
            }
            GeniousSettings.Debug("Setting state to " + value);
            foreach (var s in smashPlayerStateAnimatorReferences)
            {
                m_Animator.SetBool(s.Value, value == s.Key);
            }
            if (onStateCallbacks.ContainsKey(state))
            {
                onStateCallbacks[state]?.Invoke(false);
            }

            _state = value;
            if (onStateCallbacks.ContainsKey(state))
            {
                onStateCallbacks[state]?.Invoke(true);
            }
        }
    }
    Dictionary<FighterStateEnum, Action<bool>> onStateCallbacks = new Dictionary<FighterStateEnum, Action<bool>>();

    public void RegisterStateCallback(FighterStateEnum callbackState, Action<bool> action)
    {
        if (onStateCallbacks.ContainsKey(callbackState))
        {
            onStateCallbacks[callbackState] += action;
        }
        else
        {
            onStateCallbacks[callbackState] = action;
        }
    }

    public Action<FighterStateEnum> OnState;
    */



    //This is used for debugging and editor inspecting
    //TODO - make this read only and have the ReadOnly attribute properly draw the property drawer
    [Tooltip("Adjusting this will probably do nothing. This would be read only, but it's not drawing the correct property drawer")]
    public FighterStateType fighterState_READONLY;
    AFighterState _fighterStateBackingField;
    public AFighterState fighterState {
        get
        {
            return _fighterStateBackingField;
        }

        set
        {
            _fighterStateBackingField = value;
            fighterState_READONLY = new FighterStateType(value.GetType());
        }
    }

    public bool m_IsGrounded = true;// { get; set; }

    void Reset()
    {
        ConnectVars();
    }

    public static FighterController CreateController(FighterDataInputPair fdPair)
    {
        FighterData fd= fdPair.fighterData;
        GameObject fighter = new GameObject(fd.displayName);
        var fc = fighter.AddComponent<FighterController>();
        fc.fighterData = fd;
        if (fdPair.input != null)
        {
            fdPair.input.transform.SetParent(GeniousSettings.instance.fightersParent);
            fc.transform.SetParent(fdPair.input.transform);
        }
        else
        {
            fc.transform.SetParent(GeniousSettings.instance.fightersParent);
        }

        if (fdPair.input == null)
        {
            //Probably a computer, but give it one to avoid nullrefs
            fdPair.input = GeniousInputManager.instance.AddComputerPlayerInput(fighter);
        }

        fc.transform.position = GeniousSettings.instance.GetSpawnLocation(fdPair.input.playerNumber);
        fc.SetupFighter();
        return fc;
    }

    void ConnectVars()
    {
        if (playerInput == null)
        {
            playerInput = GetComponentInParent<PlayerInput>();
        }

        if (playerInput == null)
        {
            //Probably a computer player if its on the actual object (6/14/19)
            playerInput = GetComponent<PlayerInput>();
        }

        // m_Input = GetComponent<PlayerInputGameKit>();
        if (m_Animator == null)
        {
            m_Animator = GetComponent<Animator>();
        }
        if (m_CharCtrl == null)
        {
            m_CharCtrl = GetComponent<CharacterController>();
        }

        if (hitbox == null)
        {
            hitbox = GetComponentInChildren<HitBox>();
        }

        if (m_Damageable == null)
        {
            m_Damageable = GetComponent<ADamageable>();
        }
    }

    int activatedFrame = -1;

    int frameId;
    public void ActivateHitbox(Frame frames, Move move)
    {
        lastMoveId = move.api_id;
        hitbox.size = new Vector3(hitbox.size.x, move.hitbox.rect.height, move.hitbox.rect.width);
        //Debug.Log(hitboxYOffset + " + (" + move.hitbox.rect.yMax + "*" + transform.localScale.y);
        hitbox.center = new Vector3(hitbox.center.x, hitboxYOffset + (move.hitbox.rect.yMin * transform.localScale.y), move.hitbox.rect.center.x);
        //hitbox.center = new Vector3(hitbox.center.x, (move.hitbox.rect.yMin * transform.localScale.y), move.hitbox.rect.center.x);

        foreach (var trail in weaponTrails)
        {
            trail.Emit = true;
        }

        SetHitboxActive(true);
        frameId = Time.frameCount;
        int localFrameId = frameId;
        GenericCoroutineManager.instance.RunInFixedUpdateFrames(frames.value + 1, () =>
        {
            if (frameId == localFrameId)
            {
                SetHitboxActive(false);
            }
        });
    }

    public void SetTrigger(int hash)
    {
        m_Animator.SetTrigger(hash);
        locomotion.onNextAnimatorFrame += () => { m_Animator.ResetTrigger(hash); };
    }

    public void SetHitboxActive(bool val)
    {
        if (m_InAttack != val)
        {
            if (val)
            {
                activatedFrame = GeniousSettings.fixedFrame;
            }
            else
            {
                if (Time.frameCount - activatedFrame == 0)
                {
                    return;
                }

                if (activatedFrame > -1)
                {
                    Frame f = (GeniousSettings.fixedFrame - activatedFrame);
                }
            }
        }

        m_InAttack = val;
        if (hitbox != null)
        {
            hitbox.SetActive(val);
        }

    }

    //TODO - put in locomotion (or attack. probably attack) engine
    public void UpdateLandingDirection()
    {
        var ogPosition = landingDirectionTransform.position;
        landingDirectionTransform.Translate(Vector3.right, Space.Self);
        m_Animator.SetBool(m_FacingUp, landingDirectionTransform.position.y >= ogPosition.y);
        landingDirectionTransform.position = ogPosition;
    }

    public void SetFighterState(FighterStateType type)
    {
        if (type == null || type.type == null)
        {
            Debug.LogError("Can't set FighterState to null type. Maybe a SetFighterStateFromAnimator instance doesn't have a state specified?");
            return;
        }

        if (fighterState != null)
        {
            fighterState.Exit();
        }
        fighterState = (AFighterState)Activator.CreateInstance(type.type);
        fighterState.Init(this);
    }

    public void SetFighterState<T>() where T : AFighterState
    {
        SetFighterState(new FighterStateType(typeof(T)));
    }

    string _currentMoveId;
    public string lastMoveId { get {
            return _currentMoveId;
        } set {
            _currentMoveId = value;
            attackEngine.OnMoveIdSet(_currentMoveId);
        }
    }
    public HitBox hitbox { get; set; }
    float hitboxYOffset;

    Transform pushAwayFrom;
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject != gameObject && other.gameObject.HasComponent<FighterController>())
        {
            pushAwayFrom = other.transform;
        }
    }

    T GetWantedHitboxValue<T>(List<T> list, int hitIndex)
    {
        var index = list.Count - hitIndex - 1;
        if (index < 0)
        {
            index = 0;
        }

        return list[index];
    }

    public void MoveComplete(string moveId)
    {
        if (moveId == lastMoveId)
        {
            foreach (var trail in weaponTrails)
            {
                trail.Emit = false;
            }
        }

        attackEngine.MoveCompleted(fighterData.GetMove(moveId));
    }

    /* public void RegisterHit(OnHit onHit)
     {

     }
     */
    HitPoint closestHitPoint;
    public void RegisterHit(Collider collider, List<HitPoint> orderedHitPoints, Vector3 contactPoint)
    {
        GeniousDamageable d = collider.gameObject.GetComponentInParent<GeniousDamageable>();
        if (d != null && m_Damageable != d)
        {
            float distance = float.MaxValue;
            foreach (var hitPoint in orderedHitPoints)
            {
                hitPoint.ResetPosition();
                if (Vector3.Distance(hitPoint.transform.position, contactPoint) < distance)
                {
                    closestHitPoint = hitPoint;
                }
            }

            closestHitPoint.position = contactPoint;
            Attack(d, closestHitPoint);
        }
        else if (d == null && collider.gameObject.GetComponent<OnHit>() != null)
        {
            Move move = fighterData.GetMove(lastMoveId);
            collider.gameObject.GetComponent<OnHit>().ApplyDamage(move.base_damage, this, (closestHitPoint.transform.position - transform.position).normalized, transform.position, move.@throw, false);
            //meleeWeapon.CheckDamage(other, other.transform.position);
        }
        /*else if (meleeWeapon != null)
        {
            meleeWeapon.CheckDamage(collision.collider, collision.contacts[0].point);
        }*/
    }

    public void Attack(GeniousDamageable d, HitPoint hitPoint)
    {
        Move move = fighterData.GetMove(lastMoveId);
        if (move != null)
        {
            attackEngine.RegisterAttack(move, this, d.physicsObject as FighterController, hitPoint);
        }
        else
        {
            Debug.LogError("Can't find the conversion? " + lastMoveId);
        }
    }

    public Move GetCurrentMove(int index = -1)
    {
        return fighterData.GetMove(lastMoveId, index);
    }

    public Dictionary<InputAction, float> activeButtons { get; protected set;}= new Dictionary<InputAction, float>();
    Dictionary<InputAction, string> inputTimeHashKeys = new Dictionary<InputAction, string>();

    public bool InputDetected
    {
        get
        {
            bool ret = false;
            if (locomotion != null)
            {
                ret |= locomotion.EngineInputDetected;
            }
            if (attackEngine != null)
            {
                ret |= attackEngine.EngineInputDetected;
            }
            //TODO - The activeButtons check is probably enough
            return ret || activeButtons.Count > 0;
        }
    }


    /*public bool canRotate {get; protected set;} = true;
   public void SetRotationAllowed(bool val)
   {
       canRotate = val;
   }

  public void SetMovementAllowed(bool val, bool updateParentTransformHere = false)
   {
       canMove = val;
   }*/

    protected void OnAnimatorMove()
    {
        locomotion.OnAnimatorMove();
    }

    Action<FighterAnimatorScript, FighterAnimatorScriptState> OnState;
    HashSet<Action<FighterAnimatorScript, FighterAnimatorScriptState>> toUnregister = new HashSet<Action<FighterAnimatorScript, FighterAnimatorScriptState>>();
    Dictionary<Type, Action<FighterAnimatorScript>> stateMoveCallbacks = new Dictionary<Type, Action<FighterAnimatorScript>>();
    Dictionary<Type, Action<FighterAnimatorScript>> stateUpdateCallbacks = new Dictionary<Type, Action<FighterAnimatorScript>>();
    Dictionary<Type, Action<FighterAnimatorScript>> stateEnterCallbacks = new Dictionary<Type, Action<FighterAnimatorScript>>();
    Dictionary<Type, Action<FighterAnimatorScript>> stateExitCallbacks = new Dictionary<Type, Action<FighterAnimatorScript>>();
    public enum FighterAnimatorScriptState { Enter, Exit, Update, Move };
    public void RegisterStateCallbacks<T>(Action<FighterAnimatorScript, FighterAnimatorScriptState> callback) where T : FighterAnimatorScript
    {
        Action<FighterAnimatorScript, FighterAnimatorScriptState> thisCallback = (x, y) => { };
        thisCallback += (x, y) =>
        {
            if (toUnregister.Contains(callback))
            {
                OnState -= thisCallback;
                toUnregister.Remove(callback);
            }
            else if (x != null && x.GetType() == typeof(T))
            {
                callback?.Invoke(x, y);
            }
        };

        OnState += thisCallback;
        /*      RegisterStateEnterCallback<T>((x) => {
                    callback?.Invoke(x, FighterAnimatorScriptState.Enter);
                });
                RegisterStateExitCallback<T>((x) => {
                    callback?.Invoke(x, FighterAnimatorScriptState.Exit);
                });
                RegisterStateUpdateCallback<T>((x) => {
                    callback?.Invoke(x, FighterAnimatorScriptState.Update);
                });
                RegisterStateMoveCallback<T>((x) => {
                    callback?.Invoke(x, FighterAnimatorScriptState.Move);
                });
                */
    }

    public void UnregisterStateCallbacks<T>(Action<FighterAnimatorScript, FighterAnimatorScriptState> callback) where T : FighterAnimatorScript
    {
        toUnregister.Add(callback);
    }

    public void RegisterStateEnterCallback<T>(Action<FighterAnimatorScript> callback) where T : FighterAnimatorScript
    {
        RegisterStateCallback<T>(ref stateEnterCallbacks, callback);
    }
    public void RegisterStateExitCallback<T>(Action<FighterAnimatorScript> callback) where T : FighterAnimatorScript
    {
        RegisterStateCallback<T>(ref stateExitCallbacks, callback);
    }
    public void RegisterStateMoveCallback<T>(Action<FighterAnimatorScript> callback) where T : FighterAnimatorScript
    {
        RegisterStateCallback<T>(ref stateMoveCallbacks, callback);
    }

    void RegisterStateUpdateCallback<T>(Action<FighterAnimatorScript> callback) where T : FighterAnimatorScript
    {
        RegisterStateCallback<T>(ref stateUpdateCallbacks, callback);
    }

    void RegisterStateCallback<T>(ref Dictionary<Type, Action<FighterAnimatorScript>> dict, Action<FighterAnimatorScript> callback) where T : FighterAnimatorScript
    {
        Type typeOf = typeof(T);
        if (dict.ContainsKey(typeOf))
        {
            dict[typeOf] += callback;
        }
        else
        {
            dict[typeOf] = callback;
        }
    }
    
    public void OnStateMove(FighterAnimatorScript animatorStateBehaviour)
    {
        OnStateCallbacks(stateMoveCallbacks, animatorStateBehaviour, FighterAnimatorScriptState.Move);
    }

    public void OnStateUpdate(FighterAnimatorScript animatorStateBehaviour)
    {
        OnStateCallbacks(stateUpdateCallbacks, animatorStateBehaviour, FighterAnimatorScriptState.Update);
    }

    public void OnStateExit(FighterAnimatorScript animatorStateBehaviour)
    {
        OnStateCallbacks(stateExitCallbacks, animatorStateBehaviour, FighterAnimatorScriptState.Exit);
    }

    public void OnStateEnter(FighterAnimatorScript animatorStateBehaviour)
    {
        OnStateCallbacks(stateEnterCallbacks, animatorStateBehaviour, FighterAnimatorScriptState.Enter);
    }

    void OnStateCallbacks(Dictionary<Type, Action<FighterAnimatorScript>> dict, FighterAnimatorScript behaviour, FighterAnimatorScriptState state)
    {
        if (dict.ContainsKey(behaviour.GetType()))
        {
            stateMoveCallbacks[behaviour.GetType()].Invoke(behaviour);
        }
        else
        {
            //Debug.Log("WARNING: No callback registered for type " + behaviour.GetType());
        }

        OnState?.Invoke(behaviour, state);
    }

    bool shortHop = false;
    int? jumpInputFrame;
    int jumpFrame;
    bool squatting = false;

    public void TriggerJump(bool val)
    {
        locomotion.TriggerJump(val);
    }

    public bool inKnockback;

    public void OnEnable()
    {
        StartCoroutine(otherJawnFollow());
    }

    IEnumerator otherJawnFollow()
    {
        if (otherChar != null)
        {
            while (gameObject.activeSelf)
            {
                otherChar.transform.position = transform.position;
                otherChar.transform.rotation = transform.rotation;
                yield return null;
            }
        }
    }

    public FighterController otherChar;
    bool characterIs = true;

    MeleeWeaponTrail[] weaponTrails;
    Dictionary<ushort, HitboxEntry> hitboxEntries = new Dictionary<ushort, HitboxEntry>();
    Vector3 maxShieldScale;
    bool alreadySetUp;
    void SetupFighter()
    {
        if (alreadySetUp)
        {
            return;
        }

        InitializeEngines();
        model = Instantiate(fighterData.model, transform.position, transform.rotation, transform);
        model.transform.localEulerAngles = fighterData.model.transform.localEulerAngles;
        model.name = "Model";
        transform.name += " [" + fighterData.name + "]";
        hitboxYOffset = model.hitboxOffsetOrCharCenter;
        //go.transform.DetachChildren();
        for (var i = 0; i < model.transform.childCount; i++)
        {
            var t = model.transform.GetChild(i);
            GenericCoroutineManager.instance.RunAfterFrame(() =>
            {
               // t.SetParent(transform, true);
            });
        }
        m_CharCtrl = GetComponent<CharacterController>();
        CapsuleCollider cc = GetComponent<CapsuleCollider>();
        cc.isTrigger = true;
        var charControllerReference = model.GetComponent<CharacterController>();
        m_CharCtrl.center = cc.center = Quaternion.Euler(fighterData.model.rotationAdjustment) * (charControllerReference.center * charControllerReference.transform.localScale.x);
        m_CharCtrl.height = charControllerReference.height * charControllerReference.transform.localScale.x;
        cc.height = m_CharCtrl.height + GeniousSettings.instance.pushColliderHeightOffset;
        m_CharCtrl.radius = charControllerReference.radius * charControllerReference.transform.localScale.x;
        m_CharCtrl.stepOffset = m_CharCtrl.height / 4f;
        Destroy(charControllerReference);

        cc.radius = m_CharCtrl.radius + GeniousSettings.instance.pushColliderRadiusOffset;
       
        if (personalSpace == null)
        {
            personalSpace = GetComponent<CapsuleCollider>();
        }

        SetupAnimator();

        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            landingDirectionTransform = transform.FindChildRecursive(fighterData.boneRoot);
            /*var input = GetComponent<FighterPlayerInput>();
            if (input != null && input.controls == null)
            {
                input.controls = GeniousSettings.instance.fighterControls;
            }*/
        });

        fighterData.Init();
        foreach (var entry in fighterData.hitboxData.entries)
        {
            hitboxEntries[entry.animationId] = entry;
        }

        weaponTrails = GetComponentsInChildren<MeleeWeaponTrail>();
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            gameObject.SetLayerRecursive(fighterLayer);
        });

        if (fighterState == null)
        {
            SetFighterState<DefaultFighterState>();
        }

        alreadySetUp = true;
    }

    void SetupAnimator()
    {
        if (m_Animator == null)
        {
            m_Animator = GetComponent<Animator>();// go.AddComponent<Animator>();
        }

        m_Animator.avatar = fighterData.avatar;
        m_Animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
        m_Animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
        m_Animator.applyRootMotion = true;
        m_Animator.runtimeAnimatorController = fighterData.runtimeAnimatorController;
    }

    bool enginesInitialized;
    void InitializeEngines()
    {
        if (enginesInitialized)
            return;

        locomotion = GeniousSettings.instance.locomotionEngine.CreateInstance<AGeniousLocomotionEngine>(this);
        attackEngine = GeniousSettings.instance.attackEngine.CreateInstance<AGeniousAttackEngine>(this);
        audioEngine = GeniousSettings.instance.audioEngine.CreateInstance<AGeniousAudioEngine>(this);
        allEngines = new List<AGeniousFighterEngine>(new AGeniousFighterEngine[] { locomotion, attackEngine, audioEngine });
        enginesInitialized = true;
    }

    List<AGeniousFighterEngine> allEngines;
    Queue<UnparsedMove> moveQueue = new Queue<UnparsedMove>();
    ATKD atkd;
    protected IEnumerator Start()
    {
       // InitializeEngines();
        InitializeRewiredInputManager();
        SetupFighter();
//        yield return null;
        yield return new WaitForEndOfFrame(); //Wait for Fighter model stuff to be instantiated

        SetupAnimator();        

        Time.timeScale = 0f;
        if (GeniousSocketManager.instance.IsConnected)
        {
            Time.timeScale = 1f;
        }
        else
        {
            GeniousSocketManager.instance.onConnected += () =>
            {
                Time.timeScale = 1f;
            };
        }

        ConnectVars();

        if (hitbox == null)
        {
            Debug.LogError("Character Prefab for " + fighterData.characterName +" needs a hitbox. Probably on the TransN object.", fighterData.model);
        }

        m_CharCtrl.stepOffset *= transform.lossyScale.y;

        SetHitboxActive(false);
        GenericCoroutineManager.instance.RunInFrames(4, () =>
         {
             IEnumerable<InputAction> notAnAnimatorActionList = new List<InputAction>();
             foreach (var v in GeniousInputManager.NOT_ANIMATOR_ACTION_CATEGORY_NAME)
             {
                 notAnAnimatorActionList = notAnAnimatorActionList.Union(ReInput.mapping.ActionsInCategory(v));
             }
            var actions = ReInput.mapping.Actions.Except(notAnAnimatorActionList);
            foreach (var p in m_Animator.parameters)
            {
                parameterHashes[p.nameHash] = p;
            }

            foreach(var action in actions)
            {
                if (action.type == InputActionType.Button)
                {
                    SetupButton(action);
                }
                else if (action.type == InputActionType.Axis)
                {
                    SetupAxis<float>(action);
                }
            }

           RegisterInputCallbacks();
           /*
            playerInput.rewiredPlayer?.ClearInputEventDelegates();
            playerInput.rewiredPlayer?.AddInputEventDelegate((x) =>
            {
                Debug.Log("Hmm: " + x.actionName);
            }, UpdateLoopType.FixedUpdate,InputActionEventType.ButtonPressed, "Jab");*/
        });
    }

    Dictionary<int, AnimatorControllerParameter> parameterHashes = new Dictionary<int, AnimatorControllerParameter>();

    public void InitializeRewiredInputManager()
    {
        Type type = null;
        foreach(var engine in allEngines)
        {
            if (engine.RewiredInputTypeDependency != null)
            {
                if (type == null)
                {
                    type = engine.RewiredInputTypeDependency;
                }
                else if (type != engine.RewiredInputTypeDependency)
                {
                    Debug.LogError("Unable to create Rewired Input. There is a dependency incompatability between engines");
                    return;
                }
            }
        }

        GeniousInputManager.instance.SetRewiredInputManager(type);
        /*GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            GeniousInputManager.instance.Init();
        });*/
    }

    void RegisterInputCallbacks()
    {
        locomotion.RegisterInputCallbacks(playerInput);
        attackEngine.RegisterInputCallbacks(playerInput);
        audioEngine.RegisterInputCallbacks(playerInput);
        /*smashInput.onButton[smashInput.actions.CameraDown] += CameraDown;
        smashInput.onButton[smashInput.actions.CameraUp] += CameraUp;
        smashInput.onButton[smashInput.actions.CameraLeft] += CameraSide;
        smashInput.onButton[smashInput.actions.CameraRight] += CameraSide;*/
    }  

    void SetFloatAnimatorParam(string paramName, float val)
    {
        int? hash = GetAnimatorHash(paramName, AnimatorControllerParameterType.Float);
        if (hash.HasValue)
        {
            m_Animator.SetFloat(hash.Value, val);
        }
    }

    void SetBoolAnimatorParam(string paramName, bool val)
    {
        int? hash = GetAnimatorHash(paramName, AnimatorControllerParameterType.Bool);
        if (hash.HasValue)
        {
            m_Animator.SetBool(hash.Value, val);
        }
    }

    void SetTriggerAnimatorParam(string paramName)
    {
        int? hash = GetAnimatorHash(paramName, AnimatorControllerParameterType.Trigger);
        if (hash.HasValue)
        {
            SetTrigger(hash.Value);
//            m_Animator.SetTrigger(hash.Value);
        }
    }

    void ResetTriggerAnimatorParam(string paramName)
    {
        int? hash = GetAnimatorHash(paramName, AnimatorControllerParameterType.Trigger);
        if (hash.HasValue)
        {
            m_Animator.ResetTrigger(hash.Value);
        }
    }

    int? GetAnimatorHash(string paramName, AnimatorControllerParameterType paramType)
    {
        int hash = Animator.StringToHash(paramName);
        if (!parameterHashes.ContainsKey(hash))
        {
            GeniousSettings.instance.LogOnce(paramName + " doesn't exist in [" + m_Animator + "]", hash);
            return null;
        }
        else if (parameterHashes[hash].type != paramType)
        {
            GeniousSettings.instance.LogOnce(paramName + " is not of type [" + paramType + "]. It is of type " + parameterHashes[hash].type, hash);
            return null;
        }

        return hash;
    }

    void SetupAxis<T>(InputAction action)
    {
        string actionNameNoSpaces = action.name.Replace(" ", "");
        string animatorHash = ("InputAxis_" + actionNameNoSpaces);
        string startAnimatorHash = ("Input_" + actionNameNoSpaces + "Started");
        string animatorAxisHash = ("InputAxis_" + actionNameNoSpaces);
        string animatorXAxisHash = ("InputAxis_" + actionNameNoSpaces + "XAxis");
        string animatorYAxisHash = ("InputAxis_" + actionNameNoSpaces + "YAxis");

        string endAnimatorHash = ("Input_" + actionNameNoSpaces + "Ended");
        inputTimeHashKeys[action] = ("Input_" + actionNameNoSpaces + "TimePressed");
        playerInput.AppendAction<T>(action, (val) =>
        {
            // Debug.Log("Val: " + val + " for " + action.name);
            if (typeof(T) == typeof(float))
            {
                var value = (float)Convert.ChangeType(val, typeof(float));
                if (value != 0)
                {
                    SetTriggerAnimatorParam(startAnimatorHash);
                }
                else
                {
                    SetTriggerAnimatorParam(endAnimatorHash);
                    SetFloatAnimatorParam(inputTimeHashKeys[action], 0f);
                }


                SetFloatAnimatorParam(animatorAxisHash, value);
                //SetBoolAnimatorParam(animatorHash, value != 0);
            }
            else
            {
                var value = (Vector2)Convert.ChangeType(val, typeof(Vector2));
                if (value != Vector2.zero)
                {
                    SetTriggerAnimatorParam(startAnimatorHash);
                }
                else
                {
                    SetTriggerAnimatorParam(endAnimatorHash);
                    SetFloatAnimatorParam(inputTimeHashKeys[action], 0f);
                }

                SetFloatAnimatorParam(animatorAxisHash, value.magnitude);
                SetFloatAnimatorParam(animatorXAxisHash, value.x);
                SetFloatAnimatorParam(animatorYAxisHash, value.y);
                SetBoolAnimatorParam(animatorHash, value != Vector2.zero);
            }
        });
    }

    public int buttonInputsDetectedThisFrame {get; protected set;}
    void SetupButton(InputAction action)
    {
        string actionNameNoSpaces = action.name.Replace(" ", "");
        string animatorHash = ("InputButton_" + actionNameNoSpaces);
        string startAnimatorHash = ("Input_" + actionNameNoSpaces + "Started");
        string endAnimatorHash = ("Input_" + actionNameNoSpaces + "Ended");
        inputTimeHashKeys[action] = ("Input_" + actionNameNoSpaces + "TimePressed");

        playerInput.AppendAction<bool>(action, (val) =>
        {
            if (val)
            {
                SetTriggerAnimatorParam(startAnimatorHash);
                buttonInputsDetectedThisFrame++;
                locomotion.onNextAnimatorFrame += () => {
                    ResetTriggerAnimatorParam(startAnimatorHash);
                };

                activeButtons[action] = Time.time;
            }
            else
            {
                SetTriggerAnimatorParam(endAnimatorHash);

                // m_Animator.SetTrigger(releasedHash);
                activeButtons.Remove(action);
                SetFloatAnimatorParam(inputTimeHashKeys[action], 0f);
            }
            
            SetBoolAnimatorParam(animatorHash, val);
        });
    }

    void CacheAnimatorState()
    {
        m_PreviousCurrentStateInfo = m_CurrentStateInfo;
        m_PreviousNextStateInfo = m_NextStateInfo;
        m_PreviousIsAnimatorTransitioning = m_IsAnimatorTransitioning;

        m_CurrentStateInfo = m_Animator.GetCurrentAnimatorStateInfo(0);
        m_NextStateInfo = m_Animator.GetNextAnimatorStateInfo(0);
        m_IsAnimatorTransitioning = m_Animator.IsInTransition(0);
    }

    protected virtual void UpdateInputBlocking()
    {
        bool inputBlocked = m_CurrentStateInfo.tagHash == m_HashBlockInput && !m_IsAnimatorTransitioning;
        inputBlocked |= m_NextStateInfo.tagHash == m_HashBlockInput;

      /*  if (m_Input != null)
        m_Input.playerControllerInputBlocked = inputBlocked;
        */
    }

    void BasicFixedUpdate()
    {
        CacheAnimatorState();
        UpdateInputBlocking();
        //EquipMeleeWeapon(IsWeaponEquiped());

        m_Animator.SetFloat(m_HashStateTime, Mathf.Repeat(m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime, 1f));
        //        m_Animator.ResetTrigger(m_HashMeleeAttack);

        GenericCoroutineManager.instance.ExecuteBeforeEndOfFrame(() =>
        {
            locomotion.SetTargetRotation();
            locomotion.CalculateMovement();
            /*if (IsMoveInput)
            {
                if (IsOrientationUpdated())
                    UpdateOrientation();
            }*/

            m_PreviouslyGrounded = m_IsGrounded;
        });
    }

    public bool shouldRunFixedUpdate {get; set;} = true;
    protected void FixedUpdate()
    {
        if (!shouldRunFixedUpdate)//state == FighterStateEnum.Dead)
        {
            return;
        }
        HandlePushAway();
        buttonInputsDetectedThisFrame = 0;
        BasicFixedUpdate();
        UpdateInputAnimatorValues();

       // m_Animator.SetBool(m_ExitLocomotion, attackDetected || !m_IsGrounded || Mathf.Abs(inputMagnitude) < inputDeadZone);
        UpdatePositionFromRootMotion();
        /* switch (state)
        {
            case FighterStateEnum.Normal:
                UpdatePositionFromMoveInput();
                break;
            case FighterStateEnum.Reeling:
                HandleHitStun();
                break;
            case FighterStateEnum.Tumbling:
                HandleHitStun();
                break;
            case FighterStateEnum.Buried:
                break;
            case FighterStateEnum.Frozen:
                break;
            case FighterStateEnum.Helpless:
                break;
            case FighterStateEnum.GettingUp:
                break;
            case FighterStateEnum.Stunned:
                break;
            case FighterStateEnum.Floored:
                break;
            case FighterStateEnum.Guard:
                HandleGuard(true);
                break;
            case FighterStateEnum.Dodging:
                break;
            default:
                Debug.LogError("This state [" + state + "] isn't handled yet");
                break;
        }*/

        fighterState.AfterFighterFixedUpdate();
    }

    void HandlePushAway()
    {
        if (pushAwayFrom != null)
        {
            Vector3 direction = transform.position - pushAwayFrom.position;
            direction.y = 0;
            Vector3 translation = (direction).normalized * GeniousSettings.instance.pushAwaySpeed * Time.fixedDeltaTime;
            locomotion.MoveCharacter(translation);
        }
        pushAwayFrom = null;
    }

/* 
    protected void MoveCharacterFromAnimator(Vector3 movement)
    {
        base.MoveCharacter(movement); //Not very polymorphic
    }
    */

    protected bool IsOrientationUpdated()
    {
        return true;
    }
/* 
    protected override void UpdateHash()
    {
        //Ignore this. This empty function is neccessary for proper function
    }
*/
    int currentStateHash;
    public void SetCurrentStateHash(int hash)
    {
        currentStateHash = hash;
    }

    void UpdateInputAnimatorValues()
    {
        locomotion.UpdateInputAnimatorValues(m_Animator);
        foreach (var pair in activeButtons)
        {
            m_Animator.SetFloat(inputTimeHashKeys[pair.Key], Time.time - pair.Value);
        }
    }

    //I don't understand how root motion is supposed to work so this is my workaround
    protected bool UpdatePositionFromRootMotion()
    {

        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            if (ignoreRootMotionOnThisFrame == GeniousSettings.fixedFrame)
            {
                return;
            }
            
            if (animationMovementOffset != Vector3.zero)
            {
               // var translation = Quaternion.Euler(-90, 0, 0) * (animationMovementOffset - lastOffset) * transform.localScale.x;
                var translation = (animationMovementOffset - lastOffset) * transform.localScale.x;

                Vector3 newTranslation = transform.TransformDirection(translation);

                if (translation.y != 0)
                {
                     //SmashSettings.Log("Translation: " + translation.y + "(" + animationMovementOffset + ")");
                }
                m_CharCtrl.Move(newTranslation);
                lastOffset = animationMovementOffset;
            }
        });

        return Vector3.zero != animationMovementOffset;
    }

    Vector3 lastOffset;
    public Vector3 animationMovementOffset {
        get {
            return ignoreRootMotionOnThisFrame == GeniousSettings.fixedFrame ? Vector3.zero : model.scaledAnimationMovementOffset;
        }
        set {
            model.animationMovementOffset = value; }
    }

    public bool resetPosition;// direction;
    public void OnMovingAnimationStarted()
    {
        ResetLastOffset();
    }

    public void OnMovingAnimationEnded()
    {
        ResetLastOffset();
    }

    void ResetLastOffset()
    {
        //Debug.Log("ResetLastOffset!");
        lastOffset = Vector3.zero;
    }

/*    protected void UpdateOrientation()
    {
        //Needs to be empty so orientation is updated in our local SetTargetRotation even if no movement calculated
//        base.UpdateOrientation();
    }*/
}