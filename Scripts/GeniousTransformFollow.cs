using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousTransformFollow : MonoBehaviour
{
    public bool rotation;
    public bool position;
    public Transform target;
    public Transform target2;

    private void LateUpdate()
    {
        if (target == null)
        {
            return;
        }
        if (position)
            transform.position = target.position;
        if (rotation)
            transform.rotation = target.rotation;
    }

    public void FollowOtherTarget()
    {
        var temp = target;
        target = target2;
        target2 = temp;
    }
} 
