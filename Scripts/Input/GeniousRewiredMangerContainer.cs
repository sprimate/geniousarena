using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputManager))]
public class GeniousRewiredMangerContainer : MonoBehaviour// where T : RewiredInputActions
{
    InputManager _rewiredInputManager;
    public InputManager rewiredInputManager { get {
            if (_rewiredInputManager == null)
            {
                _rewiredInputManager = GetComponent<InputManager>();
            }
            return _rewiredInputManager;
        }
    }

    public RewiredInputActions actions;
}