using UnityEngine;

namespace QuickAndDirty.Gestures {
    public class StartGesture : GestureRecognizer
    {
        bool[] started;

        public float dragHoldTime;
        //public Vector2 minDelta;


        public StartGesture()
            : base()
        {
            //dragHoldTime = QADGestures.instance.dragHoldTime;
            //minDelta = QADGestures.instance.minDragDelta;
            started = new bool[QADGestures.instance.maxFingers + 1];
        }

        public override bool Recognize(Gesture gesture)
        {
            if (!started[gesture.fingerID])
            {
                //May not be desirable, phase start may be better
                if (gesture.phase != TouchPhase.Ended)
                {
                    //Set finger started to true, and set up a delegate to return false
                    started[gesture.fingerID] = true;

                    GestureEvent foo = null;
                    foo = delegate(Gesture g)
                    {
                        if (g.fingerID == gesture.fingerID)
                        {
                            started[g.fingerID] = false;
                            QADGestures.OnGestureEnd -= foo;
                        }
                    };

                    QADGestures.OnGestureEnd += foo;

                    return true;
                }
            }

            return false;

        }
    }
}