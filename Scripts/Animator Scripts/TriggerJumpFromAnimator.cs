using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerJumpFromAnimator : FighterAnimatorScript
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        controlla.TriggerJump(true);
    }
}
