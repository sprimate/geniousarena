using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterAnimatorAction : FighterAnimatorScript
{
    IHitboxActivator[] hitboxActivators;
    public string moveId;
    public string moveName;
    public FighterData FighterData;
    HitboxActiveFrames hitboxRange;
    bool canHit;
    bool notYetActivated = true;
    List<Move> moves;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        notYetActivated = true;
        hitboxActivators = animator.GetComponents<IHitboxActivator>();

        base.OnStateEnter(animator, stateInfo, layerIndex);
        moves = fighterData?.GetMoves(moveId);
        controlla?.SetCurrentStateHash(stateInfo.fullPathHash);

        SetMoveActions();

        canHit = true;
        controlla?.attackEngine.SetOnHitCallback(() =>
        {
            canHit = false;
        });

        //var actualHitbox


       hitboxRange = fighterData.GetMove(moveId)?.hitboxActive?[0];// unparsedMove.HitboxActive;
                                                                   // var openParenthesisIndex = unparsedMove.HitboxActive.IndexOf("(");

        /*
        foreach(var pair in controlla?.GetMove(moveId).specialHitboxes)
        {
            string attribute = pair.Key;
            switch (attribute)
            {
                case "Super Armor":
                    // Debug.Log("Should apply Super Armor on " + unparsedMove.Name);
                    break;
                case "Intangible":
                    // Debug.Log("Should apply Intangible");
                    break;
                case "Rehit rate":
                    break;
                case "Rebound":
                    break;
                default:
                    Debug.LogError("Don't know how to handle this attack attribute [" + attribute + "]!");
                    break;
            }
        }
        */
        OnStateEnter();
    }

    protected virtual void OnStateEnter()
    {

    }

    protected virtual void SetMoveActions()
    {
      //  controlla?.SetAction(actionData as ActionData);
        foreach (var hitboxActivator in hitboxActivators)
        {
            //hitboxActivator.SetMove(unparsedMove);
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        if (!canHit || hitboxRange == null)
        {
            return;
        }

       // if (actionData is HitActionFrameData)
        {
            var time = stateInfo.normalizedTime;
            var totalFrames = fighterData.GetMove(moveId).faf - 1;
            if (totalFrames <= 0)
            {
                totalFrames = Mathf.RoundToInt(stateInfo.length / Time.fixedDeltaTime);//Maybe need to do by speed too?
            }
            bool hitboxesActive = time >= (hitboxRange.start / (float)totalFrames);// && time <= ((float)hitboxRange.maxFrame.numFrames / data.totalFrames.numFrames);
           // if (hitboxesActive)
           // controlla.SetHitboxActive(hitboxesActive);
            if (notYetActivated && hitboxesActive)
            {
                foreach (var hitboxActivator in hitboxActivators)
                {
                    hitboxActivator.ActivateHitbox(Mathf.RoundToInt(hitboxRange.Range()), moves[0]);
                }
                notYetActivated = false;
            }
        }
        OnStateUpdate();
    }

    protected virtual void OnStateUpdate()
    {
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        foreach (var hitboxActivator in hitboxActivators)
        {
            hitboxActivator.SetHitboxActive(false);
        }
        controlla?.MoveComplete(moveId);
        OnStateExit();
    }

    protected virtual void OnStateExit()
    {
    }
}