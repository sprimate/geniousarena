using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBoolParameterFromState : SetParameterFromState
{
    public bool parameterValue;
    protected override void Set()
    {
        a.SetBool(paramHash.Value, parameterValue);
    }
}
