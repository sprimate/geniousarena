using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousLevel : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public List<Transform> respawnPoints;
    public bool containsKillzone;
    public Vector3 killzoneSize;
    public Vector3 killzoneOffset;

    public void Awake()
    {
        if (containsKillzone)
        {
            KillZone.instance.transform.SetParent(transform);
            KillZone.instance.transform.localRotation = Quaternion.identity;
            KillZone.instance.transform.localPosition = killzoneOffset;
            KillZone.instance.SetSize(killzoneSize);
            KillZone.instance.gameObject.SetActive(true);
        }
        else if (KillZone.hasInstance)
        {
            KillZone.instance.gameObject.SetActive(false);
        }
    }
}