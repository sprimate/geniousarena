﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGeniousAttackEngineUI : MonoBehaviour
{
    public abstract void RegisterPlayer(FighterController fc);
}
