using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;

public abstract class AFighterState 
{
    #if UNITY_EDITOR
    public static IEnumerable<Type> GetAllFighterStateTypes()
    {
        IEnumerable<Type> t = new Type[0];
        foreach(var a in AppDomain.CurrentDomain.GetAssemblies())
        {
        // s = s.Union(a.allReferences).ToArray();
            t = t.Union(a.GetTypes().Where(type => type.IsSubclassOf(typeof(AFighterState))));
        }

        return t;
    }
    #endif
    static Dictionary<Type, int> smashPlayerStateAnimatorReferences = new Dictionary<Type, int>();

    protected virtual InputActionCallback[] inputActionsToWatch { get {return new InputActionCallback[0];}}
    protected FighterController fc;
    public virtual void AfterFighterFixedUpdate(){}
    HashSet<Action> OnExitHashset = new HashSet<Action>();
    
    public void Init(FighterController fighterController){
        fc = fighterController;
        foreach(var i in inputActionsToWatch)
        {
            OnExitHashset.Add(fc.playerInput.AppendAction(i.inputAction, i.callback, i.type, i.verticalInputActionForVector2));
        }

        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            SetAnimatorState(true);
        });

        OnInit();
    }

    void SetAnimatorState(bool val)
    {
        if (!smashPlayerStateAnimatorReferences.ContainsKey(GetType()))
        {
            smashPlayerStateAnimatorReferences[GetType()] = Animator.StringToHash("State_" + GetType().Name.ToString()); 
        }

        fc.m_Animator.SetBool(smashPlayerStateAnimatorReferences[GetType()], val);
    }

    protected virtual void OnInit(){}
    
    public void Exit(){
        foreach(var a in OnExitHashset)
        {
            a?.Invoke();
        }

        OnExitHashset.Clear();
        SetAnimatorState(false);
        OnExit();
    }

    protected virtual void OnExit(){}

    public virtual void OnHit(){}

    protected class InputActionCallback
    {
        public InputAction inputAction;
        public Action<object> callback;
        public Type type;

        public InputAction verticalInputActionForVector2;

        public static InputActionCallback New<T>(InputAction _inputAction, Action<T> _callback, InputAction _verticalInputActionForVector2 = null)
        {
            return new InputActionCallback(_inputAction, (x) => _callback?.Invoke((T) x), typeof(T), _verticalInputActionForVector2);
        }

        protected InputActionCallback(InputAction _inputAction, Action<object> _callback, Type _type, InputAction _verticalInputActionForVector2) : this(_inputAction, _callback, _type)
        {
            verticalInputActionForVector2 = _verticalInputActionForVector2;
        }
        protected InputActionCallback(InputAction _inputAction, Action<object> _callback, Type _type)
        {
            inputAction = _inputAction;
            callback = _callback;
            type = _type;
        }
    }
}
