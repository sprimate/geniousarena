using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(CapsuleCollider))]
public class FighterModel : MonoBehaviour, IFighterDataHolder
{
    public Vector3 scaledAnimationMovementOffset {
        get
        {
            Vector3 ret = animationMovementOffset;
            ret.Scale(transform.localScale);
            return ret;
        }
    }

    [Tooltip("I believe this needs to be either the center.y of the bounding BoxCollider (on the Hitbox), or 0. " +
        "I'm not sure why or how this works Use this field as an 'offset' for collider hitboxes")]
    public float hitboxOffsetOrCharCenter;
    public FighterData fighterData { get; set; }
    public Vector3 rotationAdjustment;
    public Vector3 animationMovementOffset;
    Vector3 defaultPosition;
    Vector3 defaultRotation;
    Vector3 defaultScale;

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        defaultRotation = transform.localEulerAngles;
    }

    public void AdjustEulerAngles(Vector3 toAdjust)
    {
        transform.localEulerAngles += toAdjust;
    }

    public void ResetEulerAngles()
    {
        transform.localEulerAngles = defaultRotation;
    }
    public void AdjustPosition(Vector3 toAdjust)
    {
        transform.localPosition += toAdjust;
    }

    public void ResetPosition()
    {
        transform.localPosition = defaultPosition;
    }
    public void AdjustScale(Vector3 toAdjust)
    {
        transform.localScale += toAdjust;
    }

    public void ResetScale()
    {
        transform.localScale = defaultScale;
    }
    //Used if in the move editor scene to hide an error
    void OnMovingAnimationStarted()
    {

    }

    void OnMovingAnimationEnded()
    {

    }
}
