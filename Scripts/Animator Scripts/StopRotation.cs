using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopRotation : FighterAnimatorScript
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SetRotation(false);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        SetRotation(false);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        SetRotation(true);
    }

    void SetRotation(bool val)
    {
        if (controlla != null && controlla.locomotion != null)
        {
            controlla.locomotion.canRotate = val;
        }
    }
}
