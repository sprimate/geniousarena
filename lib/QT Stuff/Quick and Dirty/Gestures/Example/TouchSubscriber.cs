using UnityEngine;
using QuickAndDirty.Gestures;
/*
 * Testing class for Quick And Dirty Gestures
 */

public class TouchSubscriber : MonoBehaviour {
	
	class Node 
	{
		public string data;
		public Node next;
	}
	
	Node head;
	Node last;
	int depth = 0;
	
	// Use this for initialization
	void Start () 
	{
		head = new Node();
		last = head;
		head.data = "Initializing...";
		depth++;
		
		QADGestures.OnDragStart += delegate(Gesture drag) 
		{
			AddMessage("DragStart received! " + drag.position);
			AddMessage("Time held " + drag.timeHeld);
		};
		
		QADGestures.OnDrag += delegate(Gesture drag) 
		{
			AddMessage("Drag received! " + drag.position);
		};
		
		QADGestures.OnDragEnd += delegate(Gesture drag) 
		{
			AddMessage("Drag ended! " + drag.position);
            AddMessage("Total Delta " + drag.totalDeltaPosition);
		};
        QADGestures.OnTap += delegate(Gesture tap)
        {
            AddMessage("Tap detected! " + tap.position);
        };
        QADGestures.OnLongHold += delegate(Gesture longHold)
        {
            AddMessage("Holy Long-Hold Batman!");
        };
        QADGestures.OnLongHoldEnd += delegate(Gesture longHold)
        {
            AddMessage("Long hold ended!");
        };
	}
	
	void OnGUI()
	{
		GUI.skin.box.fontSize = 22;
		
		Rect r = new Rect(0, 0, Screen.width, Screen.height);
		GUILayout.BeginArea(r);
		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();
		
		for(Node n = head; n != null; n = n.next)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Box(n.data);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
	void AddMessage(string message)
	{
		Node newNode = new Node();
		newNode.data = message;
		last.next = newNode;
		last = newNode;
		
		depth++;
		
		if(depth > 12)
			head = head.next;
	}
}
